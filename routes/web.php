<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/coba', function () {
    return view('landing/layout/template');
});

Route::get('/coba2', function () {
    return view('main/index');
});


Route::get('/cb', function () {
    return view('main/layout/template_fb');
});


Route::get('/tes', function () {
    return view('tes');
});

Route::get('/administrator', function () {
    return view('admin/dashboard');
});


Route::get('/', 'HomeController@index');

Route::get('/administrator/searching', function () {
    return view('admin/searching');
});
Route::get('/administrator/cif/{ic}', 'Admin\SearchingController@cif');
Route::post('/searching_ic', 'Admin\SearchingController@searching');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/detail-berita/{id}', 'HomeController@detail_berita');
Route::get('/event', 'HomeController@event');
Route::get('/event/detail/{id}', 'HomeController@detail_event');
Route::get('/gallery', 'HomeController@gallery');


Route::resource('complaint', 'ComplaintController');
Route::get('/account-register', 'HomeController@register');
Route::get('/services/list', 'ComplaintController@list');

Route::post('/save-report-complaints', 'ComplaintController@save_report_complaints');
Route::post('/save-register', 'HomeController@post_register');


Route::get('/home', 'HomeController@index')->name('home');



Route::group( ['middleware' => ['auth']], function() {

	Route::resource('roles', 'RoleController');

	Route::group(['prefix' => 'administrator'], function(){
		Route::get('/', 'DashboardController@index');
	});

	Route::group(['prefix' => 'administrator/ngo'], function(){
		Route::get('/add', 'NGO\RegisterNGOController@add');
		Route::post('/save_register', 'NGO\RegisterNGOController@save_register');
		Route::get('/detail_ngo/{id}', 'NGO\RegisterNGOController@detail_ngo');
		Route::get('/index', 'NGO\RegisterNGOController@index');
		Route::post('/save_detail', 'NGO\RegisterNGOController@save_detail');
		Route::post('/save', 'NGO\RegisterNGOController@save');
	});



	Route::group(['prefix' => 'administrator/parameter'], function(){
		Route::get('/add-pr-report-complaint', 'ReportComplaint\PrReportComplaintController@add');
		Route::post('/save-pr-report-complaint', 'ReportComplaint\PrReportComplaintController@save');
		Route::get('/list-pr-report-complaint', 'ReportComplaint\PrReportComplaintController@index');

		Route::get('/add-pr-position', 'Parameter\PrPositionController@add');
		Route::post('/save-pr-position', 'Parameter\PrPositionController@save');
		Route::get('/list-pr-position', 'Parameter\PrPositionController@index');

		Route::get('/add-pr-organization', 'Parameter\PrOrganizationController@create');
		Route::post('/save-pr-organization', 'Parameter\PrOrganizationController@store');
		Route::get('/list-pr-organization', 'Parameter\PrOrganizationController@index');
		Route::get('/kemaskini-pr-organization/{id}', 'Parameter\PrOrganizationController@edit');
		Route::post('/kemaskini-pr-organization/{id}', 'Parameter\PrOrganizationController@update');
		Route::get('/padam-pr-organization/{id}', 'Parameter\PrOrganizationController@destroy');
		
		Route::get('/add-pr-parti', 'Parameter\PrPartiController@create');
		Route::post('/save-pr-parti', 'Parameter\PrPartiController@store');
		Route::get('/list-pr-parti', 'Parameter\PrPartiController@index');
		Route::get('/kemaskini-pr-parti/{id}', 'Parameter\PrPartiController@edit');
		Route::post('/kemaskini-pr-parti/{id}', 'Parameter\PrPartiController@update');
		Route::get('/padam-pr-parti/{id}', 'Parameter\PrPartiController@destroy');

		Route::get('/add-pr-partigabungan', 'Parameter\PrPartiGabunganController@create');
		Route::post('/save-pr-partigabungan', 'Parameter\PrPartiGabunganController@store');
		Route::get('/list-pr-partigabungan', 'Parameter\PrPartiGabunganController@index');
		Route::get('/kemaskini-pr-partigabungan/{id}', 'Parameter\PrPartiGabunganController@edit');
		Route::post('/kemaskini-pr-partigabungan/{id}', 'Parameter\PrPartiGabunganController@update');
		Route::get('/padam-pr-partigabungan/{id}', 'Parameter\PrPartiGabunganController@destroy');

		Route::get('/add-pr-dun', 'Parameter\PrDunController@create');
		Route::post('/save-pr-dun', 'Parameter\PrDunController@store');
		Route::get('/list-pr-dun', 'Parameter\PrDunController@index');
		Route::get('/kemaskini-pr-dun/{id}', 'Parameter\PrDunController@edit');
		Route::post('/kemaskini-pr-dun/{id}', 'Parameter\PrDunController@update');
		Route::get('/padam-pr-dun/{id}', 'Parameter\PrDunController@destroy');

		Route::get('/add-pr-parlimen', 'Parameter\PrParlimenController@create');
		Route::post('/save-pr-parlimen', 'Parameter\PrParlimenController@store');
		Route::get('/list-pr-parlimen', 'Parameter\PrParlimenController@index');
		Route::get('/kemaskini-pr-parlimen/{id}', 'Parameter\PrParlimenController@edit');
		Route::post('/kemaskini-pr-parlimen/{id}', 'Parameter\PrParlimenController@update');
		Route::get('/padam-pr-parlimen/{id}', 'Parameter\PrParlimenController@destroy');

		Route::get('/add-pr-beneficiary', 'Parameter\PrBeneficiaryController@create');
		Route::post('/save-pr-beneficiary', 'Parameter\PrBeneficiaryController@store');
		Route::get('/list-pr-beneficiary', 'Parameter\PrBeneficiaryController@index');
		Route::get('/kemaskini-pr-beneficiary/{id}', 'Parameter\PrBeneficiaryController@edit');
		Route::post('/kemaskini-pr-beneficiary/{id}', 'Parameter\PrBeneficiaryController@update');
		Route::get('/padam-pr-beneficiary/{id}', 'Parameter\PrBeneficiaryController@destroy');
	});

	Route::group(['prefix' => 'administrator/report-complaint'], function(){
		Route::get('/index/{id}', 'ReportComplaint\DetailReportComplaintController@index');
		Route::get('/action/{id}', 'ReportComplaint\DetailReportComplaintController@action');
		Route::get('/action_to/{id}', 'ReportComplaint\DetailReportComplaintController@action_to');
		Route::post('/save_complaint/{id}', 'ReportComplaint\DetailReportComplaintController@save_complaint');
		Route::get('/print/{id}', 'ReportComplaint\DetailReportComplaintController@print');
		Route::get('/location/{id}', 'ReportComplaint\DetailReportComplaintController@view_location');
		Route::post('/send_sms/{id}', 'ReportComplaint\DetailReportComplaintController@send_sms');
	});

	Route::group(['prefix' => 'administrator/ngo'], function(){
		Route::get('/add', 'NGO\RegisterNGOController@add');
		Route::post('/save_register', 'NGO\RegisterNGOController@save_register');
		Route::get('/detail_ngo/{id}', 'NGO\RegisterNGOController@detail_ngo');
		Route::get('/index', 'NGO\RegisterNGOController@index');
		Route::post('/save_detail', 'NGO\RegisterNGOController@save_detail');
	});

	Route::group(['prefix' => 'administrator'], function(){
		Route::get('/postcode/{id}', 'Parameter\ApiController@postcode');
		Route::get('/chairman_ic/{chairman_ic}', 'Parameter\ApiController@chairman_ic');
		Route::get('/secretary_ic/{secretary_ic}', 'Parameter\ApiController@secretary_ic');
		Route::get('/postcode2/{id}', 'Parameter\ApiController@postcode2');
		Route::get('/ic/{ic}', 'Parameter\ApiController@ic');
		Route::get('/ic_survey/{ic}', 'Parameter\ApiController@ic_survey');
		Route::get('/ic_reg/{ic}', 'Parameter\ApiController@ic_reg');
	});


	Route::group(['prefix' => 'administrator/information_public'], function(){
		Route::get('/index', 'InformationPublic\InformationPublicController@index');
		Route::post('/searching_ic', 'InformationPublic\InformationPublicController@searching');
		Route::get('/add/{ic}', 'InformationPublic\InformationPublicController@add');
		Route::post('/save', 'InformationPublic\SaveInformationController@save');
		Route::post('/save_new', 'InformationPublic\SaveInformationController@save_new');
		Route::get('/list', 'InformationPublic\InformationPublicController@list');
		Route::get('/detail/{ic}', 'InformationPublic\InformationPublicController@detail');
	});

	Route::group(['prefix' => 'administrator/survey'], function(){
		Route::get('/add', 'Survey\SurveyController@add');
		Route::post('/save', 'Survey\SurveyController@save');
		Route::get('/index', 'Survey\SurveyController@index');
		Route::get('/chart', 'Survey\SurveyController@chart');
		Route::get('/detail/{ic}', 'Survey\SurveyController@detail_survey');
	});

	/* -- Module Pilihan Raya -- */
	Route::group(['prefix' => 'administrator/pilihan-raya'], function(){
		Route::group(['prefix' => 'prn'], function(){
			Route::get('/add', 'PilihanRaya\PrnController@create');
			Route::post('/save', 'PilihanRaya\PrnController@store');
			Route::get('/index', 'PilihanRaya\PrnController@index');
			Route::get('/tree', 'PilihanRaya\PrnController@tree');
			Route::get('/kajian-sikap/{id}', 'PilihanRaya\PrnController@kajian_sikap');
			Route::post('/save-kajian-sikap', 'PilihanRaya\PrnController@save_kajian_sikap');
			Route::get('/senarai-kajian-sikap/{id}', 'PilihanRaya\PrnController@senarai_kajian_sikap');
			Route::get('/hitory/{id}/{id2}', 'PilihanRaya\PrnController@history');
			
		});


		Route::group(['prefix' => 'prk'], function(){
			Route::get('/add', 'PilihanRaya\PrkController@create');
			Route::post('/save', 'PilihanRaya\PrkController@store');
			Route::get('/index', 'PilihanRaya\PrkController@index');
			Route::get('/kajian-sikap/{id}', 'PilihanRaya\PrkController@kajian_sikap');
			Route::post('/save-kajian-sikap', 'PilihanRaya\PrkController@save_kajian_sikap');
			Route::get('/senarai-kajian-sikap/{id}', 'PilihanRaya\PrkController@senarai_kajian_sikap');
			Route::get('/hitory/{id}/{id2}', 'PilihanRaya\PrkController@history');
		});


		Route::get('/add-maklumat-calon/{id}/{type}', 'PilihanRaya\CandidateController@create');
		Route::post('/add-maklumat-calon/{id}/{type}', 'PilihanRaya\CandidateController@store');
		Route::get('/senarai-maklumat-calon/{id}/{type}', 'PilihanRaya\CandidateController@show');
		Route::get('/kemaskini-maklumat-calon/{id}/{type}/{calon}', 'PilihanRaya\CandidateController@edit');
		Route::post('/kemaskini-maklumat-calon/{id}/{type}/{calon}', 'PilihanRaya\CandidateController@update');
		Route::get('/padam-maklumat-calon/{calon}', 'PilihanRaya\CandidateController@destroy');
		
	});


	/* Web */
	Route::group(['prefix' => 'administrator/web'], function(){
		Route::group(['prefix' => 'news'], function(){
			Route::get('/add', 'Web\BeritaController@create');
			Route::post('/save', 'Web\BeritaController@store');
			Route::get('/index', 'Web\BeritaController@index');
			Route::get('/kemaskini/{id}', 'Web\BeritaController@edit');
			Route::post('/update/{id}', 'Web\BeritaController@update');
			Route::get('/padam/{id}', 'Web\BeritaController@destroy');
		});


		Route::group(['prefix' => 'slider'], function(){
			Route::get('/add', 'Web\SliderController@create');
			Route::post('/save', 'Web\SliderController@store');
			Route::get('/index', 'Web\SliderController@index');
			Route::get('/kemaskini/{id}', 'Web\SliderController@edit');
			Route::post('/update/{id}', 'Web\SliderController@update');
			Route::get('/padam/{id}', 'Web\SliderController@destroy');
		});


		Route::group(['prefix' => 'event'], function(){
			Route::get('/add', 'Web\EventController@create');
			Route::post('/save', 'Web\EventController@store');
			Route::get('/index', 'Web\EventController@index');
			Route::get('/kemaskini/{id}', 'Web\EventController@edit');
			Route::post('/update/{id}', 'Web\EventController@update');
			Route::get('/padam/{id}', 'Web\EventController@destroy');
		});

		Route::group(['prefix' => 'gallery'], function(){
			Route::get('/add', 'Web\GalleryController@create');
			Route::post('/save', 'Web\GalleryController@store');
			Route::get('/index', 'Web\GalleryController@index');
			Route::get('/kemaskini/{id}', 'Web\GalleryController@edit');
			Route::post('/update/{id}', 'Web\GalleryController@update');
			Route::get('/padam/{id}', 'Web\GalleryController@destroy');
		});
		
	});
});



Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);

Route::group(['prefix' => 'administrator/organization'], function(){
	Route::get('/add', 'Organization\RegisterOrganizationController@add');
	Route::post('/save_register', 'Organization\RegisterOrganizationController@save_register');
	Route::get('/detail_organization/{id}', 'Organization\RegisterOrganizationController@detail_Organization');
	Route::get('/index', 'Organization\RegisterOrganizationController@index');
	Route::post('/save_detail', 'Organization\RegisterOrganizationController@save_detail');
	Route::post('/save', 'Organization\RegisterOrganizationController@save');

});

Route::resource('geolocation', 'GeoLogController');
Route::get('geolocation/error/{id}', 'GeoLogController@error');
Route::get('geolocation/map/{lat}/{lng}/{location?}', 'GeoLogController@map');
Route::post('getLocation', 'GeoLogController@getlocation');


Route::group(['prefix' => 'information'], function(){
	Route::get('/help', 'InformationController@dashboard_help');
});
Route::group(['prefix' => 'contact-us'], function(){
	Route::get('/', 'ContactUsController@index');
});



Route::group(['prefix' => 'pilihan-raya'], function(){
	Route::get('/prk/{id}', 'PilihanRaya\DashboardPrkController@dashboard');
});


Route::get('/administrator/list-user', 'Admin\AdminController@list_user');
Route::get('/administrator/add-user', 'Admin\AdminController@add_user');
Route::post('/administrator/save_user', 'Admin\UserController@save_user');

Route::group(['prefix' => 'administrator/user'], function(){
	Route::get('/index', 'Admin\UserController@index');
	Route::post('/save', 'Admin\UserController@save');
	Route::get('/add', 'Admin\UserController@add');
});

Route::group(['prefix' => 'services'], function(){
	Route::get('/add/{id}', 'ComplaintController@add');
});



Route::group(['prefix' => 'event'], function(){
	Route::get('/', 'HomeController@event');
	Route::get('/detail/{id}', 'HomeController@detail_event');
});

