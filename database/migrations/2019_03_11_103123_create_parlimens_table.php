<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParlimensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('param_parlimens'))
        {
            Schema::create('param_parlimens', function (Blueprint $table) {
                $table->increments('id');
                $table->String('code',20)->nullable();
                $table->String('parlimen_name',100)->nullable();
                $table->String('state_code',100)->nullable();
                $table->String('user_id',20)->nullable();
                $table->tinyInteger('status')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('param_parlimens');
    }
}
