<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailReportComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if(!Schema::hasTable('detail_report_complaints'))
        {
            Schema::create('detail_report_complaints', function (Blueprint $table) {
                $table->increments('id');
                $table->String('user_id',50)->nullable();
                $table->String('ic',20)->nullable();
                $table->integer('pr_report_complaints_id')->nullable();
                $table->String('title',100)->nullable();
                $table->longtext('desc')->nullable();
                $table->String('address',100)->nullable();
                $table->String('postcode',10)->nullable();
                $table->String('city',100)->nullable();
                $table->String('state',100)->nullable();
                $table->integer('reply_id')->nullable();
                $table->integer('petugas_id')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_report_complaints');
    }
}
