<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('surveys'))
        {
            Schema::create('surveys', function (Blueprint $table) {
                $table->increments('id');
                $table->String('user_id',20)->nullable();
                $table->String('ic',20)->nullable();
                $table->String('kecenderungan',10)->nullable();
                $table->String('Kodlokaliti',50)->nullable();
                $table->String('NamaLokaliti',150)->nullable();
                $table->String('NamaParlimen',150)->nullable();
                $table->String('NamaDM',150)->nullable();
                $table->String('NamaDun',150)->nullable();
                $table->longtext('remark')->nullable();
                $table->tinyInteger('status')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveys');
    }
}
