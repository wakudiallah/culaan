<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('candidates'))
        {
            Schema::create('candidates', function (Blueprint $table) {
                $table->increments('id');
                $table->String('ic',20)->nullable();
                $table->String('name',50)->nullable();
                $table->String('real_name',50)->nullable();
                $table->String('picture',50)->nullable();
                $table->String('parti_id',20)->nullable();
                $table->dateTime('born_date')->nullable();
                $table->String('activity',70)->nullable();
                $table->String('position',50)->nullable();
                $table->String('web',50)->nullable();
                $table->String('soc_fb',100)->nullable();
                $table->String('soc_ig',100)->nullable();
                $table->String('soc_twitter',100)->nullable();
                $table->String('address',100)->nullable();
                $table->String('postcode',30)->nullable();
                $table->String('city',50)->nullable();
                $table->String('state',50)->nullable();
                $table->tinyInteger('status')->nullable();
                $table->integer('user_id')->nullable();
                $table->integer('parlimen_id')->nullable();
                $table->integer('dun_id')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }

        
           
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates');
    }
}
