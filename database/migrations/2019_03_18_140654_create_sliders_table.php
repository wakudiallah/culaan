<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('sliders'))
        {
            Schema::create('sliders', function (Blueprint $table) {
                $table->increments('id');
                $table->String('title',20)->nullable();
                $table->String('picture',1000)->nullable();
                $table->integer('author_id')->nullable();
                $table->integer('approve_id')->nullable();
                $table->dateTime('date_approve')->nullable();
                $table->tinyInteger('status')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
    }
}
