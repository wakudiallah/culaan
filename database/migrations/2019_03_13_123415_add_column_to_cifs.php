<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToCifs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('cifs'))
        {
            Schema::table('cifs', function ($table) {
                $table->float('lat',10,6)->nullable();
                $table->float('lng',10,6)->nullable();
                $table->text('location')->nullable();
                $table->String('ip',50)->nullable();
                $table->String('mobile2',20)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cifs', function (Blueprint $table) {
            //
        });
    }
}
