<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrReportComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('pr_report_complaints'))
        {
            Schema::create('pr_report_complaints', function (Blueprint $table) {
                $table->increments('id');
                $table->String('desc',50)->nullable();
                $table->integer('type')->nullable();
                $table->boolean('status')->nullable();
                $table->String('user_id',50)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pr_report_complaints');
    }
}
