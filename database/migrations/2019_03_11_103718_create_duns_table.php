<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('param_duns'))
        {
            Schema::create('param_duns', function (Blueprint $table) {
                $table->increments('id');
                $table->String('code',20)->nullable();
                $table->String('dun_name',100)->nullable();
                $table->String('state_code',50)->nullable();
                $table->String('parlimen_code',50)->nullable();
                $table->String('user_id',20)->nullable();
                $table->tinyInteger('status')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('duns');
    }
}
