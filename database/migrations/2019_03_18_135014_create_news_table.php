<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('news'))
        {
            Schema::create('news', function (Blueprint $table) {
                $table->increments('id');
                $table->String('title',20)->nullable();
                $table->String('news',1000)->nullable();
                $table->integer('author_id')->nullable();
                $table->integer('approve_id')->nullable();
                $table->dateTime('date_approve')->nullable();
                $table->tinyInteger('status')->nullable();
                $table->String('picture',100)->nullable();
                $table->String('pic_title',100)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
