<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('user_details'))
        {
            Schema::create('user_details', function (Blueprint $table) {
                $table->increments('id');
                $table->string('user_id',50)->nullable();
                $table->string('ic',20)->nullable();
                $table->string('fullname',100)->nullable();
                $table->longText('address')->nullable();
                $table->string('postcode',10)->nullable();
                $table->string('city',100)->nullable();
                $table->string('state',100)->nullable();
                $table->string('mobile_phone',20)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
