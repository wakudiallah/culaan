<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToDetailReportComplaints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         
         if(!Schema::hasTable('sliders'))
        {
                Schema::table('detail_report_complaints', function($table) {
                $table->float('lat',10,6)->nullable();
                $table->float('lng',10,6)->nullable();
                $table->text('location')->nullable();
                $table->String('ip',50)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_report_complaints', function (Blueprint $table) {
            //
        });
    }
}
