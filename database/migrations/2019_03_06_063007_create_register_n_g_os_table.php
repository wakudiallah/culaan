<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisterNGOsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if(!Schema::hasTable('register_ngos'))
        {
            Schema::create('register_ngos', function (Blueprint $table) {
                $table->increments('id');
                $table->String('user_id',50)->nullable();
                $table->String('ngo_name',200)->nullable();
                $table->String('ngo_reg_number',50)->nullable();
                $table->integer('total_membership')->nullable();
                $table->longText('address',200)->nullable();
                $table->String('postcode',20)->nullable();
                $table->String('city',100)->nullable();
                $table->String('state',100)->nullable();
                $table->String('chairman_ic',20)->nullable();
                $table->String('chairman_name',100)->nullable();
                $table->String('chairman_phone',20)->nullable();
                $table->String('secretary_ic',20)->nullable();
                $table->String('secretary_name',100)->nullable();
                $table->String('secretary_phone',20)->nullable();
                $table->String('email',80)->nullable();
                $table->String('fax_number',20)->nullable();
                $table->String('phone_number',20)->nullable();
                $table->tinyInteger('status')->nullable();
                $table->tinyInteger('petugas_id')->nullable();
                $table->String('Kodlokaliti',50)->nullable();
                $table->String('NamaLokaliti',150)->nullable();
                $table->String('NamaParlimen',150)->nullable();
                $table->String('NamaDM',150)->nullable();
                $table->String('NamaDun',150)->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('register_ngos');
    }
}
