<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrnegerisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if(!Schema::hasTable('prnegeris'))
        {
            Schema::create('prnegeris', function (Blueprint $table) {
                $table->increments('id');
                $table->String('state_name',100)->nullable();
                $table->String('parlimen_code',20)->nullable();
                $table->String('parlimen_name',50)->nullable();
                $table->dateTime('election_date')->nullable();
                $table->String('user_id',20)->nullable();
                $table->tinyInteger('status')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    } 


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prnegeris');
    }
}
