<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailImageReportComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       if(!Schema::hasTable('detail_image_report_complaints'))
        {
            Schema::create('detail_image_report_complaints', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('detail_report_complaints_id')->nullable();
                $table->String('filename',200)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_image_report_complaints');
    }
}
