<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('history_complaints'))
        {
            Schema::create('history_complaints', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('complaint_id')->nullable();
                $table->string('activity', 220)->nullable();
                $table->string('history_id',100)->nullable();
                $table->string('remark',100)->nullable();
                $table->string('user_id',100)->nullable();
                $table->tinyInteger('status')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_complaints');
    }
}
