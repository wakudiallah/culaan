<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCIFsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if(!Schema::hasTable('cifs'))
        {
            Schema::create('cifs', function (Blueprint $table) {
                $table->increments('id');
                $table->String('user_id',50)->nullable();
                $table->String('ic',20)->nullable();
                $table->String('name',100)->nullable();
                $table->String('address_1',100)->nullable();
                $table->String('address_2',100)->nullable();
                $table->String('address_3',100)->nullable();
                $table->String('postcode',10)->nullable();
                $table->String('city',50)->nullable();
                $table->String('state',50)->nullable();
                $table->String('telephone',20)->nullable();
                $table->String('mobile',20)->nullable();
                $table->String('email',50)->nullable();
                $table->String('residence',20)->nullable();
                $table->String('religion',20)->nullable();
                $table->String('gender',10)->nullable();
                $table->boolean('citizenship')->nullable();
                $table->boolean('is_registered')->nullable();
                $table->boolean('is_active')->default(1)->nullable();

                $table->String('ICLama',20)->nullable();
                $table->String('ICSpouse',20)->nullable();
                $table->String('NoRumah',50)->nullable();
                $table->String('Kodlokaliti',50)->nullable();
                $table->String('NamaLokaliti',50)->nullable();
                $table->String('NamaParlimen',50)->nullable();
                $table->String('NamaDM',50)->nullable();
                $table->String('NamaDun',50)->nullable();
                $table->String('Negeri',50)->nullable();
                $table->date('dob',10)->nullable();
                $table->String('Bangsa',50)->nullable();
                $table->String('NamaPusatMengundi',50)->nullable();
                $table->String('NoSaluran',50)->nullable();
                $table->String('kecenderungan',50)->nullable();

                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('cifs');
    }
}
