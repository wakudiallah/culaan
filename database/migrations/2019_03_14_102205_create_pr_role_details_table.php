<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrRoleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         if(!Schema::hasTable('pr_role_details'))
        {
            Schema::create('pr_role_details', function (Blueprint $table) {
                $table->increments('id');
                $table->String('role_id',10)->nullable();
                $table->String('modul',200)->nullable();
                $table->integer('insert')->nullable();
                $table->integer('update')->nullable();
                $table->integer('delete')->nullable();
                $table->integer('view')->nullable();
                $table->tinyInteger('status')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pr_role_details');
    }
}
