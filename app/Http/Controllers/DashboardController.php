<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use App\User;
use App\Model\Parameter\PrReportComplaint;
use App\Model\DetailReportComplaint;
use App\Model\DetailImageReportComplaint;
use App\Model\UserDetail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $count_aduan = DetailReportComplaint::where('pr_report_complaints_id', '1')->count();
        $count_cadangan = DetailReportComplaint::where('pr_report_complaints_id', '2')->count();
        $count_penghormatan = DetailReportComplaint::where('pr_report_complaints_id', '3')->count();
        $count_penghargaan = DetailReportComplaint::where('pr_report_complaints_id', '4')->count();
        $count_permohonan = DetailReportComplaint::where('pr_report_complaints_id', '5')->count();
        $count_pertanyaan = DetailReportComplaint::where('pr_report_complaints_id', '6')->count();

        return view('admin.dashboard',compact('count_aduan', 'count_cadangan', 'count_penghormatan', 'count_penghargaan', 'count_permohonan', 'count_pertanyaan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
