<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use App\User;
use App\Model\Main\Slider;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $sd  = Slider::orderby('created_at','DESC')->get();
        return view('admin.web.slider.index',compact('sd'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.web.slider.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id   = Auth::user()->id;
        $user_name   = Auth::user()->name;

        $file            = $request->file('picture');
        $filename        = $file->getClientOriginalName();
        $extension       = $file->getClientOriginalExtension();
        $picture         = $filename;
        $destinationPath = base_path().'/public/main/images/slider/';
        $file->move($destinationPath, $picture);

        $upload_file = $picture;


        $data            =  new Slider;
        
        $data->picture   =  $upload_file;
        $data->author_id =  $user_id;
        $data->status    =  '0';
        $data->save();
         

        return redirect('/administrator/web/slider/add')->with(['success' => 'Data Slider berjaya disimpan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sd  = Slider::where('id', $id)->first();
        return view('admin.web.slider.edit',compact('sd'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id   = Auth::user()->id;
        $user_name   = Auth::user()->name;

        $file            = $request->file('picture');
        $filename        = $file->getClientOriginalName();
        $extension       = $file->getClientOriginalExtension();
        $picture         = $filename;
        $destinationPath = base_path().'/public/main/images/slider/';
        $file->move($destinationPath, $picture);

        $upload_file = $picture;


        Slider::where('id', $id)->update(array('picture' => $upload_file ));
         

        return redirect('/administrator/web/slider/add')->with(['success' => 'Data Slider berjaya dikemaskini']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $emp = Slider::findOrFail($id)->delete();   
                        
        return redirect('administrator/web/slider/index')->with('success', 'Data Slider berjaya dipadam'); 
    }
}
