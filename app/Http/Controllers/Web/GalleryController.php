<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use App\User;
use App\Model\Main\Gallery;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $sd  = Gallery::orderby('created_at','DESC')->get();
        return view('admin.web.gallery.index',compact('sd'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.web.gallery.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id   = Auth::user()->id;
        $user_name   = Auth::user()->name;

        $date_event  = $request->input('date_event');
        $date =  date('Y-m-d', strtotime($date_event)); 

        $file            = $request->file('picture');
        $filename        = $file->getClientOriginalName();
        $extension       = $file->getClientOriginalExtension();
        $picture         = $filename;
        $destinationPath = base_path().'/public/main/images/gallery/';
        $file->move($destinationPath, $picture);

        $upload_file = $picture;


        $data            =  new Gallery;
        
        $data->picture   =  $upload_file;
        $data->title     =  $request->title;
       
        $data->author_id =  $user_id;
        $data->status    =  '0';
        $data->save();
         

        return redirect('/administrator/web/gallery/add')->with(['success' => 'Data Gallery berjaya disimpan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sd  = Gallery::where('id', $id)->first();
        return view('admin.web.gallery.edit',compact('sd'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id   = Auth::user()->id;
        $user_name   = Auth::user()->name;

         $title      =  $request->title;

        if($request->hasFile('picture')){ 
            $file            = $request->file('picture');
            $filename        = $file->getClientOriginalName();
            $extension       = $file->getClientOriginalExtension();
            $picture         = $filename;
            $destinationPath = base_path().'/public/main/images/gallery/';
            $file->move($destinationPath, $picture);

            $upload_file = $picture;



            Gallery::where('id', $id)->update(array('picture' => $upload_file, 'title' => $title, 'author_id' => $user_id)); 
        }
        else{
            Gallery::where('id', $id)->update(array('title' => $title, 'author_id' => $user_id)); 
        }

        return redirect('/administrator/web/gallery/index')->with(['success' => 'Data Gallery berjaya dikemaskini']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $emp = Gallery::findOrFail($id)->delete();   
                        
        return redirect('administrator/web/gallery/index')->with('success', 'Data Gallery berjaya dipadam'); 
    }
}
