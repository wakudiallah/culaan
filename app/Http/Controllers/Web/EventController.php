<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use App\User;
use App\Model\Event;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $event = Event::all();
        return view('admin.web.event.index', compact('event'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.web.event.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id   = Auth::user()->id;

        $event_date  = $request->input('date');
        $date        =  date('Y-m-d 00:00:01', strtotime($event_date)); 

        $data               =  new Event;
        $data->title        =  $request->title;
        $data->desc         =  $request->desc;
        $data->event_date   =  $date;
        $data->user_id      =  $user_id;
        $data->status       =  '1';
        $data->save();
         

        return redirect('/administrator/web/event/add')->with(['success' => 'Data berjaya disimpan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event  = Event::where('id', $id)->first();
        return view('admin.web.event.edit',compact('event'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function update(Request $request, $id)
    {
        $user_id     = Auth::user()->id;
        $title       =  $request->title;
        $desc        =  $request->desc;
        $event_date  =  $request->input('date');
        $date        =  date('Y-m-d 00:00:01', strtotime($event_date)); 

       

        Event::where('id', $id)->update(array('title' => $title, 'desc' => $desc, 'event_date' => $date)); 
      

        return redirect('/administrator/web/event/index')->with(['success' => 'Data Acara berjaya dikemaskini']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
