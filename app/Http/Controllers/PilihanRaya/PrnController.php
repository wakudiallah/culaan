<?php

namespace App\Http\Controllers\PilihanRaya;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use PDF;
use App\Model\Parameter\Parlimen;
use App\Model\Parameter\Dun;
use App\Model\Parameter\State;
use App\Model\Prnegeri;
use App\Model\Prk;
use App\Model\Survey;


class PrnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $pr  = Prnegeri::get();

        return view('admin.pilihan_raya.prnegeri.index',compact('pr', 'state'));
        
    }
    public function tree()
    {
        $pr  = Prnegeri::get();
       


        $state = DB::table('prnegeris')
        ->select('prnegeris.state_name', DB::raw('sum(id) as amount'))
        ->groupBy('state_name')
        ->get();


        return view('admin.pilihan_raya.prnegeri.tree',compact('pr', 'state'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pr  = Parlimen::where('status', 1)->get();
        $state  = State::whereIn('state_code', ['KUL', 'LBN', 'PJY'])->get();

        return view('admin.pilihan_raya.prnegeri.add',compact('pr', 'state'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id   = Auth::user()->id;
        
        $kod_parlimen = $request->input('parlimen_name');
        $code_parlimen   = substr($kod_parlimen,0, 4); 

        $parlimen = $kod_parlimen;    
        $parlimen_name = substr($parlimen, strpos($parlimen, "|") + 1 );     


        $election_date  = $request->input('election_date');
        $date =  date('Y-m-d 00:00:01', strtotime($election_date)); 

        $now = \Carbon\Carbon::now()->startOfDay();

        $check_prk = Prnegeri::where('parlimen_code', $code_parlimen)->where('election_date', '>=', $now)->count();


        
        $data                  = new Prnegeri; 
        $data->user_id         = $user_id; 
        $data->state_name      = $request->input('state_name');
        /*$data->parlimen_name = $parlimen_name;*/
        $data->parlimen_code   = $code_parlimen;
        $data->election_date   = $date;
        $data->status          = '1';
        $data->election_type   = 'PRN';
        $data->save();

        return redirect('/administrator/pilihan-raya/prn/add')->with('success', 'Data Pilihan Raya berjaya disimpan');         

        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    public function kajian_sikap($id)
    {
        $parlimen  = Prnegeri::where('parlimen_code', $id)->first();

        return view('admin.pilihan_raya.prnegeri.kajian_sikap',compact('id', 'parlimen'));

    }


    public function save_kajian_sikap(Request $request)
    {
        $user_id   = Auth::user()->id;

        $kod_parlimen = $request->input('code_parlimen');
        $ic_surveyx = $request->input('ic_survey');

        
        $data                   =  new Survey;
        $data->user_id          =  $user_id;
        $data->ic               =  $request->ic_survey;
        $data->name             =  $request->name_survey;
        $data->phone            =  $request->phone;
        $data->Kodlokaliti      =  $request->kodlokaliti_survey;
        $data->NamaLokaliti     =  $request->NamaLokaliti_survey;
        $data->NamaDM           =  $request->NamaDM_survey;
        $data->NamaDUN          =  $request->NamaDUN_survey;
        $data->NamaParlimen     =  $request->NamaParlimen_survey;
        $data->Negeri           =  $request->Negeri_survey;
        $data->kecenderungan    =  $request->kecenderungan;
        
        
        $data->ket           =  "PRN";
        $data->code_parlimen =  $kod_parlimen;
        $data->status        =  '1';
        $data->save();
         

         return redirect('/administrator/pilihan-raya/prn/kajian-sikap/'.$kod_parlimen)->with(['success' => 'Data Pilihan Raya berjaya disimpan']);
    }

    public function senarai_kajian_sikap($id)
    {
        $senarai  = Survey::where('code_parlimen', $id)->get();
        $parlimen  = Prnegeri::where('parlimen_code', $id)->first();

        return view('admin.pilihan_raya.prnegeri.senarai_kajian_sikap',compact('id', 'senarai', 'parlimen'));

    }


    public function history($id,  $id2)
    {
       
        $senarai = Survey::where('code_parlimen', $id)->where('ic', $id2)->get();
        $user    = Survey::where('code_parlimen', $id)->where('ic', $id2)->first();

        return view('admin.shared.history',compact( 'user', 'senarai'));
        
    }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
