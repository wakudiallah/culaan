<?php

namespace App\Http\Controllers\PilihanRaya;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use PDF;
use App\Model\Parameter\Parlimen;
use App\Model\Parameter\Dun;
use App\Model\Parameter\State;
use App\Model\Prnegeri;
use App\Model\Prk;
use App\Model\Survey;

class PrkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $pr  = Prk::get();

        return view('admin.pilihan_raya.prk.index',compact('pr', 'state'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pr  = Parlimen::where('status', 1)->get();
        $state  = State::get();
        $state  = State::whereNotIn('state_code', ['KUL', 'LBN', 'PJY'])->get();
        $dun  = Dun::get();

        return view('admin.pilihan_raya.prk.add',compact('pr', 'state', 'dun'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id   = Auth::user()->id;
        
        $kod_parlimen = $request->input('parlimen_name');
        $code_parlimen   = substr($kod_parlimen,0, 4); 

        $parlimen = $kod_parlimen;    
        $parlimen_name = substr($parlimen, strpos($parlimen, "|") );     

        $kod_dun = $request->input('dun_name');
        $dun_code   = substr($kod_dun,0, 3);

        $dun = $kod_dun;    
        $dun_name = substr($dun, strpos($dun, "|") + 1 ); 

        $election_date  = $request->input('election_date');
        $date =  date('Y-m-d 00:00:01', strtotime($election_date)); 

        

        $now = \Carbon\Carbon::now()->startOfDay();

        $check_prk = Prk::where('dun_code', $dun_code)->where('parlimen_code', $code_parlimen)->first();


        if($check_prk->election_date == $date){
            return redirect('/administrator/pilihan-raya/prk/add')->with('warning', 'Data Pilihan Raya sudah ada');

        }elseif($election_date > $now){
              return redirect('/administrator/pilihan-raya/prk/add')->with('warning', 'Tarikh Undi telah berlalu');

        }else{

        $data                  = new Prk; 
        $data->state_name      = $request->input('state_name');
        /*$data->parlimen_name = $parlimen_name;*/
        $data->parlimen_code   = $code_parlimen;
        
        $data->dun_code        = $dun_code;
        $data->dun_name        = $dun_name;
        $data->election_date   = $date;
        $data->user_id         = $user_id; 
        $data->status          = '1';
        $data->election_type   = 'PRK';
        $data->save();

        return redirect('/administrator/pilihan-raya/prk/add')->with('success', 'Data Pilihan Raya berjaya disimpan');
        }
        

    }


    public function kajian_sikap($id)
    {
        $parlimen  = Prk::where('dun_code', $id)->first();

        return view('admin.pilihan_raya.prk.kajian_sikap',compact('id', 'parlimen'));

    }

    public function senarai_kajian_sikap($id)
    {
        $senarai  = Survey::where('code_parlimen', $id)->get();
        $parlimen  = Prk::where('dun_code', $id)->first();

        return view('admin.pilihan_raya.prk.senarai_kajian_sikap',compact('id', 'senarai', 'parlimen'));

    }

    public function dashboard($id)
    {
        $prk  = Prk::where('dun_code', $id)->first();

        return view('main.pilihan_raya.index',compact('pr', 'state'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
