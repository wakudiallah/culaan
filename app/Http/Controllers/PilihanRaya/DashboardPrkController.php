<?php

namespace App\Http\Controllers\PilihanRaya;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use PDF;
use App\Model\Parameter\Parlimen;
use App\Model\Parameter\Dun;
use App\Model\Parameter\State;
use App\Model\Prnegeri;
use App\Model\Prk;
use App\Model\Survey;

class DashboardPrkController extends Controller
{
    public function dashboard($id)
    {
        $prk  = Prk::where('dun_code', $id)->first();

        return view('main.pilihan_raya.index',compact('prk', 'state'));
    }
}
