<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Parameter\PrBeneficiary;

class InformationController extends Controller
{
    public function dashboard_help()
    {
        $help  = PrBeneficiary::orderBy('id', 'DESC')->get();
        $total  = PrBeneficiary::where('status','1')->count();

        return view('main.information.list_beneficiary',compact('help','total'));
    }
}
