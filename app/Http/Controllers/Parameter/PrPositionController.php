<?php

namespace App\Http\Controllers\Parameter;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use App\Model\Parameter\PrPosition;

class PrPositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $report  = PrPosition::get();
        return view('admin.parameter.pr_position.index',compact('report'));
    }

    public function add()
    {
        return view('admin.parameter.pr_position.add');
    }

     public function save(Request $request)
    {
        $user = Auth::User();

        $desc        = $request->input('desc');
        $user_id     = $user->id;

        $count = PrPosition::where('desc',$desc)->count();
        if($count==0){
        	$pr_position          = new PrPosition;
	        $pr_position->desc    = $desc;
	        $pr_position->user_id = $user_id;
	        $pr_position->status  = '1';
	        $pr_position->save();

        	return redirect('/administrator/parameter/add-pr-position')->with(['success' => 'Data telah tersimpan']);
        }
        else{
        	return redirect('/administrator/parameter/list-pr-position')->with(['success' => 'Data telah ada']);
        }
       
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Parameter\PrReportComplaint  $prReportComplaint
     * @return \Illuminate\Http\Response
     */
    public function show(PrReportComplaint $prReportComplaint)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Parameter\PrReportComplaint  $prReportComplaint
     * @return \Illuminate\Http\Response
     */
    public function edit(PrReportComplaint $prReportComplaint)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Parameter\PrReportComplaint  $prReportComplaint
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PrReportComplaint $prReportComplaint)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Parameter\PrReportComplaint  $prReportComplaint
     * @return \Illuminate\Http\Response
     */
    public function destroy(PrReportComplaint $prReportComplaint)
    {
        //
    }
}
