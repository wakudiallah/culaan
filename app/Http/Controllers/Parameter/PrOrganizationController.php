<?php

namespace App\Http\Controllers\Parameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use App\Model\Parameter\PrOrganization;

class PrOrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $pr  = PrOrganization::where('status', 1)->get();
        return view('admin.parameter.pr_organization.index',compact('pr'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.parameter.pr_organization.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id   = Auth::user()->id;

        $data               = new PrOrganization; 
        $data->user_id      = $user_id; 
        $data->organization = $request->input('organization');
        $data->status       = '1';
        $data->save();

        return redirect('/administrator/parameter/add-pr-organization')->with('success', 'Data parameter berjaya disimpan'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pr  = PrOrganization::where('id', $id)->first();

        return view('admin.parameter.pr_organization.edit',compact('pr'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $organization         = $request->input('organization');

        PrOrganization::where('id', $id)->update(array('organization' => $organization )); 

        return redirect('/administrator/parameter/list-pr-organization')->with('success', 'Data parameter berjaya dikemaskini');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $emp = PrOrganization::findOrFail($id)->delete();   
                        
        return redirect('administrator/parameter/list-pr-organization')->with('warning', 'Data parameter berjaya dipadam'); 
    }
}
