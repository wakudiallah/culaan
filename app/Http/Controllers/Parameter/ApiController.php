<?php

namespace App\Http\Controllers\Parameter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Parameter\Postcode;
use Input;
use DB;
use Response;

class ApiController extends Controller
{
     public function postcode($id)
    {
        $data = Postcode::where('postcode',$id)->with('state')->limit(1)->get();
        return $data;
    }


    public function chairman_ic($ic)
    {
        $st2_0004=DB::table('st2_0004')->where('IC', 'LIKE', '%'.$ic.'%');
		$st2_0005=DB::table('st2_0005')->where('IC', 'LIKE', '%'.$ic.'%');
        $st2_6608=DB::table('st2_6608')->where('IC', 'LIKE', '%'.$ic.'%');

		$data = DB::table('st2_0005')->where('IC', 'LIKE', '%'.$ic.'%')
            ->union($st2_0004)
            ->union($st2_6608)
            ->limit(1)->get();
            return $data;


    }
    public function postcode2($id)
    {
        $data = Postcode::where('postcode',$id)->with('state')->limit(1)->get();
        return $data;
    }

    public function ic($ic)
    {
        $st2_0004=DB::table('st2_0004')->where('IC', 'LIKE', '%'.$ic.'%');
        $st2_0005=DB::table('st2_0005')->where('IC', 'LIKE', '%'.$ic.'%');
        $st2_6608=DB::table('st2_6608')->where('IC', 'LIKE', '%'.$ic.'%');

        $data = DB::table('st2_0005')->where('IC', 'LIKE', '%'.$ic.'%')
            ->union($st2_0004)
            ->union($st2_6608)
            ->limit(1)->get();
            return $data;
    }

    public function ic_survey($ic)
    {
        $st2_0004=DB::table('st2_0004')->where('IC', 'LIKE', '%'.$ic.'%');
        $st2_0005=DB::table('st2_0005')->where('IC', 'LIKE', '%'.$ic.'%');
        $st2_6608=DB::table('st2_6608')->where('IC', 'LIKE', '%'.$ic.'%');

        $data = DB::table('st2_0005')->where('IC', 'LIKE', '%'.$ic.'%')
            ->union($st2_0004)
            ->union($st2_6608)
            ->limit(1)->get();
            return $data;
    }

    public function ic_reg($ic)
    {
        $st2_0004=DB::table('st2_0004')->where('IC', 'LIKE', '%'.$ic.'%');
        $st2_0005=DB::table('st2_0005')->where('IC', 'LIKE', '%'.$ic.'%');
        $st2_6608=DB::table('st2_6608')->where('IC', 'LIKE', '%'.$ic.'%');

        $data = DB::table('st2_0005')->where('IC', 'LIKE', '%'.$ic.'%')
            ->union($st2_0004)
            ->union($st2_6608)
            ->limit(1)->get();
            return $data;
    }


}
