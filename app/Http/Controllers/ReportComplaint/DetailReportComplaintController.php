<?php
namespace App\Http\Controllers\ReportComplaint;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use PDF;
use App\Model\DetailReportComplaint;
use App\Model\DetailReportComplaintReply;
use App\Model\DetailImageReportComplaint;
use App\Model\Parameter\PrOrganization;
use App\Model\Parameter\PrReportComplaint;
use App\Model\Parameter\PrService;
use App\Model\Parameter\PrStage;
class DetailReportComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index($id)
    {   
        $user = Auth::User();
        $user_id = $user->id;
        $report  = DetailReportComplaint::where('pr_report_complaints_id',$id)->where(function ($query) use ($user_id) {
            $query->where('petugas_id', $user_id)
                  ->orwherenull('petugas_id');
                  })->orderBy('id', 'DESC')->get();
        $report2  = DetailReportComplaint::where('pr_report_complaints_id',$id)->where(function ($query) use ($user_id) {
            $query->where('petugas_id', $user_id)
                  ->orwherenull('petugas_id');
                  })->orderBy('id', 'DESC')->get();

        $stage = PrStage::where('status','1')->get();

        return view('admin.parliament.report.index',compact('report', 'report2','stage'));
    }

    public function action($id)
    {   
        $user = Auth::User();

        $org  = PrOrganization::where('status',1)->get();
        $report  = DetailReportComplaint::where('id',$id)->limit('1')->first();
        $pr_report_complaints  = PrReportComplaint::where('status',1)->get();
        $pr_service  = PrService::where('status',1)->get();
        $image = DetailImageReportComplaint::where('detail_report_complaints_id',$report->id)->get();

        return view('admin.parliament.report.action',compact('report', 'org','image','pr_report_complaints','pr_service'));
    }

    public function action_to($id)
    {   
        $user = Auth::User();

        $org  = PrOrganization::where('status',1)->get();
        $report  = DetailReportComplaint::where('id',$id)->limit('1')->first();
        $pr_report_complaints  = PrReportComplaint::where('status',1)->get();
        $pr_service  = PrService::where('status',1)->get();
        $image = DetailImageReportComplaint::where('detail_report_complaints_id',$report->id)->get();

        return view('admin.parliament.report.action_to',compact('report', 'org','image','pr_report_complaints','pr_service'));
    }

    public function view_location($id)
    {
        $report  = DetailReportComplaint::where('id',$id)->limit('1')->first();
        $image = DetailImageReportComplaint::where('detail_report_complaints_id',$report->id)->get();
        $lat=$report->lat;
        $lng = $report->lng;
        $geocode = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.$report->lat.','.$report->lng.'&key=AIzaSyCjyNfeJKhNl43VJox5jX0uRuf4N6jlHPA&callback=initMap';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $geocode);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $response = curl_exec($ch);
            curl_close($ch);
            $output = json_decode($response);
            $dataarray = get_object_vars($output);
            if ($dataarray['status'] != 'ZERO_RESULTS' && $dataarray['status'] != 'INVALID_REQUEST') {
                if (isset($dataarray['results'][0]->formatted_address)) {

                    $address = $dataarray['results'][0]->formatted_address;

                } else {
                    $address = 'Not Found';

                }
            } else {
                $address = 'Not Found';
            }

        return view('admin.parliament.report.location', compact('report','address','image'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function print($id)
    {
        $org  = PrOrganization::where('status',1)->get();
        $report  = DetailReportComplaint::where('id',$id)->limit('1')->first();
        $count = DetailImageReportComplaint::where('detail_report_complaints_id',$report->id)->count();
        $image = DetailImageReportComplaint::where('detail_report_complaints_id',$report->id)->get();
        $pdf = PDF::loadView('admin.parliament.report.print', compact( 'report', 'org','image','count'));
        $pdf->setPaper('A4', 'portrait');
            
        return $pdf->stream('Complaint.pdf', array('Attachment'=>false));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save_complaint($id, Request $request)
    {
        $user = Auth::user()->id;

        /*$status       = $request->input('status');
        $organization = $request->input('organization');
        $remark       = $request->input('remark');*/
        $pelayanan       = $request->input('pelayanan');

        $data                             = new DetailReportComplaintReply; 
        $data->detail_report_complaint_id = $id; 
        $data->remark                     = $request->input('remark');
        $data->user_id                    = $user;
        $data->status                     = $request->input('status');
        $data->organization_id            = $request->input('organization');
        $data->stage                      = 'ST01';
        $data->save();

        DetailReportComplaint::where('id', $id)->update(array('petugas_id' => $user, 'reply_id' => $data->id)); 

        return redirect('/administrator/report-complaint/index/'.$pelayanan)->with('success', 'Data berjaya disimpan'); 
    }

    public function send_sms($id, Request $request)
    {
        $user = Auth::user()->id;
        $pelayanan       = $request->input('pelayanan');
        /*$status       = $request->input('status');
        $organization = $request->input('organization');
        $remark       = $request->input('remark');*/
        $mobile       = $request->input('mobile');
        $remark       = $request->input('remark');

        $nexmo = app('Nexmo\Client');
            $nexmo->message()->send([
              'to' => "6".$mobile,
             'from' => 'P119',
             'text' => $remark,
              ]);


        return redirect('/administrator/report-complaint/index/'.$pelayanan)->with('success', 'Data berjaya disimpan'); 
    }


 
    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Parameter\DetailReportComplaint  $detailReportComplaint
     * @return \Illuminate\Http\Response
     */
    public function show(DetailReportComplaint $detailReportComplaint)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Parameter\DetailReportComplaint  $detailReportComplaint
     * @return \Illuminate\Http\Response
     */
    public function edit(DetailReportComplaint $detailReportComplaint)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Parameter\DetailReportComplaint  $detailReportComplaint
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetailReportComplaint $detailReportComplaint)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Parameter\DetailReportComplaint  $detailReportComplaint
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetailReportComplaint $detailReportComplaint)
    {
        //
    }
}
