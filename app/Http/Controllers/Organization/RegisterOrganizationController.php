<?php

namespace App\Http\Controllers\Organization;

use App\Model\PengerusiOrganization;
use App\Model\RegisterOrganization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Redirect;
use App\Model\Parameter\PrPosition;
use App\Model\Parameter\PrAddress;
use App\Model\Address;

class RegisterOrganizationController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {

        $org = RegisterOrganization::get();
        return view('admin.parliament.organization.index',compact('org'));
    }

    public function add()
    {
        return view('admin.parliament.organization.add',compact(''));
    }

     public function detail_organization($id)
    {
        $org = RegisterOrganization::where('org_reg_number',$id)->first();

        $ajk = PengerusiOrganization::where('org_id',$org->id)->get();
        $position = PrPosition::get();

        return view('admin.parliament.organization.detail',compact('org','ajk','position'));
    }

    public function save_detail(Request $request)
    {

        $data              =  new PengerusiOrganization;
        $data->org_id      =  $request->org_id;
        $data->ic          =  $request->ic;
        $data->position    =  $request->position;
        $data->name        =  $request->name;
        $data->phone       =  $request->phone;
        $data->NamaDM      =  $request->NamaDM_ajk;
        $data->NamaDUN     =  $request->NamaDUN_ajk;
        $data->NamaParlimen=  $request->NamaParlimen_ajk;
        $data->save();

          return Redirect::back()->with(['success' => 'Data telah tersimpan']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\RegisterNGO  $registerNGO
     * @return \Illuminate\Http\Response
     */
    public function show(RegisterOrganization $registerOrganization)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RegisterNGO  $registerNGO
     * @return \Illuminate\Http\Response
     */
    public function edit(RegisterOrganization $registerOrganization)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RegisterNGO  $registerNGO
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegisterOrganization $registerOrganization)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RegisterNGO  $registerNGO
     * @return \Illuminate\Http\Response
     */
    public function destroy(RegisterOrganization $registerOrganization)
    {
        //
    }

     public function save_register(Request $request)
    {
        $user_id   			= Auth::user()->id;
        $org_name           = $request->input('org_name');
        $org_reg_number     = $request->input('org_reg_number');
        $phone_number       = $request->input('phone_number');
        $email              = $request->input('email');
        $total_membership   = $request->input('total_membership');
        $chairman_ic        = $request->input('chairman_ic');
        $chairman_name      = $request->input('chairman_name');
        $chairman_phone     = $request->input('chairman_phone');
        $NamaDM             = $request->input('NamaDM');
        $NamaDUN            = $request->input('NamaDUN');
        $NamaParlimen       = $request->input('NamaParlimen');
        $user               = Auth::User();

        //$user   = Auth::user()->id;
        $id     = Uuid::uuid4()->tostring();

        $check_org = RegisterOrganization::where('org_reg_number',$org_reg_number)->count();
        $check_email = RegisterOrganization::where('email',$email)->count();

        if(($check_org=='0') && ($check_email=='0')){

            $org                    = new RegisterOrganization; 
            $org->user_id          	= $user_id;    
            $org->org_name          = $org_name;    
            $org->org_reg_number    = $org_reg_number;
            $org->phone_number      = $phone_number;
            $org->email             = $email;
            $org->total_membership  = $total_membership;
            $org->chairman_ic       = $chairman_ic;
            $org->chairman_name     = $chairman_name;
            $org->chairman_phone    = $chairman_phone;
            $org->NamaDM            = $NamaDM;
            $org->NamaDun           = $NamaDUN;
            $org->NamaParlimen      = $NamaParlimen;
            $org->petugas_id        = $user->id;
            $org->save();    

            $today          = date('Y-m-d H:i:s');
            $d              = date('d');
            $m              = date('m');
            $y              = date('y');
            $h              = date('h');
            $i              = date('i');

            $ref_no = $d.$m.$y.$h.$i.$org->id.'OG';

            $pr_address      = PrAddress::where('desc','Organization')->limit('1')->first();

            $data                   =  new Address;
            $data->ic               = $chairman_ic;
            $data->add_type         = $pr_address->code;
            $data->ref_no           = $ref_no;
            $data->status           =  '1';
            $data->save();

            $update_org = RegisterOrganization::where('id', $org->id)->update(array('ref_no' => $ref_no)); 


            return redirect('/administrator/organization/detail_organization/'.$org_reg_number)->with(['success' => 'Data Organisasi berjaya disimpan']);
        }
        else if (($check_org=='1') && ($check_email=='0'))
        {
            return redirect('/administrator/organization/add')->with(['success' => 'Nombor Pendaftaran Organisasi telah berdaftar']);
        }
        else if (($check_org=='0') && ($check_email=='1'))
        {
            return redirect('/administrator/organization/addr')->with(['success' => 'Alamat Emel telah berdaftar']);
        }
        else if (($check_org=='1') && ($check_email=='1'))
        {
            return redirect('/administrator/organization/add')->with(['success' => 'Organisasi telah berdaftar']);
        }
    }

     public function save(Request $request)
    {
        
        $org_name           = $request->input('org_name');
        $org_reg_number     = $request->input('org_reg_number');
        $phone_number       = $request->input('phone_number');
        $email              = $request->input('email');
        $total_membership   = $request->input('total_membership');
        $chairman_ic        = $request->input('chairman_ic');
        $chairman_name      = $request->input('chairman_name');
        $chairman_phone     = $request->input('chairman_phone');
        $NamaDM             = $request->input('NamaDM');
        $NamaDUN            = $request->input('NamaDUN');
        $NamaParlimen       = $request->input('NamaParlimen');
        $postcode           = $request->input('postcode');
        $address_1          = $request->input('address_1');
        $address_2          = $request->input('address_2');
        $address_3          = $request->input('address_3');
        $state              = $request->input('state');
        $city               = $request->input('city');
        $ref_no             = $request->input('ref_no');

        $user               = Auth::User();
        

        $update_org = RegisterOrganization::where('org_reg_number', $org_reg_number)->update(array('phone_number' => $phone_number,'total_membership' => $total_membership,'chairman_ic' => $chairman_ic,'chairman_name' => $chairman_name,'NamaDM' => $NamaDM,'NamaDUN' => $NamaDUN,'NamaParlimen' => $NamaParlimen)); 

        $update_address = Address::where('ref_no', $ref_no)->update(array('address_1' => $address_1,'address_2' => $address_2,'address_3' => $address_3,'state' => $state,'city' => $city,'postcode' => $postcode)); 

        return redirect('/administrator/organization/detail_organization/'.$org_reg_number)->with(['success' => 'Data Organisasi berjaya diperbaharui']);
       
    }
}
