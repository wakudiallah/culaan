<?php

namespace App\Http\Controllers;

use App\CIF;
use Illuminate\Http\Request;

class CIFController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CIF  $cIF
     * @return \Illuminate\Http\Response
     */
    public function show(CIF $cIF)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CIF  $cIF
     * @return \Illuminate\Http\Response
     */
    public function edit(CIF $cIF)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CIF  $cIF
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CIF $cIF)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CIF  $cIF
     * @return \Illuminate\Http\Response
     */
    public function destroy(CIF $cIF)
    {
        //
    }
}
