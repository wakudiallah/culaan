<?php

namespace App\Http\Controllers\InformationPublic;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\CIF;
use App\Model\SickInfo;
use App\Model\Parameter\Disease;
use DB;
use Auth;
use Illuminate\Support\Facades\Hash;
use Session;
use App\Model\Parameter\Parlimen;
use App\Model\Parameter\Dun;
use App\Model\Parameter\State;
use App\Model\Parameter\PrAddress;
use App\Model\Address;
use App\Model\Survey;

class SaveInformationController extends Controller
{
	 public function __construct()
    {
        $this->middleware('auth');
    }

     public function save(Request $request)
    {
        $user = Auth::User();

        $ic             = $request->input('ic');
        $name           = $request->input('name');
        $ICLama         = $request->input('ICLama');
        $mobile         = $request->input('mobile');
        $mobile2        = $request->input('mobile2');
        $telephone      = $request->input('telephone');
        $email          = $request->input('email');
        $dob            = $request->input('dob');

        $Kodlokaliti    = $request->input('kodlokaliti');
        $NamaLokaliti   = $request->input('lokaliti');
        $NamaDM         = $request->input('dms');
        $NamaDun        = $request->input('dun');
        $NamaParlimen   = $request->input('NamaParlimen');
        $Negeri         = $request->input('Negeri');
        $NoRumah      	= $request->input('norumah');

        $issue          = $request->input('issue');
        $rumah          = $request->input('rumah');
        $postcode       = $request->input('postcode');
        $address_1      = $request->input('address_1');
        $address_2      = $request->input('address_2');
        $address_3      = $request->input('address_3');
        $state          = $request->input('state');
        $city           = $request->input('city');

        $gender         = $request->input('gender');
        $marital_status = $request->input('marital_status');
        $residence      = $request->input('residence');
        $occupation_id  = $request->input('occupation_id');
        $no_dependants  = $request->input('no_dependants');
        $school_dep     = $request->input('school_dep');
        $working_dep    = $request->input('working_dep');
        $parents_dep    = $request->input('parents_dep');
        $unemployee_dep = $request->input('unemployee_dep');
        $beneficiary    = $request->input('beneficiary');
        $remark_beneficiary = $request->input('remark_beneficiary');
        $kecenderungan = $request->input('kecenderungan');
       
        $user_id     	= $user->id;
        $hash           = Hash::make($ic); 

        $count_cif      = Cif::where('ic',$ic)->count();
        $total          = $school_dep + $working_dep + $unemployee_dep;

        if($no_dependants== $total) {

            if($count_cif==0){
                $cif                  = new CIF;
                $cif->ic              = $ic;
                $cif->user_id 	      = $user_id;
                $cif->name            = $name;
                $cif->ICLama          = $ICLama;
                $cif->mobile          = $mobile;
                $cif->mobile2         = $mobile2;
                $cif->telephone       = $telephone;
                $cif->email           = $email;
                $cif->dob             = $dob;
                $cif->ic              = $ic;
                $cif->Kodlokaliti     = $Kodlokaliti;
                $cif->NamaLokaliti    = $NamaLokaliti;
                $cif->NamaDM          = $NamaDM;
                $cif->NamaDun         = $NamaDun;
                $cif->NamaParlimen    = $NamaParlimen;
                $cif->Negeri          = $Negeri;
                $cif->NoRumah         = $NoRumah;
                $cif->issue           = $issue;
                $cif->gender          = $gender;
                $cif->marital_status  = $marital_status;
                $cif->residence       = $residence;
                $cif->occupation_id   = $occupation_id;
                $cif->no_dependants   = $no_dependants;
                $cif->school_dep      = $school_dep;
                $cif->working_dep     = $working_dep;
                $cif->parents_dep     = $parents_dep;
                $cif->unemployee_dep  = $unemployee_dep;
                $cif->beneficiary     = $beneficiary;
                $cif->remark_beneficiary = $remark_beneficiary;
                $cif->kecenderungan   =  $kecenderungan;
                $cif->status          =  '1';
                $cif->save();


                $today          = date('Y-m-d H:i:s');
                $d              = date('d');
                $m              = date('m');
                $y              = date('y');
                $h              = date('h');
                $i              = date('i');

                $ref_no = $d.$m.$y.$h.$i.$cif->id.'MR';

                $update_cif = CIF::where('id', $cif->id)->update(array('ref_no' => $ref_no)); 

                $data                   =  new Survey;
                $data->user_id          =  $user_id;
                $data->ic               =  $ic;
                $data->ref_no           =  $ref_no;
                $data->name             =  $name;
                $data->phone            =  $mobile;
                $data->Kodlokaliti      =  $Kodlokaliti;
                $data->NamaLokaliti     =  $NamaLokaliti;
                $data->NamaDM           =  $NamaDM;
                $data->NamaDUN          =  $NamaDun;
                $data->NamaParlimen     =  $NamaParlimen;
                $data->Negeri           =  $Negeri;
                $data->kecenderungan    =  $kecenderungan;
                $data->ket              =  'PRU';
                $data->code_dun         =  'PRU';
                $data->code_parlimen    =  'PRU';
                $data->status           =  '1';
                $data->save();

                $pr_address      = PrAddress::where('desc','Maklumat Rakyat')->limit('1')->first();

                $data                   =  new Address;
                $data->ic               = $ic;
                $data->add_type         = $pr_address->code;
                $data->ref_no           = $ref_no;
                $data->address_1        = $address_1;
                $data->address_2        = $address_2;
                $data->address_3        = $address_3;
                $data->postcode         = $postcode;
                $data->state            = $state;
                $data->city             = $city;
                $data->status           =  '1';
                $data->save();


                if(!empty($request->disease_type)){
                	$sizegroup = array_map(null,  $request->x,$request->disease_type); 
    	            foreach ($sizegroup as $val) 
    	                {
    	                    $sick_info                 = new SickInfo;
    	                    $sick_info->ic            = $ic;
    	                    $sick_info->user_id       = $user->id;
    	                    $sick_info->disease_type  = $val[1];
    	                    $sick_info->save();
    	                }
                }
                
                return redirect('/administrator/information_public/index')->with(['success' => 'IC  '.$ic.' telah tersimpan']);
            }
            else{
                return redirect('/administrator/information_public/index');
            }
        }
        else
        {
            Session::flash('mobile', $mobile);
            Session::flash('mobile2', $mobile2);
            Session::flash('telephone', $telephone);
            Session::flash('email', $email);
            Session::flash('address_1', $address_1);
            Session::flash('address_2', $address_2);
            Session::flash('address_3', $address_3);
            Session::flash('postcode', $postcode);
            Session::flash('state', $state);
            Session::flash('city', $city); 
            Session::flash('issue', $issue);
            Session::flash('beneficiary', $beneficiary);
            Session::flash('remark_beneficiary', $remark_beneficiary);
            Session::flash('no_dependants', $no_dependants); 
            Session::flash('school_dep', $school_dep);
            Session::flash('working_dep', $working_dep);
            Session::flash('unemployee_dep', $unemployee_dep);
             return redirect('/administrator/information_public/add/'.$ic)->with(['success' => 'Jumlah Tanggungan tidak sepadan']);
        }
    }

    public function save_new(Request $request)
    {
        $user = Auth::User();

        $ic             = $request->input('ic');
        $name           = $request->input('name');
        $ICLama         = $request->input('ICLama');
        $mobile         = $request->input('mobile');
        $mobile2         = $request->input('mobile2');
        $email          = $request->input('email');
        $dob            = $request->input('dob');
        $telephone      = $request->input('telephone');
        $Kodlokaliti    = $request->input('kodlokaliti');
        $NamaLokaliti   = $request->input('lokaliti');
        $NamaDM         = $request->input('dms');
        $NoRumah        = $request->input('norumah');
        $Negeri         = $request->input('state_name');
        $kecenderungan = $request->input('kecenderungan');

        $kod_parlimen = $request->input('parlimen_name');
        $code_parlimen   = substr($kod_parlimen,0, 4); 

        $parlimen = $kod_parlimen;    
        $parlimen_name = substr($parlimen, strpos($parlimen, "|") );     

        $kod_dun = $request->input('dun_name');
        $dun_code   = substr($kod_dun,0, 3);

        $dun = $kod_dun;    
        $dun_name = substr($dun, strpos($dun, "|") + 1 ); 

        $pr  = Parlimen::where('code',$code_parlimen)->limit('1')->first();
        $name_pr = $pr->parlimen_name;

        $tanggal  = substr($ic,4, 2);
        $bulan    = substr($ic,2, 2);
        $tahun    = substr($ic,0, 2); 
        $jantina  = substr($ic,10, 2); 

        if($tahun > 30) 
            {
              $tahun2 = "19".$tahun;
            }
        else 
            {
              $tahun2 = "19".$tahun;
            }

        $lahir = $tahun2.'-'.$bulan.'-'.$tanggal;


        if($jantina % 2 ==0 )
        {
            $gender   =  "P" ;
        }
        else
        {
            $gender   =  "L" ;
        }

        if(($Negeri=='SELANGOR')){

            $dn  = Dun::where('code',$dun_code)->limit('1')->first();
            $name_dn = $dn->dun_name;
        }
        else if(($Negeri=='PERLIS')){

            $dn  = Dun::where('code',$dun_code)->limit('1')->first();
            $name_dn = $dn->dun_name;
        }
        else if(($Negeri=='KEDAH')){

            $dn  = Dun::where('code',$dun_code)->limit('1')->first();
            $name_dn = $dn->dun_name;
        }
        else if(($Negeri=='KELANTAN')){

            $dn  = Dun::where('code',$dun_code)->limit('1')->first();
            $name_dn = $dn->dun_name;
        }
        else if(($Negeri=='MELAKA')){

            $dn  = Dun::where('code',$dun_code)->limit('1')->first();
            $name_dn = $dn->dun_name;
        }
        else if(($Negeri=='PULAU PINANG')){

            $dn  = Dun::where('code',$dun_code)->limit('1')->first();
            $name_dn = $dn->dun_name;
        }
        else if(($Negeri=='PAHANG')){

            $dn  = Dun::where('code',$dun_code)->limit('1')->first();
            $name_dn = $dn->dun_name;
        }
        else if(($Negeri=='PERAK')){

            $dn  = Dun::where('code',$dun_code)->limit('1')->first();
            $name_dn = $dn->dun_name;
        }else if(($Negeri=='SABAH')){

            $dn  = Dun::where('code',$dun_code)->limit('1')->first();
            $name_dn = $dn->dun_name;
        }
        else if(($Negeri=='SERAWAK')){

            $dn  = Dun::where('code',$dun_code)->limit('1')->first();
            $name_dn = $dn->dun_name;
        }
        else if(($Negeri=='JOHOR')){

            $dn  = Dun::where('code',$dun_code)->limit('1')->first();
            $name_dn = $dn->dun_name;
        }
        else if(($Negeri=='TERENGGANU')){

            $dn  = Dun::where('code',$dun_code)->limit('1')->first();
            $name_dn = $dn->dun_name;
        }
        else if(($Negeri=='NEGERI SEMBILAN')){

            $dn  = Dun::where('code',$dun_code)->limit('1')->first();
            $name_dn = $dn->dun_name;
        }
        else{
            $name_dn='';
        }

        $issue          = $request->input('issue');
        $rumah          = $request->input('rumah');
        $address_1      = $request->input('address_1');
        $address_2      = $request->input('address_2');
        $address_3      = $request->input('address_3');
        $postcode       = $request->input('postcode');
        $state          = $request->input('state');
        $city           = $request->input('city');

        $marital_status = $request->input('marital_status');
        $occupation_id  = $request->input('occupation_id');
        $residence      = $request->input('residence');
        $no_dependants  = $request->input('no_dependants');
        $school_dep     = $request->input('school_dep');
        $working_dep    = $request->input('working_dep');
        $parents_dep    = $request->input('parents_dep');
        $unemployee_dep = $request->input('unemployee_dep');
        $beneficiary    = $request->input('beneficiary');
        $remark_beneficiary = $request->input('remark_beneficiary');
       
        $user_id        = $user->id;
        $hash           = Hash::make($ic); 

        $count_cif      = Cif::where('ic',$ic)->count();
        $total          = $school_dep + $working_dep + $unemployee_dep;

        if($no_dependants== $total) {

            if($count_cif==0){
                $cif                  = new CIF;
                $cif->ic              = $ic;
                $cif->user_id         = $user_id;
                $cif->name            = $name;
                $cif->ICLama          = $ICLama;
                $cif->mobile          = $mobile;
                $cif->mobile2         = $mobile2;
                $cif->telephone       = $telephone;
                $cif->email           = $email;
                $cif->dob             = $lahir;
                $cif->ic              = $ic;
                $cif->Kodlokaliti     = $Kodlokaliti;
                $cif->NamaLokaliti    = $NamaLokaliti;
                $cif->NamaDM          = $NamaDM;
                $cif->NamaDun         = $name_dn;
                $cif->NamaParlimen    = $name_pr;
                $cif->Negeri          = $Negeri;
                $cif->NoRumah         = $NoRumah;
                $cif->address         = $address;
                $cif->postcode        = $postcode;
                $cif->state           = $state;
                $cif->city            = $city;
                $cif->issue           = $issue;
                $cif->gender          = $gender;
                $cif->marital_status  = $marital_status;
                $cif->occupation_id   = $occupation_id;
                $cif->no_dependants   = $no_dependants;
                $cif->school_dep      = $school_dep;
                $cif->working_dep     = $working_dep;
                $cif->parents_dep     = $parents_dep;
                $cif->unemployee_dep  = $unemployee_dep;
                $cif->beneficiary     = $beneficiary;
                $cif->remark_beneficiary = $remark_beneficiary;
                $cif->kecenderungan   =  $kecenderungan;
                $cif->status          =  '1';
                $cif->save();
                
                $today          = date('Y-m-d H:i:s');
                $d              = date('d');
                $m              = date('m');
                $y              = date('y');
                $h              = date('h');
                $i              = date('i');

                $ref_no = $d.$m.$y.$h.$i.$cif->id.'MR';

                $update_cif = CIF::where('id', $cif->id)->update(array('ref_no' => $ref_no)); 
               
                $data                   =  new Survey;
                $data->user_id          =  $user_id;
                $data->ref_no           =  $ref_no;
                $data->ic               =  $ic;
                $data->name             =  $name;
                $data->phone            =  $mobile;
                $data->Kodlokaliti      =  $Kodlokaliti;
                $data->NamaLokaliti     =  $NamaLokaliti;
                $data->NamaDM           =  $NamaDM;
                $data->NamaDUN          =  $name_dn;
                $data->NamaParlimen     =  $name_pr;
                $data->Negeri           =  $Negeri;
                $data->kecenderungan    =  $kecenderungan;
                $data->ket              =  'PRU';
                $data->code_dun         =  'PRU';
                $data->code_parlimen    =  'PRU';
                $data->status           =  '1';
                $data->save();

                $pr_address      = PrAddress::where('desc','Maklumat Rakyat')->limit('1')->first();

                $address                   =  new Address;
                $address->ic               = $ic;
                $address->add_type         = $pr_address->code;
                $address->ref_no           = $ref_no;
                $address->address_1        = $address_1;
                $address->address_2        = $address_2;
                $address->address_3        = $address_3;
                $address->postcode         = $postcode;
                $address->state            = $state;
                $address->city             = $city;
                $address->status           =  '1';
                $address->save();

                if(!empty($request->disease_type)){
                    $sizegroup = array_map(null,  $request->x,$request->disease_type); 
                    foreach ($sizegroup as $val) 
                        {
                            $sick_info                 = new SickInfo;
                            $sick_info->ic            = $ic;
                            $sick_info->user_id       = $user->id;
                            $sick_info->disease_type  = $val[1];
                            $sick_info->save();
                        }
                }
                
                return redirect('/administrator/information_public/index')->with(['success' => 'IC  '.$ic.' telah tersimpan']);
            }
            else{
                return redirect('/administrator/information_public/index');
            }
        }
        else
        {
            Session::flash('name', $name); 
            Session::flash('ICLama', $ICLama);
            Session::flash('mobile', $mobile);
            Session::flash('email', $email);
            Session::flash('lokaliti', $NamaLokaliti); 
            Session::flash('dm', $NamaDM);
            Session::flash('state_name', $Negeri);
            Session::flash('mobile2', $mobile2);
            Session::flash('telephone', $telephone);
            Session::flash('address', $address);
            Session::flash('postcode', $postcode);
            Session::flash('state', $state);
            Session::flash('city', $city); 
            Session::flash('issue', $issue);
            Session::flash('beneficiary', $beneficiary);
            Session::flash('remark_beneficiary', $remark_beneficiary);
            Session::flash('no_dependants', $no_dependants); 
            Session::flash('school_dep', $school_dep);
            Session::flash('working_dep', $working_dep);
            Session::flash('unemployee_dep', $unemployee_dep);

             return redirect('/administrator/information_public/add/'.$ic)->with(['success' => 'Jumlah Tanggungan tidak sepadan']);
        }
    }
}
