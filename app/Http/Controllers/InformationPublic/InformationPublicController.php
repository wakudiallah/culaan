<?php

namespace App\Http\Controllers\InformationPublic;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\CIF;
use App\Model\Parameter\Disease;
use App\Model\Parameter\PrOccupation;
use App\Model\Parameter\PrMarital;
use DB;
use Auth;
use App\Model\Parameter\Parlimen;
use App\Model\Parameter\Dun;
use App\Model\Parameter\State;
use App\Model\Parameter\PrResidence;
use App\Model\SickInfo;
class InformationPublicController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
 
    public function index()
    {
    	return view('admin.parliament.information_public.index',compact(''));
    }

    public function list()
    {
        $cif = CIF::get();
        return view('admin.parliament.information_public.list',compact('cif'));
    }

    public function detail($ic)
    {
         
        $ics     = substr($ic,0,4);
        $i       = substr($ic,0,1);
        $s       ='0010';
        $e       ='1708'; 
        $cif     = Cif::where('ic',$ic)->limit(1)->first();
        $sick    = SickInfo::where('ic',$ic)->get();
        $occupation= PrOccupation::orderby('desc','ASC')->get();
        $marital   = PrMarital::orderby('id','ASC')->get();
        $pr        = Parlimen::where('status', 1)->get();
        $state     = State::get();
        $dun       = Dun::get();
        $residence = PrResidence::where('status', 1)->get();
        $disease   = Disease::orderby('disease_desc','ASC')->get();
        return view('admin.parliament.information_public.detail',compact('ic','ics','queries','i','religion','marital','cif','occupation','disease','sick','occupation','marital','residence'));
    }

    public function searching(Request $request)
    {
        $ic = $request->input('search');
        $ic_s = substr($ic[0],3);

        $cif = CIF::where('ic',$ic)->count();

        if($cif=='0'){
            return redirect('/administrator/information_public/add/'.$ic)->with(['success' => 'IC tersedia']);
        }
        else{
        	$get_cif = CIF::where('ic',$ic)->first();
            $name_petugas = $get_cif->petugas->name;

            return redirect('/administrator/information_public/detail/'.$ic)->with(['success' => 'IC telah terdaftar oleh petugas bernama : '.$name_petugas]);
        }
    }

    public function add($ic)
    {
        $disease      = Disease::orderby('disease_desc','ASC')->get();
        $occupation   = PrOccupation::orderby('desc','ASC')->get();
        $marital   = PrMarital::orderby('id','ASC')->get();
        $pr  = Parlimen::where('status', 1)->get();
        $state  = State::get();
        $dun  = Dun::get();
        $residence  = PrResidence::where('status', 1)->get();
		
		$st2_0004=DB::table('st2_0004')->where('IC', 'LIKE', '%'.$ic.'%')->orwhere('ICLama', 'LIKE', '%'.$ic.'%')->orwhere('Nama', 'LIKE', '%'.$ic.'%');
		$st2_0005=DB::table('st2_0005')->where('IC', 'LIKE', '%'.$ic.'%')->orwhere('ICLama', 'LIKE', '%'.$ic.'%')->orwhere('Nama', 'LIKE', '%'.$ic.'%');
        $st2_6608=DB::table('st2_6608')->where('IC', 'LIKE', '%'.$ic.'%')->orwhere('ICLama', 'LIKE', '%'.$ic.'%')->orwhere('Nama', 'LIKE', '%'.$ic.'%');

		$queries = DB::table('st2_0005')->where('IC', 'LIKE', '%'.$ic.'%')->orwhere('ICLama', 'LIKE', '%'.$ic.'%')->orwhere('Nama', 'LIKE', '%'.$ic.'%')
            ->union($st2_0004)
            ->union($st2_6608)
            ->get();

        $count = DB::table('st2_0005')->where('IC', 'LIKE', '%'.$ic.'%')->orwhere('ICLama', 'LIKE', '%'.$ic.'%')->orwhere('Nama', 'LIKE', '%'.$ic.'%')
            ->union($st2_0004)
            ->union($st2_6608)
            ->count();

            if($count=='1'){
                return view('admin.parliament.information_public.add',compact('ic','disease','ics','queries','occupation','marital','pr','state','dun','residence'))->with(['success' => 'IC tersedia']);
            }
            else if($count>'1'){
                return view('admin.parliament.information_public.list_ic',compact('ic','disease','ics','queries','count','occupation','marital','pr','state','dun','residence'));
            }
            else if($count=='0'){
            	return view('admin.parliament.information_public.add_new',compact('ic','disease','ics','queries','count','occupation','marital','pr','state','dun','residence'))->with(['success' => 'IC tidak ditemukan dalam database']);
            }
    }
}
