<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use App\User;
use App\Model\Parameter\PrReportComplaint;
use App\Model\Parameter\PrType;
use App\Model\DetailReportComplaint;
use App\Model\DetailImageReportComplaint;
use App\Model\UserDetail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Mail;
use Slice;
use App\Model\Prnegeri;
use App\Model\Prk;
use App\Model\Main\Slider;
use App\Model\Main\News;
use App\Model\Event;
use App\Model\Main\Gallery;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $prk    = PRK::where('status','1')->orderby('created_at','DESC')->limit('1')->first();
        $slider = Slider::where('status','1')->orderby('created_at','DESC')->limit(3)->get();
        $news   = News::where('status','1')->orderby('created_at','DESC')->latest('updated_at')->take(4)->get();

        $news_ids= $news->pluck('id')->toArray();

        $other_news   = News::where('status','1')->orderby('created_at','DESC')->whereNotIn('id', $news->pluck('id'))->limit(10)->get();

        //dd($news_ids);
        
        return view('index', compact('prk', 'slider', 'news','other_news'));  
    }

    
    public function detail_berita($id)
    {       
       
       $news   = News::where('id',$id)->first();

        return view('main.detail_berita', compact('news'));  
    }

  

 


    public function gallery()
    {       
       

       $gallery   = Gallery::where('status','1')->orderby('created_at','DESC')->latest('updated_at')->take(8)->get();

        return view('main.gallery.index', compact('gallery', 'other_event'));  
    }



    public function register()
    {       
        $type = PrType::get();
         $prk = PRK::where('status','1')->orderby('created_at','DESC')->limit('1')->first();
        return view('main.register', compact('type','prk'));  
    }

      public function post_register(Request $request)
    {
        
        $name           = $request->input('name');
        $email          = $request->input('email');
        $ic             = $request->input('ic');
        //$type           = $request->input('type');
        //$address        = $request->input('address');
        //$postcode       = $request->input('postcode');
        //$city           = $request->input('city');
        //$state          = $request->input('state');
        $mobile_phone   = $request->input('mobile_phone');
        $password = $request->input('password');
       
        $password2      = bcrypt($password);

        //$user   = Auth::user()->id;
        $id     = Uuid::uuid4()->tostring();

        $check_email = User::where('email',$email)->count();

        if($check_email=='0'){
            $data           = new User; 
            $data->user_id  = $id;    
            $data->name     = $request->name;
            $data->email    = $request->email;
            $data->name     = $request->name;
            $data->password = $password2;
            $data->role     = '2';
            $data->save();


            $detail                 = new UserDetail; 
            $detail->user_id        = $id;    
            $detail->ic             = $ic;
            $detail->fullname       = $name;
            //$detail->address        = $address;
            //$detail->postcode       = $postcode;
            //$detail->city           = $city;
            //$detail->state          = $state;
            $detail->phone   = $mobile_phone;
           // $detail->type           = $type;
            $detail->Kodlokaliti    = $request->kodlokaliti_reg;
            $detail->NamaLokaliti   = $request->NamaLokaliti_reg;
            $detail->NamaDM         = $request->NamaDM_reg;
            $detail->NamaDUN        = $request->NamaDUN_reg;
            $detail->NamaParlimen   = $request->NamaParlimen_reg;
            $detail->Negeri         = $request->Negeri_reg;
            $detail->lat            = $request->latitude;
            $detail->lng            = $request->longitude;
            $detail->location       = $request->location;
            $detail->ip             = $request->ip;
            $detail->save();    

           
            //$data->user_id = $user;
            Mail::send('mail.register', compact('name','password','email'), function ($message) use ($email) 
            {
                $message->from('admin@semarak.insko.my', 'SEMARAK');
                $message->subject('New User Created - SEMARAK');
                $message->to($email);
            });
            
            $credentials     = array(
                'email'     => $email,
                'password' => $password );
                    if (Auth::attempt($credentials)) 
                        {
                            return redirect('/')->with(['success' => 'Tahniah, Anda telah terdaftar']);
                        } 

        }
        else{
            return redirect('/account-register')->with(['warning' => 'Alamat Emel sudah terdaftar, sila cuba menggunakan emel lain']);
        }
        
        
        
        $data->save();

    }

     public function event()
    {
        $event = Event::all();
        return view('main.event.index', compact('event'));
    }

    public function detail_event($id)
    {
        $event  = Event::where('id', $id)->first();
        return view('main.event.detail',compact('event'));
    }

}
