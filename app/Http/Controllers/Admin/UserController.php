<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;
use App\User;
use App\Model\Role\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Mail;

class UserController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }


     public function index()
    {
    	$user = User::where('role','!=','0')->get();
        return view('admin.user.index',compact('user'));
    }

     public function add()
    {
    	$role = Role::get();
        return view('admin.user.add',compact('role'));
    }
    
    public function save(Request $request)
    {

        $role       = $request->input('role');
        $name       = $request->input('name');
        $email      = $request->input('email');
        $password   = $request->input('password');
        $phone      = $request->input('phone');

        $user =  User::where('email',$email)->count();
        if($user=='0'){


    	$hash_password = Hash::make($password); 

    	 	$new           	= new User;
            $new->name     	=  $name;
            $new->email    	=  $email;
            $new->password 	=  $hash_password;
            $new->phone   	=  $phone;
            $new->role  	=  $role;
            $new->save();

                /*Mail::send('mail.register', compact('name','password',"email"), function ($message) use ($email) 
                {
                    $message->from('admin@n24.insko.my', 'Parti Pribumi Bersatu Malaysia');
                    $message->subject('New User Created - Parti Pribumi Bersatu Malaysia');
                    $message->to($email);
                });*/

            /*$nexmo = app('Nexmo\Client');
            $nexmo->message()->send([
              'to' => "60142449179",
             'from' => 'Icop Angkasa',
             'text' => "Thanks for registration,please login ",
              ]);*/

         return redirect('/administrator/user/index')->with(['success' => 'Pengguna telah berjaya didaftarkan']);
         }
         else{
             return redirect('/administrator/user/index')->with(['error' => 'Pengguna dengan alamat emel ini telah berdaftar']);
         }
    }
}
