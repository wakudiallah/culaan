<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HistoryComplaint extends Model
{
    protected $table 	= 'history_complaints';

    public function status_complaint()
    {
        return $this->belongsTo('App\Model\Parameter\PrHistoryComplaint','history_id','id_history');	
    }

    public function associate_id()
    {
        return $this->belongsTo('App\Model\DetailReportComplaint', 'id', 'complaint_id');
    }
}
