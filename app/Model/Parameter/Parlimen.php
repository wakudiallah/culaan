<?php

namespace App\Model\Parameter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Parlimen extends Model
{
    protected $table = 'param_parlimens';

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;

	public function state()
    {
        return $this->belongsTo('App\Model\Parameter\State','state_code', 'state_code');  
    }
}
