<?php

namespace App\Model\Parameter;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'pr_state';
}
