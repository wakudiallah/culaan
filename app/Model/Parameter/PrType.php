<?php

namespace App\Model\Parameter;

use Illuminate\Database\Eloquent\Model;

class PrType extends Model
{
    protected $table 	= 'pr_types';
}
