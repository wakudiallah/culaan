<?php

namespace App\Model\Parameter;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class PrRole extends Model
{
    protected $table 	= 'pr_roles';

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;

	  public function roles()
    {
        return $this->belongsTo('App\Model\Parameter\PrRoleDetail','id', 'role_id');  
    }
}

