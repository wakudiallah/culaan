<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Prk extends Model
{
    protected $table = 'prks';

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;

	public function name_parlimen()
    {
        return $this->belongsTo('App\Model\Parameter\Parlimen','parlimen_code', 'code');  
    }

    public function name_dun()
    {
        return $this->belongsTo('App\Model\Parameter\Dun','dun_code', 'code');  
    }
}