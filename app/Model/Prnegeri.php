<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Prnegeri extends Model
{
    protected $table = 'prnegeris';

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;


	public function name_parlimen()
    {
        return $this->belongsTo('App\Model\Parameter\Parlimen','parlimen_code', 'code');  
    }

    public function name_dun()
    {
        return $this->belongsTo('App\Model\Parameter\Dun','dun_code', 'code');  
    }
}
