<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PengerusiOrganization extends Model
{
    protected $table 	= 'pengerusi_organizations';

     public function jawatan()
    {
        return $this->belongsTo('App\Model\Parameter\PrPosition','position','id');	
    }
}
