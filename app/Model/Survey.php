<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $table 	= 'surveys';

      public function user_petugas()
    {
        return $this->belongsTo('App\User','user_id','id');	
    }
}
