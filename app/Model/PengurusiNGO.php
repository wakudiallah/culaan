<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PengurusiNGO extends Model
{
    protected $table 	= 'pengurusi_ngos';

     public function jawatan()
    {
        return $this->belongsTo('App\Model\Parameter\PrPosition','position','id');	
    }
}
