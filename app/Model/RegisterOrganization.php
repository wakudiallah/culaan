<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RegisterOrganization extends Model
{
    protected $table 	= 'register_organizations';

     public function Address()
    {
        return $this->belongsTo('App\Model\Address','ref_no', 'ref_no');  
    }
}
