<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RegisterNGO extends Model
{
    protected $table 	= 'register_ngos';

     public function Address()
    {
        return $this->belongsTo('App\Model\Address','ref_no', 'ref_no');  
    }
}
