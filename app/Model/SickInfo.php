<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SickInfo extends Model
{
    protected $table 	= 'sick_infos';

    public function disease()
    {
        return $this->belongsTo('App\Model\Disease','disease_type','disease_code');	
    }
}
