<?php

return [
	'home' => 'Home',
	'login' => 'Login',
	'logout' => 'Logout',
	'about' => 'About Us',
	'faq' => 'FAQ',
	'contact' => 'Contact',
	'information'=>'Information',
	'beneficiary'=>'List of Beneficiary',
	'gallery'=> 'Gallery',
	'service'=>'Services',
	'know'=>'Did you know',
	'proposal'=>'Proposal',
	'help'=>'Bantuan',
	'complaint'=>'Complaints',
	'event'=>'Event',
	'list'		=> 'List'
];