<?php

return [
	'register' => 'Register',
	'name' => 'Name',
	'ic' => 'IC Number',
	'phone' => 'Mobile Phone',
	'address' => 'Address',
	'postcode' => 'Postcode',
	'city' => 'City',
	'state' => 'State',
	'email' => 'E-mail',
	'password' => 'Password',
	'personal' => 'Individu',
	'party' => 'Party',
	'as' => 'Register as',
];