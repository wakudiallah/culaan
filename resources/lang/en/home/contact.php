<?php

return [
	'contact' => 'Contact Us',
	'sentence' => 'There are many ways to contact us. You may drop us a line, give us a call or send an email, choose what suits you the most',
	'weekend' => 'Saturday - Sunday: Closed',
	'weekday'=>'Open hours: 8.00-18.00 Mon-Fri',
	'get' => 'Get In Touch With Us!',
	'fill' => 'Fill out the form below to recieve a free and confidential.',
	'name' => 'Name',
	'email' => 'Email',
	'message' => 'Message',
	'send' => 'Send Message'
];