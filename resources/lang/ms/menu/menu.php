<?php

return [
	'home' => 'Utama',
	'login' => 'Masuk',
	'logout' => 'Keluar',
	'about' => 'Tentang Kami',
	'faq' => 'FAQs',
	'contact' => 'Hubungi Kami',
	'information'=>'Maklumat',
	'beneficiary'=>'Senarai Bantuan',
	'gallery'=> 'Galeri',
	'service'=>'Pelayanan',
	'know'=>'Tahukah Anda',
	'proposal'=>'Cadangan',
	'help'=>'Bantuan',
	'complaint'=>'Aduan',
	'event'=>'Acara',
	'list'		=> 'Senarai'
];