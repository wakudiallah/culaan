<?php

return [
	'aduan' => 'Aduan',
	'service' => 'Perkhidmatan',
	'welcome' => 'SELAMAT DATANG PADA SMARAK',
	'welcome2' => 'Sistem Maklumat Rakyat - Malaysia',
	'day' => 'Hari',
	'hour' => 'Jam',
	'min' => 'Minit',
	'sec' => 'Saat',
	'create' => 'Buat Aduan Baharu',
	'list' => 'Semak Senarai Aduan',
	'start' => 'Bermula',
];