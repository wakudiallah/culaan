<div style="font-family: Helvetica Neue, Helvetica, Helvetica, Arial, sans-serif;">
    <table style="width: 100%;">
        <tr>
            <td></td>
            <td bgcolor="#FFFFFF ">
                <div style="padding: 15px; max-width: 600px;margin: 0 auto;display: block; border-radius: 0px;padding: 0px; border: 1px solid lightseagreen;">
                    <table style="width: 100%;background: #e51f11 ;">
                        <tr>
                            <td></td>
                            <td>
                                <div>
                                    <table width="100%">
                                        <tr>
                                            <td rowspan="2" style="text-align:center;padding:10px;">
                							     <img style="float:left;"  src="{{asset('/img/semarak.png')}}" width="208px" height="69px" />
                                                 <span style="color:white;float:right;font-size: 13px;font-style: italic;margin-top: 20px; padding:10px; font-size: 14px; font-weight:normal;">"S  M A R A K"<span></span></span></td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td></td>
                        </tr>
                    </table>
                    <table style="padding: 10px;font-size:14px; width:100%;color: #151515">
                        <tr>
                            <td style="padding:10px;font-size:14px; width:100%;">
                                <p>Hello {{$name}}</p>
                                <p>You have successfully registered for SMARAK system. For login please click <a href="https://www.smarak.insko.my/login"> here</a> and login with email and password below. </p>
                                <table border="0" style="font-weight: bold">
                                    <tr>
                                        <td>Name</td>
                                        <td>:</td>
                                        <td>{{$name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>:</td>
                                        <td>{{$email}}</td>
                                    </tr>
                                    <tr>
                                        <td>Password</td>
                                        <td>:</td>
                                        <td>{{$password}}</td>
                                    </tr>
                               </table>
                                <!--<p>Sekian, terima kasih.</p>-->
                                <p>b/p smarak.com.my</p>
                            </td>
                        </tr>
        			    <tr>
            			    <td>
            				    <div align="center" style="font-size:12px; margin-top:20px; padding:5px; width:100%; background:#eee;">
                                    © {{ date('Y') }} <a href="https://smarak.insko.my" target="_blank" style="color:#333; text-decoration: none;">smarak.com.my</a>
                                </div>
                            </td>
        			    </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>