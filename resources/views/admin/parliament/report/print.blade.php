<html>
	<head>
	<style type="text/css">
	body {
		font-size:13px;
	}
	th {
    font-size: 14px;
  

}

br.border2  {
   display: block;
   margin: 20px 0;
}

td {
    font-size: 14px;
    line-height: 170%;

}
p {
    
    line-height: 130%;

}

ul
{
    list-style-type: none;
}
	td.underline {
		
	}
	td.header {
		background-color: #e10606;
		font-weight:bold; 
	}
  	td.border {
	 	border-collapse: collapse;
		border: 1px solid black;
    }
 	td.foroffice {


	  	padding: 8px; /* cellpadding */
   		line-height: 130%;
 
    }

    table.border {
	  border-collapse: collapse;
	    border: 1px solid black;
	}
table.finborder {
    border: 1px solid black;
  border-collapse: collapse;
}

td.finborder {
    border: 1px solid black;
  border-collapse: collapse;
}
.logo_container a span
{
	color: #fe4c50;
}



	</style>
	
	</head>
	<body>
		<table  width="530">
			<tr>
				<td width="20%" align="top">	  <div class="logo_container">
							<a href="#">SEMA<span>RAK</span></a>
						</div></td>
				<td width="70%" valign="top" align="center">
					<font style=" font-weight: bold;text-align: center;"><b>SENARAI ADUAN RAKYAT</b></font>
				 </td>
				 <td width="10%" >&nbsp;</td>
			</tr>
		</table>
		<br><br><br><br><br>
		<table width="100%" cellspacing="4" style="font-size: 14px">
			<tbody>
				<tr>
					<td class="header" colspan="7" style="color:white; font-weight: bold;text-align: center;">MAKLUMAT ADUAN</td>
				</tr>
				<tr>
					<td width="40%">Nama</td>
					<td width="5%">:</td>
					<td width="55%" class="underline">{{$report->user_rakyat->fullname}}</td>
				</tr>
				<tr>
					<td>Kad Pengenal</td>
					<td>:</td>
					<td class="underline">{{$report->ic}}</td>
				</tr>
				<tr>
					<td>No Tel Bimbit</td>
					<td>:</td>
					<td class="underline">{{$report->user_rakyat->mobile_phone}}</td>
				</tr>
				<tr>
					<td>Jenis</td>
					<td>:</td>
					<td class="underline">{{$report->pr_report->desc}}</td>
				</tr>
				<tr>
					<td>Judul</td>
					<td>:</td>
					<td class="underline">{{$report->title}}</td>
				</tr>
				<tr>
					<td>Keterangan</td>
					<td>:</td>
					<td class="underline">{{$report->desc}}</td>
				</tr>
				<tr>
					<td>Alamat Rumah</td>
					<td>:</td>
					<td class="underline">{{$report->address}}</td>
				</tr>
				<tr>
					<td>Poskod</td>
					<td>:</td>
					<td class="underline">{{$report->postcode}}</td>
				</tr>
				<tr>
					<td>Bandar</td>
					<td>:</td>
					<td class="underline">{{$report->city}}</td>
				</tr>
				<tr>
					<td>Negeri</td>
					<td>:</td>
					<td class="underline">{{$report->state}}</td>
				</tr>
				<tr>
					<td>Nama Petugas</td>
					<td>:</td>
					<td class="underline">{{$report->user_petugas->name}}</td>
				</tr>
				<tr>
					<td>Status</td>
					<td>:</td>
					@if($report->reply->status=='TRM')
					<td class="underline">Diterima</td>
					@else
					<td class="underline">Ditolak</td>
					@endif
				</tr>
				<tr>
					<td>Remark</td>
					<td>:</td>
					<td class="underline">{{$report->reply->remark}}</td>
				</tr>
				@if($count!=0)
				 @foreach($image as $image)
				<tr>
					<td>Gambar</td>
					<td>:</td>
					<td class="underline"> <img style="float:left; "  src="{{asset('/admin/complaint/'.$report->user_rakyat->fullname.'/'.$image->filename)}}" width="70px" height="70px" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
                                @endforeach
                                @endif
				
			</tbody>
		</table>
		
		














	
	</div>
         

	</body>
</html>