@extends('admin.layout.template_datatables')

@section('content_admin')
<style>
.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
}

.button2 {background-color: #008CBA;} /* Blue */
.button3 {background-color: #f44336;} /* Red */ 
.button4 {background-color: #e7e7e7; color: black;} /* Gray */ 
.button5 {background-color: #555555;} /* Black */
</style>     
    <div class="wrapper wrapper-content animated fadeInRight">
        
        
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>SENARAI LAPORAN RAKYAT </h5>
                        <div class="ibox-tools">
                          
                        </div>
                    </div>

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" id="examtable">
                                <thead>
                                    <tr>
                                        <th class="hidden">Selisih</th>
                                        <th class="hidden">flow</th>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>IC</th>
                                        <!-- <th>Judul</th> -->
                                        <th>Jenis Laporan</th>
                                        <th>Detail</th>
                                        <th>Petugas</th>
                                        <th>Tarikh dibuka</th>
                                        <th>Tarikh ditutup</th>
                                        <th>History</th>
                                        <th>Status</th>
                                        <th>Tindakan</th>
                                    </tr>
                                </thead>
                              
                                <tbody>
                                <?php $i=1; ?>
                                @foreach($report as $report)

                                
                                    <tr class="gradeX">
                                        <?php

                                            $now = \Carbon\Carbon::now()->startOfDay();
                                            $date = \Carbon\Carbon::parse($report->created_at)->startOfDay();

                                            $diff = $now->diffInDays($date);

                                        ?>

                                        <td class="hidden">
                                            {{$diff}} 
                                        </td>
                                        <td class="hidden">{{$report->reply_id}}</td>
                                        <td>{{$i}}</td>
                                        <td>{{$report->user_rakyat->fullname}}</td>
                                        <td>{{$report->ic}}</td>
                                        <!-- <td>{{$report->title}}</td> -->
                                        <td>
                                            <span class="badge badge-{{$report->pr_report->color}}">{{$report->pr_report->desc}}</span>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#example{{$report->id}}"><i class="fa fa-chevron-down"></i> Detail</button>
                                        </td><!-- modal image -->
                                        
                                        <td class="center">
                                            @if($report->petugas_id!='')
                                                {{$report->user_petugas->name}}
                                            @else
                                        
                                            @endif
                                        </td>

                                        <td>
                                            <?php $tarikh =  date('d-m-Y ', strtotime($report->created_at)); ?>
                                            {{$tarikh}}
                                        </td>

                                        <td>
                                            @if($report->reply_id!='')
                                            
                                            <?php $tarikh_tutup =  date('d-m-Y ', strtotime($report->reply->created_at)); ?>
                                                    {{$tarikh_tutup}}
                                            
                                            @else
                                            
                                            @endif
                                        </td>
                                        <td>
                                            <a href="" type="submit" class="btn btn-warning btn-circle btn-lg" onclick="window.open(); return false;">
                                            <i class="fa fa-search"></i>
                                            </a>

                                        </td>
                                        <td>
                                        @if($report->reply_id!='')
                                            @if($report->reply->status=='TRM')
                                                   <button class="button button3">Diterima</button>
                                            @elseif($report->reply->status=='TRK')
                                                    <button class="button button5">Ditolak</button>
                                            @endif
                                        @else
                                        @endif

                                        </td>
                                       

                                        <td class="center">
                                            @if($report->petugas_id=='')
                                            <a href="{{url('/')}}/administrator/report-complaint/action/{{$report->id}}" type="submit" class="btn btn-danger" style="cursor:pointer;">
                                            <i class="fa fa-paste"></i> Follow Up
                                            </a>

                                            @elseif($report->reply->status=='TRM')
                                                <a href="{{url('/')}}/administrator/report-complaint/print/{{$report->id}}" type="submit" class="btn btn-warning" target="_blank">
                                                <i class="fa fa-print"></i> Print
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                <?php  $i++; ?>
                                @endforeach
                                </tbody>
                                <!--<tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>IC</th>
                                        <th>Nama DUN</th>
                                        <th>Petugas</th>
                                        <th>Tindakan</th>
                                    </tr>
                                </tfoot>-->
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @foreach($report2 as $data)
    <div class="modal fade bd-example-modal-lg" id="example{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header bg-red">
                    <h2 class="modal-title" id="exampleModalLabel"><b>Detail - {{$data->user_rakyat->fullname}} {{$data->ic}}</b></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <?php 
                        $image          = \DB::table('detail_image_report_complaints')->where('detail_report_complaints_id', $data->id)->get();
                    ?>
                    
                   

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            
                            <tbody>
                                <b style="color: blue !important">Detail </b>
                            
                                <tr class="gradeX">
                                    <td>Type </td>
                                    <td>:</td>
                                    <td><b style="color: red !important">{{$data->pr_report->desc}}</b></td>
                                    
                                </tr>
                                <tr class="gradeX">
                                    <td>Tajuk  </td>
                                    <td>:</td>
                                    <td><b>{{$data->title}}</b></td>
                                    
                                </tr>
                                <tr class="gradeX">
                                    <td>Keterangan  </td>
                                    <td>:</td>
                                    <td><b>{{$data->desc}}</b></td>
                                </tr>
                                <tr class="gradeX">
                                    <td>Alamat  </td>
                                    <td>:</td>
                                    <td><b>{{$data->address}}</b></td>
                                </tr>
                                <tr class="gradeX">
                                    <td>Kod Pos </td>
                                    <td>:</td>
                                    <td><b>{{$data->postcode}}</b></td>
                                </tr>

                                <tr class="gradeX">
                                    <td>Bandar </td>
                                    <td>:</td>
                                    <td><b>{{$data->city}}</b></td>
                                </tr>

                                <tr class="gradeX">
                                    <td>Negeri </td>
                                    <td>:</td>
                                    <td><b>{{$data->state}}</b></td>
                                </tr>

                            </tbody>
                            
                        </table>
                    </div>
                     
                    <b style="color: blue !important; margin-top: 40px !important">Detail Image </b>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>File Name</th>
                                        <th>Submit Date</th>
                                        
                                    </tr>
                                </thead>
                              
                                <tbody>
                                <?php $i=1; ?>
                                @foreach($image as $report)

                                
                                    <tr class="gradeX">
                                        <td>{{$i}}</td>
                                        <td>
                                            <a href="{{asset('/admin/complaint/'.$data->user_rakyat->fullname.'/'.$report->filename)}}" class="btn bg-light-blue" target='_blank'>{{$report->filename}}</a>
                                        </td>
                                        <td>{{$report->created_at}}</td>
                                        
                                    </tr>
                                <?php  $i++; ?>
                                @endforeach
                                </tbody>
                                
                            </table>
                        </div>
                        


                  </div>
                </div>
            </div>
        </div>
    @endforeach



@endsection

@push('custom')

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>



    <script type="text/javascript">     
        $(document).ready( function () {
          var table = $('#examtable').DataTable({
            "createdRow": function( row, data, dataIndex ) {
                     if ( data[0] >= 3 && data[1] != 1) {         //max_day = 3
                 $(row).css('color', 'Red');
               }
               
            }
          });
        } );

    </script>

@endpush