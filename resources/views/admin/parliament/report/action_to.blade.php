@extends('admin.layout.template_multiselect')

@section('content_admin')
    <link href="{{asset('admin/css/plugins/blueimp/css/blueimp-gallery.min.css')}}" rel="stylesheet">
     <div class="wrapper wrapper-content animated fadeInRight">
        
        <div class="row">
            <div class="col-lg-12"></div></div>
        </div>


        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Laporan Rakyat</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                
                                
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <form class="form-horizontal" method="post" action="{{url('/administrator/report-complaint/save_complaint/'.$report->id)}}">{{csrf_field()}}
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nama Penuh</label>
                                    <div class="col-sm-4">
                                        <input id="name" name="name" type="text" class="form-control required" value="{{$report->user_rakyat->fullname}}">
                                    </div>
                                    <label class="col-sm-2 control-label">IC </label>
                                    <div class="col-sm-4"> 
                                        <input id="ic" name="ic" type="text" class="form-control required" value="{{$report->user_rakyat->ic}}" readonly=""></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No Telp Bimbit</label>
                                    <div class="col-sm-4">  
                                        <input id="mobile" name="mobile" type="text" class="form-control required" value="{{$report->user_rakyat->mobile_phone}}" readonly="">
                                    </div>
                                <?php 
                                    $tarikh =  date('d-m-Y ', strtotime($report->created_at));
                                ?>
                                    <label class="col-sm-2 control-label">Tarikh Laporan</label>
                                    <div class="col-sm-4">
                                        <input id="tarikh" name="tarikh" type="text" class="form-control required" value="{{$tarikh}}" readonly="">
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Pelayanan</label>
                                    <div class="col-sm-4">
                                        <select name="pelayanan" class="form-control" required="" id="pelayanan"> 
                                            <option value="" selected disabled hidden>- Sila Pilih -</option>
                                            @foreach($pr_report_complaints as $data)
                                                @if($report->pr_report_complaints_id !=NULL)
                                                    <?php 
                                                        if($report->pr_report_complaints_id==$data->id) {
                                                          $selected = "selected";
                                                        }
                                                        else {
                                                           $selected = "";
                                                        } 
                                                    ?>
                                                    <option {{$selected}} value="{{$data->id}}">{{$data->desc}}</option>
                                                @else 
                                                    <option value="{{$data->id}}">{{$data->desc}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <label class="col-sm-2 control-label">Jenis</label>
                                    <div class="col-sm-4">
                                        <select name="jenis" class="form-control" required="" id="jenis">
                                            @foreach($pr_service as $pr_service)
                                                @if($report->pr_service_id !=NULL)
                                                    <?php 
                                                        if($report->pr_service_id==$pr_service->id) {
                                                          $selected = "selected";
                                                        }
                                                        else {
                                                           $selected = "";
                                                        } 
                                                    ?>
                                                    <option {{$selected}} value="{{$pr_service->code}}|{{$pr_service->report_complaint_id}}">{{$pr_service->code}} - {{$pr_service->desc}}</option>
                                                @else 
                                                    <option value="{{$pr_service->code}}|{{$pr_service->report_complaint_id}}">{{$pr_service->code}} - {{$pr_service->desc}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Judul </label>
                                    <div class="col-sm-10">
                                        <input id="ic" name="ic" type="text" class="form-control required" value="{{$report->title}}" readonly="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Keterangan</label>
                                    <div class="col-sm-10">
                                        <textarea id='address_1' class="form-control" rows="3"  style="color: black" name='address_1' readonly="" onkeyup="this.value = this.value.toUpperCase()">{{$report->desc}}</textarea>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Alamat 1</label>
                                        <div class="col-sm-10">
                                           <input type="text" name="address_1" id="address_1" readonly="" class="form-control" value="{{$report->address_1}}">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Alamat 2</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="address_2" id="address_2" readonly="" class="form-control" value="{{$report->address_1}}">
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Alamat 3</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="address_3" id="address_3" readonly="" class="form-control" value="{{$report->address_1}}">
                                        </div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                        <div class="col-md-3">
                                            <p class="font-bold">
                                                Poskod
                                            </p>
                                            <input type="text" name="postcode" id="postcode" min="0" inputmode="numeric" pattern="[0-9]*"  maxlength="6" readonly="" class="form-control" value="{{$report->postcode}}">
                                        </div>
                                        <div class="col-md-3">
                                            <p class="font-bold">
                                                Bandar
                                            </p>
                                           <input type="text" name="city" id="city" value="{{$report->city}}" readonly="" class="form-control">
                                        </div>
                                        <div class="col-md-4">
                                            <p class="font-bold">
                                                Negeri
                                            </p>
                                             <input type="text" name="state" id="state" readonly="" class="form-control" value="{{$report->state}}">
                                        </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-4">
                                            <select class="form-control" name="status" id="status" readonly>
                                            @if(!empty($report->reply->status))
                                                @if($report->reply->status=='TRM')
                                                <option value="TRM">Terima</option>
                                                @elseif($report->reply->status=='TLK')
                                                 <option value="TLK">Tolak</option>
                                                @endif
                                            @endif
                                                <option value="TRM">Terima</option>
                                                <option value="TLK">Tolak</option>
                                            </select>
                                        </div>
                                    <label class="col-sm-2 control-label organization" >Forward to</label>
                                        <div class="col-sm-4 organization">
                                            <select class="form-control" name="organization">
                                                @foreach($org as $data)
                                                <option value="{{$data->id}}">{{$data->organization}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                </div>

                                <!--<div class="form-group">
                                    <label class="col-sm-2 control-label">Catatan</label>
                                        <div class="col-sm-10">
                                            <textarea id='address_1' class="form-control" rows="3"  style="color: black" name='remark' onkeyup="this.value = this.value.toUpperCase()"></textarea>
                                        </div>
                                </div>-->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Catatan</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control ckeditor" rows="5" id="desc" name="remark" required=""></textarea>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Gambar</label>
                                        <div class="col-sm-2">
                                            <div class="lightBoxGallery">
                                                @foreach($image as $image)
                                                <a href="{{asset('/admin/complaint/'.$report->user_rakyat->fullname.'/'.$image->filename)}}" title="Image from Unsplash" data-gallery=""><img src="{{asset('/admin/complaint/'.$report->user_rakyat->fullname.'/'.$image->filename)}}" width="150px" height="150px"></a>   
                                                @endforeach

                                                <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                                                <div id="blueimp-gallery" class="blueimp-gallery">
                                                    <div class="slides"></div>
                                                    <h3 class="title"></h3>
                                                    <a class="prev">‹</a>
                                                    <a class="next">›</a>
                                                    <a class="close">×</a>
                                                    <a class="play-pause"></a>
                                                    <ol class="indicator"></ol>
                                                </div>  
                                            </div>
                                        </div>
                                </div>
                                <div class="form-group">

                                    <div class="col-sm-4 col-sm-offset-2">
                                        <input type="submit" value="Save" class="btn btn-primary">
                                    </div>

                                </div>


                                    

                                </div>

                                

                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

         

    <script type="text/javascript">

        function show2(){
            document.getElementById('div1').style.display ='none';
        }

        function show1(){
            document.getElementById('div1').style.display = 'block';
        }

    </script>



@endsection


@push('custom')
       <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>

    <script type="text/javascript">  
        CKEDITOR.replace( 'desc', { 
        shiftEnterMode : CKEDITOR.ENTER_P,
        enterMode: CKEDITOR.ENTER_BR, 
        on: {'instanceReady': function (evt) { evt.editor.execCommand('');     }},
        });      
    </script>
       <script src="{{asset('admin/js/plugins/blueimp/jquery.blueimp-gallery.min.js')}}"></script>
    <script type="text/javascript">
        var Privileges = jQuery('#status');
        var select = this.value;
        Privileges.change(function () {
            if ($(this).val() == 'TRM') { //1 others  
                $('.organization').show().find(':input').attr('required', true);
            }
            else{
                $('.organization').hide().find(':input').attr('required', false);
            } 

        });
    </script>
     <script type="text/javascript">
       $( document ).ready(function() {
       var options = $('#jenis').children().clone();
      
        $('#pelayanan').change(function() {
          $('#jenis').children().remove();
        var rawValue =this.value;
         options.each(function () {
                var newValue = $(this).val().split('|');
                if (rawValue == newValue[1] ) {
                    $('#jenis').append(this);
                 }
            });
          $('#jenis').val('');
        });
    });
    </script>
  <script>
        $(document).ready(function(){

            $('.summernote').summernote();

       });
    </script>
@endpush