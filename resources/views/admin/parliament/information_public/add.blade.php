@extends('admin.layout.template_multiselect')
@section('content_admin')
    </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Tambah Maklumat</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="index.html">Dashboard</a>
                    </li>
                    <li>
                        <a>Maklumat</a>
                    </li>
                    <li class="active">
                        <strong>Tambah</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5 style="text-align: center !important;">MAKLUMAT</h5>
                        </div>
                        <div class="ibox-content">
                            <form class="form-horizontal" method="post" action="{{url('/administrator/information_public/save')}}">
                            {{csrf_field()}}
                             @foreach($queries as $queries)
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nama Penuh</label>
                                    <div class="col-sm-10">
                                        <input id="name" name="name" type="text" class="form-control required" value="{{$queries->Nama}}" onkeyup="this.value = this.value.toUpperCase()">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">IC Baru</label>
                                    <div class="col-sm-4"> 
                                        <input id="ic" name="ic" type="text" class="form-control required" value="{{$queries->IC}}" readonly="">
                                    </div>
                                     <label class="col-sm-2 control-label">IC Lama</label>
                                    <div class="col-sm-4"> 
                                        <input id="iclama" name="ICLama" type="text" class="form-control required" value="{{$queries->ICLama}}" readonly="">
                                    </div>
                                </div>
                                <div class="form-group">
                                   
                                </div>
                                 <?php 
                                    $tanggal  = substr($queries->IC,4, 2);
                                    $bulan    = substr($queries->IC,2, 2);
                                    $tahun    = substr($queries->IC,0, 2); 
                                    $jantina  = substr($queries->IC,10, 2); 

                                    if($tahun > 30) 
                                        {
                                          $tahun2 = "19".$tahun;
                                        }
                                    else 
                                        {
                                          $tahun2 = "19".$tahun;
                                        }

                                    $lahir = $tahun2.'-'.$bulan.'-'.$tanggal;
                                    //$lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month')); 
                                ?>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tarikh Lahir</label>
                                    <div class="col-sm-4">
                                        <input id="dob" name="dob" type="text" class="form-control required" value="{{$lahir}}" readonly="">
                                    </div>
                                     <label class="col-sm-2 control-label">Jantina</label>
                                    <div class="col-sm-4">
                                        <select name="gender" required="" class="chosen-select">
                                             @if(!empty($queries->Jantina))
                                                <option  value="{{$queries->Jantina}}">{{$queries->Jantina}} </option>
                                            @endif
                                                <option value="L">L</option>
                                                <option value="P">P</i></option>
                                        </select>
                                        <input type="hidden" name="x[]" id="a" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Status Perkahwinan</label>
                                    <div class="col-sm-4">
                                       <select name="marital_status"  class="form-control m-b" id="marital_status" required>
                                            <option selected=""  value="" >--Sila Pilih--</option>
                                                @foreach($marital as $marital)
                                                     <option value="{{$marital->marital_code}}">{{$marital->marital_desc}}</option>
                                                @endforeach
                                        </select>
                                        <input type="hidden" name="x[]" id="a" value="">
                                    </div>
                                      <label class="col-sm-2 control-label">Status Kediaman</label>
                                    <div class="col-sm-4">
                                       <select name="residence"  class="form-control m-b" id="residence" required>
                                            <option selected=""  value="" >--Sila Pilih--</option>
                                                @foreach($residence as $residence)
                                                     <option value="{{$residence->code}}">{{$residence->desc}}</option>
                                                @endforeach
                                        </select>
                                        <input type="hidden" name="x[]" id="a" value="">
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nama Lokaliti</label>
                                    <div class="col-sm-10">
                                        <input id="lokaliti" name="lokaliti" type="text" class="form-control required" value="{{$queries->NamaLokaliti}}" readonly="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nama Dun</label>
                                    <div class="col-sm-4">
                                        <input id="dun" name="dun" type="text" class="form-control required" value="{{$queries->NamaDun}}" readonly="">
                                    </div>
                                    <label class="col-sm-2 control-label">Nama DM</label>
                                    <div class="col-sm-4">
                                        <input id="dms" name="dms" type="text" class="form-control required" value="{{$queries->NamaDM}}" readonly="">
                                    </div>
                                </div>
                                <div class="form-group">
                                     <label class="col-sm-2 control-label">Nama Parlimen</label>
                                    <div class="col-sm-4">
                                        <input id="NamaParlimen" name="NamaParlimen" type="text" class="form-control required" value="{{$queries->NamaParlimen}}" readonly="">
                                    </div>
                                    <label class="col-sm-2 control-label">Negeri</label>
                                    <div class="col-sm-4">
                                        <input id="negeri" name="Negeri" type="text" class="form-control required" value="{{$queries->Negeri}}" readonly="">
                                    </div>
                                </div>
                                <input id="norumah" name="norumah" type="hidden" class="form-control required" value="{{$queries->NoRumah}}" readonly="">
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No Telp Bimbit 1</label>
                                    <div class="col-sm-4">  
                                        <input id="mobile" name="mobile" type="text" class="form-control" maxlength="12" value="{{ Session::get('mobile') }}" onkeypress="return isNumberKey(event)" required="">
                                    </div>
                                     <label class="col-sm-2 control-label">No Telp Bimbit 2</label>
                                    <div class="col-sm-4">  
                                        <input id="mobile2" name="mobile2" type="text" class="form-control" maxlength="12" value="{{ Session::get('mobile2') }}" onkeypress="return isNumberKey(event)">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No Telp Rumah</label>
                                    <div class="col-sm-4">  
                                        <input id="telephone" name="telephone" type="text" class="form-control" maxlength="12" value="{{ Session::get('telephone') }}" onkeypress="return isNumberKey(event)">
                                    </div>
                                    <label class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-4">  
                                        <input id="email" name="email" type="email" class="form-control" value="{{ Session::get('email') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Pekerjaan</label>
                                    <div class="col-sm-4">
                                        <select name="occupation_id" id="occupation_id" data-placeholder="x" class="chosen-select" style="width:350px;" tabindex="4">

                                            <option selected=""  value="" >--Sila Pilih--</option>
                                            @foreach($occupation as $occupation)
                                                <option value="{{$occupation->id}}">{{$occupation->desc}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <label class="col-sm-2 control-label">Penyakit</label>
                                    <div class="col-sm-4">
                                        <select name="disease_type[]" id="mySelectId" data-placeholder="x" multiple="multiple" class="chosen-select" style="width:350px;" tabindex="4">
                                            @foreach($disease as $disease)
                                                <option value="{{$disease->disease_code}}">{{$disease->disease_desc}}</option>
                                            @endforeach
                                        </select>
                                        <input type="hidden" name="x[]" id="a" value="">
                                    </div>
                                </div>
                               
                                 
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Jumlah Tanggungan</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="no_dependants" id="no_dependants"min="0" inputmode="numeric" pattern="[0-9]*"  max="20"  @if (Session::has('no_dependants'))  value="{{ Session::get('no_dependants') }}" @endif required="" class="form-control fn" onkeypress="return isNumberKey(event)">
                                    </div>
                                    <label class="col-sm-2 control-label">Status Tanggungan</label>
                                    <div class="col-sm-4">
                                        <input type="text" id="school_dep" name="school_dep" class="form-control fn" placeholder="Sekolah"  min="0" inputmode="numeric" pattern="[0-9]*"  max="20" @if (Session::has('school_dep'))  value="{{ Session::get('school_dep') }}" @endif required="" onkeypress="return isNumberKey(event)"><br>

                                        <input type="text" id="working_dep" name="working_dep" class="form-control fn" placeholder="Bekerja"  min="0" inputmode="numeric" pattern="[0-9]*" max="20" @if (Session::has('working_dep'))  value="{{ Session::get('working_dep') }}" @endif required="" onkeypress="return isNumberKey(event)"><br>

                                        <input type="text" id="unemployee_dep" name="unemployee_dep" class="form-control fn" placeholder="Tidak Bekerja" min="0" inputmode="numeric" pattern="[0-9]*"  max="20" @if (Session::has('unemployee_dep'))  value="{{ Session::get('unemployee_dep') }}" @endif required="" onkeypress="return isNumberKey(event)"><br>
                                    </div>
                                    <div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Penerima Bantuan</label>
                                    <div class="col-sm-4">
                                       <select name="beneficiary"  class="form-control m-b" id="one" required>
                                            <option selected=""  value="" >--Sila Pilih--</option>
                                            <option  value="Ya">Ya</option>
                                            <option  value="Tidak">Tidak</option> 
                                        </select>
                                    </div>
                                            <label class="col-sm-2 control-label resources" style="display:none">Remark</label>
                                             <div class="col-sm-4 resources" style="display:none">
                                                <textarea id='remark_beneficiary' class="form-control" rows="3"  style="color: black" name='remark_beneficiary'  onkeyup="this.value = this.value.toUpperCase()" required=""></textarea>
                                            </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Alamat 1</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="address_1" id="address_1" required="" class="form-control" value="{{ Session::get('address_1') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Alamat 2</label>
                                    <div class="col-sm-10">
                                         <input type="text" name="address_2" id="address_2" required="" class="form-control" value="{{ Session::get('address_2') }}">
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label">Alamat 3</label>
                                    <div class="col-sm-10">
                                         <input type="text" name="address_3" id="address_3" required="" class="form-control" value="{{ Session::get('address_3') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                            <div class="col-md-3">
                                                <p class="font-bold">
                                                    Poskod
                                                </p>
                                                <input type="text" name="postcode" id="postcode" min="0" inputmode="numeric" pattern="[0-9]*"  maxlength="6"  class="form-control" required="" value="{{ Session::get('postcode') }}">
                                            </div>
                                            <div class="col-md-3">
                                                <p class="font-bold">
                                                    Bandar
                                                </p>
                                                <input type="text" name="city" id="city" required="" class="form-control" value="{{ Session::get('city') }}">
                                            </div>
                                            <div class="col-md-4">
                                                <p class="font-bold">
                                                    Negeri
                                                </p>
                                                <input type="text" name="state" id="state" required="" class="form-control" value="{{ Session::get('state') }}">
                                            </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Kecenderungan</label>
                                    <div class="col-sm-10">
                                        <div class="radio radio-info radio-inline">
                                            <input type="radio" id="inlineRadiop" value="1" name="kecenderungan"  required="">
                                            <label for="inlineRadiop"> Putih </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadiok" value="2" name="kecenderungan" required="">
                                            <label for="inlineRadiok"> Kelabu </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadioh" value="3" name="kecenderungan" required="">
                                            <label for="inlineRadioh"> Hitam </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Lain Lain Issue</label>
                                    <div class="col-sm-10">
                                        <textarea id='issue' class="form-control" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 183px;" style="color: black" name='issue'></textarea>   
                                    </div>
                                </div>
                               
                                @endforeach
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <!--<button class="btn btn-white" type="submit">Cancel</button>-->
                                        <button class="btn btn-primary" type="submit">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@push('custom')
<script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>
<script type="text/javascript">
  /*  $( ".fn" ).keyup(function() {
var no_dependants = $('#no_dependants').val();

var school_dep = $('#school_dep').val();

var unemployee_dep = $('#unemployee_dep').val();

var working_dep = $('#working_dep').val();

var total =  parseFloat(school_dep) + parseFloat(unemployee_dep) +parseFloat(working_dep);

   $("#total").val(parseFloat(total));

   if(total != no_dependants){
     alert("You must agree to the terms and conditions");
   }
});*/
</script>
@endpush
