@extends('admin.layout.template_datatables')
@section('content_admin')
        </div>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Data Tables</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="index.html">Home</a>
                        </li>
                        <li class="active">
                            <strong>Senarai Pencarian</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>SENARAI PETUGAS PROGRAM BERTANDANG DM SUNGAI JAI & SESAPAN BATU (MINANGKABAU) PRK N.24 DUN SEMENYIH</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>IC</th>
                        <th>IC Lama</th>
                        <th>Nama DM</th>
                        <th>Tindakan</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($queries as $queries)
                    <tr class="gradeX">
                        <td>{{$queries->Nama}}</td>
                        <td>{{$queries->IC}}</td>
                        <td>{{$queries->ICLama}}</td>
                        <td class="center">{{$queries->NamaDM}}</td>
                        <td class="center">
                             <form action="{{url('/administrator/information_public/searching_ic')}}" method="post">
                                    {{csrf_field()}}   
                                    <div class="input-group">
                                       <input type="hidden" name="search" class="form-control input-lg"  value="{{$queries->IC}}">
                                       <button type="submit" class="btn m-btn pull-right wow zoomInUp" data-wow-delay="0.5s">Kemas Kini <span class="fa fa-angle-right"></span></button>
                                </form>
                        </td>

                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Nama</th>
                        <th>IC</th>
                        <th>IC Lama</th>
                        <th>Nama DUN</th>
                        <th>Tindakan</th>
                    </tr>
                    </tfoot>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
      @endsection
