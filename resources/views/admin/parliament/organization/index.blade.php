@extends('admin.layout.template_datatables')

@section('content_admin')
              
    <div class="wrapper wrapper-content animated fadeInRight">
        @include('admin.shared.notif')
        
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>SENARAI Organisasi </h5>
                        <div class="ibox-tools">
                          
                        </div>
                    </div>

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Organisasi</th>
                                        <th>No Pendaftaran Organisasi</th>
                                        <th>Nama Pengurusi</th>
                                        <th>Negeri</th>
                                        <th>Tindakan</th>
                                    </tr>
                                </thead>
                              
                                <tbody>
                                <?php $i=1; ?>
                                @foreach($org as $org)
                                    <tr class="gradeX">
                                        <td>{{$i}}</td>
                                        <td>{{$org->org_name}}</td>
                                        <td>{{$org->org_reg_number}}</td>
                                        <td>{{$org->chairman_name}}</td>
                                        <td>{{$org->state}}</td>
                                        <td class="center">
                                            <a href="{{url('/')}}/administrator/organization/detail_organization/{{$org->org_reg_number}}" type="submit" class="btn btn-danger" style="cursor:pointer;">
                                            <i class="fa fa-paste"></i> Detail
                                            </a>
                                        </td>
                                    </tr>
                                <?php  $i++; ?>
                                @endforeach
                                </tbody>
                                <!--<tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>IC</th>
                                        <th>Nama DUN</th>
                                        <th>Petugas</th>
                                        <th>Tindakan</th>
                                    </tr>
                                </tfoot>-->
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection