@extends('admin.layout.template_form')

@section('content_admin')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Tambah Organisasi</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>Pengguna</a>
                </li>
                <li class="active">
                    <strong>Tambah Organisasi</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2"></div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        @include('admin.shared.notif')
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Tambah Organisasi Baharu</h5>
                            <div class="ibox-tools"></div>
                    </div>
                    <div class="ibox-content">
                        <form class="form-horizontal" method="post" action="{{url('/administrator/organization/save')}}">
                            {{csrf_field()}}
                            <input type="hidden" class="form-control" name="ref_no"  id="ref_no" value="{{$org->ref_no}}">
                            <div class="form-group"><label class="col-sm-4 control-label">Nama Organisasi</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="org_name" required="" id="org_name" value="{{$org->org_name}}" onkeyup="this.value = this.value.toUpperCase()">
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-4 control-label">Nombor Pendaftaran</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="org_reg_number" required="" id="org_reg_number" value="{{$org->org_reg_number}}" readonly="">
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-4 control-label">Nombor Telefon</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="phone_number" required="" id="phone_number" value="{{$org->phone_number}}">
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-4 control-label">Email</label>
                                <div class="col-sm-6">
                                    <input type="email" class="form-control" name="email" required="" id="email" value="{{$org->email}}" readonly="">
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                             <div class="form-group"><label class="col-sm-4 control-label">Total Membership</label>
                                <div class="col-sm-6">
                                    <input type="number" class="form-control" name="total_membership" required="" id="total_membership" value="{{$org->total_membership}}">
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-4 control-label">Alamat 1</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="address_1" required="" id="address_1" value="{{$org->Address->address_1}}">
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                             <div class="form-group"><label class="col-sm-4 control-label">Alamat 2</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="address_2"  id="address_2" value="{{$org->Address->address_2}}">
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                             <div class="form-group"><label class="col-sm-4 control-label">Alamat 3</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="address_3"  id="address_3" value="{{$org->Address->address_3}}">
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-4 control-label">Poskod</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="postcode" required="" id="postcode" value="{{$org->Address->postcode}}">
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-4 control-label">Bandar</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="city" required="" id="city" value="{{$org->Address->city}}">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-4 control-label">Negeri</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="state" required="" id="state" value="{{$org->Address->state}}">
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-4 control-label">IC Pengurusi</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="chairman_ic" required="" id="chairman_ic" value="{{$org->chairman_ic}}">
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-4 control-label">Nama Pengurusi</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="chairman_name" required="" id="chairman_name" value="{{$org->chairman_name}}">
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-4 control-label">Telefon Pengurusi</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="chairman_phone" required="" id="chairman_phone" value="{{$org->chairman_phone}}">
                                </div>
                                <div class="col-sm-2"></div>
                            </div>

                             <div class="form-group"><label class="col-sm-4 control-label">Nama DM</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="NamaDM" required="" id="NamaDM" value="{{$org->NamaDM}}">
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                             <div class="form-group"><label class="col-sm-4 control-label">Nama DUN</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="NamaDUN" required="" id="NamaDUN" value="{{$org->NamaDun}}">
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                             <div class="form-group"><label class="col-sm-4 control-label">Nama Parlimen</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="NamaParlimen" required="" id="NamaParlimen" value="{{$org->NamaParlimen}}">
                                </div>
                                <div class="col-sm-2"></div>
                            </div>
                            <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-6 col-sm-offset-6">
                                        <button class="btn btn-primary" type="submit">Simpan</button>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <div class="col-sm-12">
                        @include('admin.parliament.organization.ajk')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection