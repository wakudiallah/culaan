@extends('admin.layout.template_form')

@section('content_admin')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Survey Kecenderungan Pilihan Raya</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>Pengguna</a>
                </li>
                <li class="active">
                    <strong>Tambah</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2"></div>
    </div>
    @include('admin.shared.notif') 
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Tambah Data Baharu</h5>
                            <div class="ibox-tools"></div>
                    </div>
                    <div class="ibox-content">
                        <form class="form-horizontal" method="post" action="{{url('/administrator/survey/save')}}">
                            {{csrf_field()}}
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">IC</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="ic_survey" required="" id="ic_survey" value="{{$survey->ic}}">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Nama</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="name_survey" required="" id="name_survey" value="{{$survey->name}}">
                                </div>
                            </div>
                             <div class="form-group"><label class="col-sm-2 control-label">No Tel Bimbit</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="phone" required="" id="phone" value="{{$survey->phone}}">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Kod Lokaliti</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="kodlokaliti_survey" required="" id="kodlokaliti_survey" value="{{$survey->Kodlokaliti}}">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Nama Lokaliti</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="NamaLokaliti_survey" required="" id="NamaLokaliti_survey" value="{{$survey->NamaLokaliti}}">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Nama DM</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="NamaDM_survey" required="" id="NamaDM_survey" value="{{$survey->NamaDM}}">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Nama DUN</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="NamaDUN_survey" required="" id="NamaDUN_survey" value="{{$survey->NamaDun}}">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Nama Parlimen</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="NamaParlimen_survey" required="" id="NamaParlimen_survey" value="{{$survey->NamaParlimen}}">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Kecenderungan</label>
                                <div class="col-sm-10">
                                      @if($survey->kecenderungan == 'p')

                                         <div class="radio radio-info radio-inline">

                                            <input type="radio" id="inlineRadiop" value="p" name="kecenderungan"  checked disabled>

                                            <label for="inlineRadiop"> Putih </label>

                                        </div>

                                        <div class="radio radio-inline">

                                            <input type="radio" id="inlineRadioh" value="h" name="kecenderungan" disabled="">

                                            <label for="inlineRadioh"> Hitam </label>

                                        </div>

                                        <div class="radio radio-inline">

                                            <input type="radio" id="inlineRadiok" value="k" name="kecenderungan" disabled="">

                                            <label for="inlineRadiok"> Kelabu </label>

                                        </div>

                                        @elseif($survey->kecenderungan == 'h'  )

                                         <div class="radio radio-info radio-inline">

                                            <input type="radio" id="inlineRadiop" value="p" name="kecenderungan"  disabled="">

                                            <label for="inlineRadiop"> Putih </label>

                                        </div>

                                        <div class="radio radio-inline">

                                            <input type="radio" id="inlineRadioh" value="h" name="kecenderungan" checked disabled>

                                            <label for="inlineRadioh"> Hitam </label>

                                        </div>

                                        <div class="radio radio-inline">

                                            <input type="radio" id="inlineRadiok" value="k" name="kecenderungan" disabled="">

                                            <label for="inlineRadiok"> Kelabu </label>

                                        </div>

                                        @elseif($survey->kecenderungan == 'k'  )

                                         <div class="radio radio-info radio-inline">

                                            <input type="radio" id="inlineRadiop" value="p" name="kecenderungan" disabled=""  >

                                            <label for="inlineRadiop"> Putih </label>

                                        </div>

                                        <div class="radio radio-inline">

                                            <input type="radio" id="inlineRadioh" value="h" name="kecenderungan" disabled="" >

                                            <label for="inlineRadioh"> Hitam </label>

                                        </div>

                                        <div class="radio radio-inline">

                                            <input type="radio" id="inlineRadiok" value="k" name="kecenderungan" checked disabled>

                                            <label for="inlineRadiok" > Kelabu </label>

                                        </div>

                                        @endif

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




