@extends('admin.layout.template_barchart')

@section('content_admin')


        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>Laporan</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="index.html">Dashboard</a>
                    </li>

                    <li class="active">
                        <strong>Senarai Kecenderungan</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">
            </div>
 
        </div>


        

        

     

            <a href="javascript:void(processPrint());" class="btn btn-warning" style="margin-bottom: 10px !important">Print</a>

            <div id="printMe">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-9">
                        <div id="container" style="margin-bottom: 60px !important; min-width: 210px; height: 500px; margin: 0 auto"></div>
                    </div> <!-- end div print -->
                    <div class="col-lg-1"></div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                       <thead>
                            <tr>
                                <th class="biru">No</th>
                                <th class="merah">P</th>
                                <th class="hitam">K</th>
                                <th class="kelabu">H</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="gradeX">
                                <td>1</td>
                                <td>{{$putih}}</td>
                                <td>{{$kelabu}}</td>
                                <td>{{$hitam}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
              </div>
        </div>
        </div>


        <script language="javascript">
            var gAutoPrint = true;

            function processPrint(){

            if (document.getElementById != null){
            var html = '<HTML>\n<HEAD>\n';
            if (document.getElementsByTagName != null){
            var headTags = document.getElementsByTagName("head");
            if (headTags.length > 0) html += headTags[0].innerHTML;
            }

            html += '\n</HE' + 'AD>\n<BODY>\n';
            var printReadyElem = document.getElementById("printMe");

            if (printReadyElem != null) html += printReadyElem.innerHTML;
            else{
            alert("Error, no contents.");
            return;
            }

            html += '\n</BO' + 'DY>\n</HT' + 'ML>';
            var printWin = window.open("","processPrint");
            printWin.document.open();
            printWin.document.write(html);
            printWin.document.close();

            if (gAutoPrint) printWin.print();
            } else alert("Browser not supported.");

            }
        </script>
  

@endsection





