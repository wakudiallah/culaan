@extends('admin.layout.template_form')

@section('content_admin')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Tambah NGO</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>Pengguna</a>
                </li>
                <li class="active">
                    <strong>Tambah NGO</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2"></div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Tambah NGO Baharu</h5>
                            <div class="ibox-tools"></div>
                    </div>
                    <div class="ibox-content">
                        <form class="form-horizontal" method="post" action="{{url('/administrator/ngo/save_register')}}">
                            {{csrf_field()}}
                            <div class="form-group"><label class="col-sm-2 control-label">Nama Pertubuhan</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="ngo_name" required="" id="ngo_name" onkeyup="this.value = this.value.toUpperCase()">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Nombor Pendaftaran</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="ngo_reg_number" required="" id="ngo_reg_number" onkeyup="this.value = this.value.toUpperCase()">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Nombor Telefon</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="phone_number" required="" id="phone_number">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" name="email" required="" id="email">
                                </div>
                            </div>
                             <div class="form-group"><label class="col-sm-2 control-label">Total Membership</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" name="total_membership" required="" id="total_membership">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">IC Pengurusi</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="chairman_ic" required="" id="chairman_ic">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Nama Pengurusi</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="chairman_name" required="" id="chairman_name" onkeyup="this.value = this.value.toUpperCase()">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Telefon Pengurusi</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="chairman_phone" required="" id="chairman_phone">
                                </div>
                            </div>

                             <div class="form-group"><label class="col-sm-2 control-label">Nama DM</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="NamaDM" required="" id="NamaDM">
                                </div>
                            </div>
                             <div class="form-group"><label class="col-sm-2 control-label">Nama DUN</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="NamaDUN" required="" id="NamaDUN">
                                </div>
                            </div>
                             <div class="form-group"><label class="col-sm-2 control-label">Nama Parlimen</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="NamaParlimen" required="" id="NamaParlimen">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




