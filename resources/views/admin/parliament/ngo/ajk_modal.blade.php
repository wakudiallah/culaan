<div class="modal fade" id="material" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title">
                           
                        </h4>
                    </div>
                    <div class="modal-body no-padding">

                             <form class="form-horizontal" method="post" action="{{url('/administrator/ngo/save')}}">

                                    <fieldset> 
                                   
                                           <div class="row" >
                                             <section class="col col-6">
                                             <label class="label">Material</label>
                                                <select  style="width:100%" class="select2 material" name="material" required="" >
                        <option value="">Select Material </option>
                      
                        
                      </select>
                                            </section>
                                            </div>
                                             <div class="row" >
                                            <section class="col col-10 label">
                                           qty material
                                            </section>
                                              <section class="col col-4">

                                                <label class="input"> <i class="icon-append fa fa-cube"></i>
                                                    <input type="text" name="mqty" placeholder=" Qty  " required="">
                                                     <input type="hidden" name="id_project" value="{{$project->id}}">
                                                    <b class="tooltip tooltip-bottom-right"> Qty </b> 
                                                </label>
                                            </section>


                                             <section class="col col-2">
                                           
                                            <label class="select">
                                                <select class="input-sm" name="munit" required="" id="unit_m">
                                                 
                                                   
                                                </select> <i></i> </label>
                                        </section>
                                        
                                          </div>

                                             <div class="row" >
                                            <section class="col col-10 label">
                                            Estimasi hasil per satuan material 
                                            </section>
                                              <section class="col col-4">

                                                <label class="input"> <i class="icon-append fa fa-cube"></i>
                                                    <input type="text" name="mhqty" placeholder=" Qty  " required="">
                                                    <b class="tooltip tooltip-bottom-right"> Qty </b> 
                                                </label>
                                            </section>


                                             <section class="col col-2">
                                           
                                            <label class="select">
                                                <select class="input-sm" name="mhunit" required=""> 
                                                 
                                                   
                                                </select> <i></i> </label>
                                        </section>
                                        
                                          </div>
                                        
                                       
                                    </fieldset>
                                    
                                    <footer>
                                        <button type="submit" class="btn btn-primary">
                                           Save
                                        </button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">
                                            Cancel
                                        </button>

                                    </footer>
                                </form>                     
                                

                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->