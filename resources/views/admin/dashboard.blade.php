@extends('admin.layout.template')
@section('content_admin')              

        <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                    <h2>Dashboard</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{url('administrator')}}">Home</a>
                        </li>
                        
                    </ol>
                </div>
            </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content">
                    <div class="row">
                        <div class="col-lg-4">
                            <a href="{{url('administrator/report-complaint/index')}}">
                                <div class="widget style1 navy-bg">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <i class="fa fa-envelope-open fa-5x"></i>
                                        </div>
                                        <div class="col-xs-8 text-right">
                                            <span> Aduan </span>
                                            <h2 class="font-bold">{{$count_aduan}}</h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4">
                            <a href="{{url('administrator/report-complaint/index')}}">
                                <div class="widget style1 lazur-bg">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <i class="fa fa-pencil-square-o fa-5x"></i>
                                        </div>
                                        <div class="col-xs-8 text-right">
                                            <span> Cadangan</span>
                                            <h2 class="font-bold">{{$count_cadangan}}</h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4">
                            <a href="{{url('administrator/report-complaint/index')}}">
                                <div class="widget style1 yellow-bg">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <i class="fa fa-thumbs-o-up fa-5x"></i>
                                        </div>
                                        <div class="col-xs-8 text-right">
                                            <span> Bantuan</span>
                                            <h2 class="font-bold">{{$count_penghormatan}}</h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-4">
                            <a href="{{url('administrator/report-complaint/index')}}">
                                <div class="widget style1 red-bg">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <i class="fa fa-quote-left fa-5x"></i>
                                        </div>
                                        <div class="col-xs-8 text-right">
                                            <span> Penghargaan </span>
                                            <h2 class="font-bold">{{$count_penghargaan}}</h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4">
                            <a href="{{url('administrator/report-complaint/index')}}">
                                <div class="widget style1 black-bg">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <i class="fa fa-book fa-5x"></i>
                                        </div>
                                        <div class="col-xs-8 text-right">
                                            <span>Permohonan</span>
                                            <h2 class="font-bold">{{$count_permohonan}}</h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4">
                            <a href="{{url('administrator/report-complaint/index')}}">
                                <div class="widget style1 blue-bg">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <i class="fa fa-envelope-o fa-5x"></i>
                                        </div>
                                        <div class="col-xs-8 text-right">
                                            <span> Pertanyaan</span>
                                            <h2 class="font-bold">{{$count_pertanyaan}}</h2>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection