<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="icon" href="{{asset('main/images/favicon.ico')}}" type="image/x-icon">

    <title>History</title>
  </head>
  <body>
    
    <style>
    .button {
      background-color: #4CAF50; /* Green */
      border: none;
      color: white;
      padding: 10px 10px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      font-size: 16px;
      
      cursor: pointer;
    }

    .button2 {background-color: #008CBA;} /* Blue */
    .button3 {background-color: #f44336;} /* Red */ 
    .button4 {background-color: #e7e7e7; color: black;} /* Gray */ 
    .button5 {background-color: #555555;} /* Black */
    </style>


    <div class="container" style="margin: 40px !important">
        <div class="table-responsive">
            <table class="table dataTables-example" >
                
                <tbody>
                    <tr>
                        <td><b>IC</b></td>
                        <td>:</td>
                        <td>{{$user->ic}}</td>
                        <td></td>
                        <td><b>Kod Lokaliti</b></td>
                        <td>:</td>
                        <td>{{$user->Kodlokaliti}}</td>
                        <td></td>
                          
                    </tr>

                    <tr>
                        <td><b>Name</b></td>
                        <td>:</td>
                        <td>{{$user->name}}</td>
                        <td></td>
                        <td><b>Nama Lokaliti</b></td>
                        <td>:</td>
                        <td>{{$user->NamaLokaliti}}</td>
                        <td></td>
                        
                    </tr>
                </tbody>
            </table>
        </div>
    </div>


    <div class="container">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover dataTables-example" >
                <thead class="bg-danger" style="color: white">
                    <tr>
                        <th>No</th>
                        <th>Tarikh</th>
                        <th>Kecenderungan</th>
                        
                    </tr>
                </thead>
              
                <tbody>
                <?php $i=1; ?>
                @foreach($senarai as $survey)
                    <tr class="gradeX">
                        <td>{{$i}}</td>
                        <td>
                            <?php $tarikh =  date('d-m-Y ', strtotime($survey->created_at)); ?>
                            {{$tarikh}}
                        </td>
                        <td>
                            @if($survey->kecenderungan=='p')
                            <button class="button button3">Putih</button>
                            @elseif($survey->kecenderungan=='h')
                            <button class="button button5">Hitam</button>
                            @elseif($survey->kecenderungan=='k')
                            <button class="button button4">Kelabu</button>
                            @endif
                        </td>
                        
                        
                    </tr>
                <?php  $i++; ?>
                @endforeach
                </tbody>
                
            </table>
        </div>
    </div>
    


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>


