@extends('admin.layout.template_form')

@section('content_admin')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Survey Kecenderungan Pilihan Raya</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                </li>
                <li>
                    <a>Pengguna</a>
                </li>
                <li class="active">
                    <strong>Tambah</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2"></div>
    </div>
    
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Tambah Data Baharu <b style="color: red !important"> PRK {{$parlimen->parlimen_code}} - {{$parlimen->name_parlimen->parlimen_name}} : {{$parlimen->dun_code}} - {{$parlimen->name_dun->dun_name}} </b></h5>
                            <div class="ibox-tools"></div>
                    </div>
                    <div class="ibox-content">
                        
                        <form class="form-horizontal" method="post" action="{{url('/administrator/pilihan-raya/prn/save-kajian-sikap')}}">


                            {{csrf_field()}}

                            <input type="hidden" class="form-control" name="code_parlimen" value="{{$parlimen->parlimen_code}}" required="" >


                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">IC</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="ic_survey" required="" id="ic_survey">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Nama</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="name_survey" required="" id="name_survey">
                                </div>
                            </div>
                             <div class="form-group"><label class="col-sm-2 control-label">No Tel Bimbit</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="phone" required="" id="phone">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Kod Lokaliti</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="kodlokaliti_survey" required="" id="kodlokaliti_survey">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Nama Lokaliti</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="NamaLokaliti_survey" required="" id="NamaLokaliti_survey">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Nama DM</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="NamaDM_survey" required="" id="NamaDM_survey">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Nama DUN</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="NamaDUN_survey" required="" id="NamaDUN_survey">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Nama Parlimen</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="NamaParlimen_survey" required="" id="NamaParlimen_survey">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Negeri</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="Negeri_survey" required="" id="Negeri_survey">
                                </div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Kecenderungan</label>
                                <div class="col-sm-10">
                                     <div class="radio radio-info radio-inline">
                                        <input type="radio" id="inlineRadiop" value="p" name="kecenderungan"  required="">
                                        <label for="inlineRadiop"> Putih </label>
                                    </div>
                                    <div class="radio radio-inline">
                                        <input type="radio" id="inlineRadioh" value="h" name="kecenderungan" required="">
                                        <label for="inlineRadioh"> Hitam </label>
                                    </div>
                                    <div class="radio radio-inline">
                                        <input type="radio" id="inlineRadiok" value="k" name="kecenderungan" required="">
                                        <label for="inlineRadiok"> Kelabu </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection




