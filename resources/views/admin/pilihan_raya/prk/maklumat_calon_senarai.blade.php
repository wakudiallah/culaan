@extends('admin.layout.template_datatables')

@section('content_admin')


    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Maklumat Calon</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('/administrator')}}">Home</a>
                </li>
                <li>
                    <a href="{{url('/administrator/pilihan-raya/prk/index')}}"> Prk</a>
                </li>
                <li>
                    <a href="{{url('/administrator/pilihan-raya/senarai-maklumat-calon/'.$id.'/'.$type)}}"> Maklumat Calon</a>
                </li>
                
            </ol>
        </div>
        <div class="col-lg-2"></div>
    </div>



    <div class="wrapper wrapper-content animated fadeInRight">
        
        
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>SENARAI MAKLUMAT CALON <b style="color: red !important">
                            
                            {{$parlimen->parlimen_code}} - {{$parlimen->name_parlimen->parlimen_name}} : {{$parlimen->dun_code}} - {{$parlimen->name_dun->dun_name}}
                            
                        </b> 
                        </h5>
                        <div class="ibox-tools">
                          
                        </div>
                    </div>

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>IC</th>
                                        <th>Parti</th>
                                        <th width="20%">Gambar</th>
                                        <th>Detail</th>
                                        <!-- 
                                        <th class="align-center">History</th> -->
                                        <th>Tindakan</th>
                                    </tr>
                                </thead>
                              
                                <tbody>
                                <?php $i=1; ?>
                                @foreach($candidate as $data)
                                    <tr class="gradeX">
                                        <td>{{$i}}</td>
                                        <td>{{$data->real_name}}</td>
                                        <td>{{$data->ic}}</td>
                                        <td>{{$data->partigabungan->parti_gabungan_desc}}</td>
                                        <td width="20%">
                                            <img src="{{url('/')}}/admin/calon/{{$id}}/{{$data->picture}}" class="img-responsive" width="40%" height="40%">
                                        </td>
                                        
                                        <!--
                                        <td class="align-center">
                                            
                                            <a href="{{url('/')}}/administrator/pilihan-raya/prn/hitory/{{$parlimen->parlimen_code}}/{{$data->ic}}" type="submit" class="btn btn-warning btn-circle btn-lg" onclick="window.open('{{url('/')}}/administrator/pilihan-raya/prn/hitory/{{$parlimen->parlimen_code}}/{{$data->ic}}', 'newwindow', 'width=750,height=400'); return false;">
                                            <i class="fa fa-search"></i>
                                            </a>

                                        </td> -->
                                        <td class="center">
                                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#example{{$data->id}}"><i class="fa fa-chevron-down"></i> Detail</button>
                                        </td>

                                         <td width="20%">


                                            <a href="{{url('/')}}/administrator/pilihan-raya/kemaskini-maklumat-calon/{{$id}}/prk/{{$data->id}}" type="submit" class="btn btn-warning" >
                                            <i class="fa fa-edit"></i> Kemaskini
                                            </a>
                                            <a href="{{url('/')}}/administrator/pilihan-raya/padam-maklumat-calon/{{$data->id}}" type="submit" class="btn btn-danger" >
                                            <i class="fa fa-trash"></i> Padam
                                            </a>
                                        </td>
                                        
                                    </tr>
                                <?php  $i++; ?>
                                @endforeach
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    @foreach($candidate as $datax)
    <div class="modal fade bd-example-modal-lg" id="example{{$datax->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header bg-red">
                    <h2 class="modal-title" id="exampleModalLabel"><b>Detail - {{$datax->name}}</b></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            
                            <tbody>
                                
                            
                                <tr class="gradeX">
                                    <td>Full Nama </td>
                                    <td>:</td>
                                    <td><b style="color: red !important">{{$datax->real_name}}</b></td>
                                    
                                </tr>
                                <tr class="gradeX">
                                    <td> Name </td>
                                    <td>:</td>
                                    <td><b>{{$datax->name}}</b></td>
                                    
                                </tr>
                                <tr class="gradeX">
                                    <td>Tarikh Lahir  </td>
                                    <td>:</td>
                                    <td><b>{{$datax->born_date}}</b></td>
                                </tr>
                                <tr class="gradeX">
                                    <td>Activiti  </td>
                                    <td>:</td>
                                    <td><b>{{$datax->activity}}</b></td>
                                </tr>
                                <tr class="gradeX">
                                    <td>Jawatan </td>
                                    <td>:</td>
                                    <td><b>{{$datax->position}}</b></td>
                                </tr>

                                <tr class="gradeX">
                                    <td>Web </td>
                                    <td>:</td>
                                    <td><b>{{$datax->web}}</b></td>
                                </tr>

                                <tr class="gradeX">
                                    <td>Facebook </td>
                                    <td>:</td>
                                    <td><b>{{$datax->soc_fb}}</b></td>
                                </tr>


                                <tr class="gradeX">
                                    <td>Instagram </td>
                                    <td>:</td>
                                    <td><b>{{$datax->soc_ig}}</b></td>
                                </tr>

                                <tr class="gradeX">
                                    <td>Twitter </td>
                                    <td>:</td>
                                    <td><b>{{$datax->soc_twitter}}</b></td>
                                </tr>

                                <tr class="gradeX">
                                    <td>Alamat </td>
                                    <td>:</td>
                                    <td><b>{{$datax->address}}</b></td>
                                </tr>

                                <tr class="gradeX">
                                    <td>Poskod </td>
                                    <td>:</td>
                                    <td><b>{{$datax->postcode}}</b></td>
                                </tr>

                                <tr class="gradeX">
                                    <td>Bandar </td>
                                    <td>:</td>
                                    <td><b>{{$datax->city}}</b></td>
                                </tr>

                                <tr class="gradeX">
                                    <td>Negeri </td>
                                    <td>:</td>
                                    <td><b>{{$datax->state}}</b></td>
                                </tr>

                            </tbody>
                            
                        </table>
                    </div>
                    


                  </div>
                </div>
            </div>
        </div>
    @endforeach


@endsection