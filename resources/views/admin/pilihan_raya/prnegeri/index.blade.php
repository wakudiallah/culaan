@extends('admin.layout.template_datatables')

@section('content_admin')
              
    <div class="row wrapper border-bottom white-bg page-heading">
           
        <div class="col-lg-10">
            <h2>SENARAI PRN</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('/administrator')}}">Home</a>
                </li>
                <li>
                    <a>PRN</a>
                </li>

            </ol>
        </div>
        <div class="col-lg-2">
        </div>

    </div>


    <div class="wrapper wrapper-content animated fadeInRight">
        
        
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>SENARAI PRN</h5>
                        <div class="ibox-tools">
                          
                        </div>
                    </div>

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kawasan</th>
                                        <th>Parlimen</th>
                                        <th>Tarikh Undi</th>
                                        <th>Status</th>
                                        <th>Kajian Sikap</th>
                                        <th>Maklumat Calon</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $i=1; ?>
                                @foreach($pr as $data)
                                    <tr class="gradeX">
                                        <td>{{$i}}</td>
                                        <td>{{$data->state_name}}</td>
                                        <td>{{$data->parlimen_code}} - {{$data->name_parlimen->parlimen_name}}</td>
                                        <td>
                                            <?php $tarikh =  date('d-m-Y ', strtotime($data->election_date)); ?>
                                            {{$tarikh}} 
                                        </td>
                                        <td>
                                           <?php 

                                            $now = \Carbon\Carbon::now()->startOfDay();
                                            $date = \Carbon\Carbon::parse($data->election_date)->startOfDay();

                                            $diff = $now->diffInDays($date);
                                        ?>


                                            @if($data->election_date >= $now )
                                                <p><b style="color: red"> - {{$diff}} days</b></p>
                                            @else
                                                <p style="color: blue">Close</p>
                                            @endif
                                            
                                        </td>
                                        <td class="center"> <!-- kajian sikap -->
                                           <a href="{{url('/')}}/administrator/pilihan-raya/prn/kajian-sikap/{{$data->parlimen_code}}" type="submit" class="btn btn-warning" >
                                            <i class="fa fa-edit"></i> Add
                                            </a>

                                            <a href="{{url('/')}}/administrator/pilihan-raya/prn/senarai-kajian-sikap/{{$data->parlimen_code}}" type="submit" class="btn btn-success" >
                                            <i class="fa fa-table"></i> Senarai
                                            </a>

                                            <a href="{{url('/')}}/administrator/pilihan-raya/prn/chart-kajian-sikap/{{$data->parlimen_code}}" type="submit" class="btn btn-danger" >
                                            <i class="fa fa-bar-chart-o"></i> Chart
                                            </a>
                                            
                                        </td>
                                        <td class="center"> <!-- maklumat calon -->
                                            <a href="{{url('/')}}/administrator/pilihan-raya/add-maklumat-calon/{{$data->parlimen_code}}/prn" type="submit" class="btn btn-warning" >
                                            <i class="fa fa-edit"></i> Add
                                            </a>

                                            <a href="{{url('/')}}/administrator/pilihan-raya/senarai-maklumat-calon/{{$data->parlimen_code}}/prn" type="submit" class="btn btn-success" >
                                            <i class="fa fa-table"></i> Senarai
                                            </a>

                                            
                                        </td>
                                    </tr>
                                <?php  $i++; ?>
                                @endforeach
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection