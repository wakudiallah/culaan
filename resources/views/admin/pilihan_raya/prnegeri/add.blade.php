@extends('admin.layout.template_form')

@section('content_admin')

    <link href="{{asset('admin/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">

   

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Pilihan Raya</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('administrator')}}">Home</a>
                </li>
                <li>
                    <a>PRN</a>
                </li>
                <li class="active">
                    <strong>Tambah</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2"></div>
    </div>
    
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Tambah Data Baharu</h5>
                            <div class="ibox-tools"></div>
                    </div>
                    <div class="ibox-content">
                        <form class="form-horizontal" method="post" action="{{url('/administrator/pilihan-raya/prn/save')}}">
                            {{csrf_field()}}
                            <div class="hr-line-dashed"></div>
                                
                            <div class="form-group"><label class="col-sm-2 control-label">Nama Kawasan</label>
                                <div class="col-sm-10">
                                    <select name="state_name" class="form-control" required="" id="state_name"> 
                                        <option value="" selected disabled hidden>- Sila Pilih -</option>
                                        @foreach($state as $data)
                                        <option value="{{$data->state_code}}">{{$data->state_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Nama Parlimen</label>
                                <div class="col-sm-10">
                                        
                                        <select name="parlimen_name" class="form-control" required="" id="parlimen_name">
                                            <option value="" selected disabled hidden>- Sila Pilih -</option>

                                            @foreach($pr as $data)
                                            <option value="{{$data->code}}|{{$data->state_code}}">{{$data->code}} - {{$data->parlimen_name}}</option>
                                            @endforeach
                                        </select>
                                        
                                </div>
                            </div>
                                
                                
                            
                            <div class="form-group"><label class="col-sm-2 control-label">Tarikh Undi</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" placeholder="" id="datepicker"  name="election_date" required="" >
                                </div>
                            </div>



                            
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection


@push('custom')

    <script type="text/javascript">
       $( document ).ready(function() {
       var options = $('#parlimen_name').children().clone();
      
        $('#state_name').change(function() {
          $('#parlimen_name').children().remove();
        var rawValue =this.value;
         options.each(function () {
                var newValue = $(this).val().split('|');
                if (rawValue == newValue[1] ) {
                    $('#parlimen_name').append(this);
                 }
            });
          $('#parlimen_name').val('');
        });
    });
    </script>


    <!-- Data picker -->
   <script src="{{asset('admin/js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>



    <script>
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap'
        });
    </script>

@endpush

