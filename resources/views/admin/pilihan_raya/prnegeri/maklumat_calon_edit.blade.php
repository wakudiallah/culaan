@extends('admin.layout.template_multiselect')

@section('content_admin')

<link href="{{asset('admin/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">


    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Maklumat Calon</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('/administrator')}}">Home</a>
                </li>
                <li>
                    <a href="{{url('/administrator/pilihan-raya/prn/index')}}"> Prn</a>
                </li>
                <li>
                    <a href="{{url('/administrator/pilihan-raya/senarai-maklumat-calon/'.$id.'/'.$type)}}"> Maklumat Calon</a>
                </li>
                <li class="active">
                    <strong>Edit</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2"></div>
    </div>

    
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Tambah Data Baharu <b style="color: red !important"> </b></h5>
                            <div class="ibox-tools"></div>
                    </div>
                    <div class="ibox-content">
                        
                        <form class="form-horizontal" method="post" action="{{url('/administrator/pilihan-raya/kemaskini-maklumat-calon/'.$id.'/'.$type.'/'.$candidate->id)}}" enctype="multipart/form-data">
                            

                            {{csrf_field()}}


                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">IC</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="ic" required="" id="ic_candidate" onkeypress="return isNumberKey(event)" maxlength="12" value="{{$candidate->ic}}">
                                </div>
                                <label class="col-sm-2 control-label">Nama Penuh</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="fullname" required="" id="name_candidate" value="{{$candidate->real_name}}">
                                </div>
                            </div>
                            
                            <?php $born_date =  date('m/d/Y ', strtotime($candidate->born_date)); ?>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Nama</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="name" required="" onkeyup="this.value = this.value.toUpperCase()" value="{{$candidate->name}}">
                                </div>


                                <label class="col-sm-2 control-label">Tarikh Lahir</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="born_date" required="" id="datepicker" value="{{$born_date}} ">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No Telp Bimbit 1</label>
                                    <div class="col-sm-4">  
                                        <input id="mobile" name="phone" type="text" class="form-control" maxlength="12"  onkeypress="return isNumberKey(event)" required="" value="{{$candidate->phone}}">
                                    </div>
                                     <label class="col-sm-2 control-label">No Telp Bimbit 2</label>
                                    <div class="col-sm-4">  
                                        <input id="mobile2" name="phone2" type="text" class="form-control" value="{{$candidate->phone2}}"" maxlength="12"  onkeypress="return isNumberKey(event)" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No Telp Rumah</label>
                                    <div class="col-sm-4">  
                                        <input id="telephone" name="telp" type="text" class="form-control" maxlength="12"  onkeypress="return isNumberKey(event)" value="{{$candidate->telp}}">
                                    </div>
                                    <label class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-4">  
                                        <input id="email" name="email" type="email" class="form-control" value="{{$candidate->email}}" >
                                    </div>
                                </div>
                                

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Alamat Asal</label>
                                    <div class="col-sm-10">
                                        <textarea id='address' class="form-control" rows="3" name='address' >{{$candidate->address}}</textarea>
                                    </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Poskod</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" name="postcode" required="" id="postcode" onkeypress="return isNumberKey(event)" maxlength="5" value="{{$candidate->postcode}}">
                                </div>
                                <label class="col-sm-2 control-label">Bandar</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" name="city" required="" id="city" value="{{$candidate->city}}">
                                </div>
                                <label class="col-sm-2 control-label">Negeri</label>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" name="state" required="" id="state" value="{{$candidate->state}}">
                                </div>
                            </div>
                            
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Pekerjaan</label>
                                <div class="col-sm-4">
                                    <select name="activity" id="activity" data-placeholder="x" class="chosen-select" style="width:350px;" tabindex="4" required="">

                                        <option value="" selected disabled hidden>- Sila Pilih -</option>
                                        @foreach($occupation as $occupation)
                                            <option value="{{$occupation->id}}" {{ $candidate->activity == $occupation->id ? 'selected' : '' }}>{{$occupation->desc}}</option>
                                        @endforeach
                                       
                                    </select>
                                    
                                </div>
                                <label class="col-sm-2 control-label">Jawatan</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="position" required="" id="" value="{{$candidate->position}}">
                                </div>
                            </div>
                            
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Website</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="web" required="" id="" value="{{$candidate->web}}">
                                </div>
                                <label class="col-sm-2 control-label">Facebook</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="fb" required="" id="" value="{{$candidate->soc_fb}}">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Instagram</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="ig" required="" id="" value="{{$candidate->soc_ig}}">
                                </div>
                                <label class="col-sm-2 control-label">Twitter</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="twitter" required="" id="" value="{{$candidate->soc_twitter}}">
                                </div>
                            </div>
                            
                            <div class="hr-line-dashed"></div>

                            <div class="form-group"><label class="col-sm-2 control-label">Parti</label>
                                <div class="col-sm-10">
                                    
                                    <select name="parti" id="mySelectId" class="form-control chosen-select" >
                                        <option value="" selected disabled hidden>- Sila Pilih -</option>
                                        @foreach($parti as $data)
                                            <option value="{{$data->parti_code}}" {{ $candidate->parti_id == $data->parti_code ? 'selected' : '' }}>{{$data->parti_gabungan_desc}}</option>
                                        @endforeach
                                    </select>
                                    
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>



                            <div class="form-group"><label class="col-sm-2 control-label">Gambar Profil</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control" name="picture" id="">
                                </div>
                            </div>
                           
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@push('custom')

<script type="text/javascript">
        $( "#ic_candidate" ).change(function() {
            var ic = $('#ic_candidate').val();
            $.ajax({
            url: "<?php  print url('/'); ?>/administrator/ic/"+ic,
            dataType: 'json',
            data: {
            },
            success: function (data, status) {
                jQuery.each(data, function (k) {
                    $("#name_candidate").val(data[k].Nama );
                    $("#jantina").val(data[k].Jantina );
                    $("#NamaDUN_ajk").val(data[k].NamaDun );
                    $("#NamaParlimen_ajk").val(data[k].NamaParlimen );

                    });
                }
            });
        });
    </script>


    <script type="text/javascript">
            $( "#postcode" ).change(function() {
                var postcode = $('#postcode').val();
                $.ajax({
                url: "<?php  print url('/'); ?>/administrator/postcode/"+postcode,
                dataType: 'json',
                data: {
                },
                success: function (data, status) {
                    jQuery.each(data, function (k) {
                        $("#city").val(data[k].post_office );
                        $("#state").val(data[k].state.state_name );
                        $("#country").val("Malaysia");
                        });
                    }
                });
            });
        </script>


    <!-- Data picker -->
   <script src="{{asset('admin/js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>



    <script>
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap'
        });
    </script>

@endpush
