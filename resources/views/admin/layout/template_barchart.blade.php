<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>SEMARAK</title>

        <link href="{{asset('admin/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{asset('admin/css/plugins/iCheck/custom.css')}}" rel="stylesheet">

    <link href="{{asset('admin/css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('admin/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link href="{{asset('admin/css/plugins/colorpicker/bootstrap-colorpicker.min.css')}}" rel="stylesheet">

    <link href="{{asset('admin/css/plugins/cropper/cropper.min.css')}}" rel="stylesheet">

    <link href="{{asset('admin/css/plugins/switchery/switchery.css')}}" rel="stylesheet">

    <link href="{{asset('admin/css/plugins/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">

    
    <link href="{{asset('admin/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">

    <link href="{{asset('admin/css/plugins/ionRangeSlider/ion.rangeSlider.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css')}}" rel="stylesheet">

    <link href="{{asset('admin/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">

    <link href="{{asset('admin/css/plugins/clockpicker/clockpicker.css')}}" rel="stylesheet">

    <link href="{{asset('admin/css/plugins/daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet">

    <link href="{{asset('admin/css/plugins/select2/select2.min.css')}}" rel="stylesheet">

    <link href="{{asset('admin/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css')}}" rel="stylesheet">

    <link href="{{asset('admin/css/plugins/dualListbox/bootstrap-duallistbox.min.css')}}" rel="stylesheet">

    <link href="{{asset('admin/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('admin/css/style.css')}}" rel="stylesheet">
        <link href="{{asset('admin/css/plugins/steps/jquery.steps.css')}}" rel="stylesheet">

    <link rel="icon" href="{{asset('main/images/favicon.ico')}}" type="image/x-icon">

    
    </head>
    @include('admin.layout.admin_header')
    @yield('content_admin')
    @include('admin.layout.admin_footer')
        <!-- Mainly scripts -->
        <!-- Mainly scripts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
         <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>

        <script src="{{asset('admin/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('admin/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
        <script src="{{asset('admin/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
        <!-- Custom and plugin javascript -->
        <script src="{{asset('admin/js/inspinia.js')}}"></script>
        <script src="{{asset('admin/js/plugins/pace/pace.min.js')}}"></script>
        <!-- Steps -->
        <script src="{{asset('admin/js/plugins/dataTables/datatables.min.js')}}"></script>
      
         <script type="text/javascript">

    $(function () { 
        var data_asses = <?php echo $hitam; ?>;
        var data_sub = <?php echo $hitam; ?>;
        var data_dis = <?php echo $kelabu; ?>; 


        Highcharts.chart('container', {
    chart: {
        type: 'column',
        width:700,
        x:-20
    },
    title: {
        text: 'Senarai Total Kecenderungan Berdasarkan DM',
         x: -20 //center
    },
    subtitle: {
        text: '<b>Senarai Kecenderungan Parlimen</b>'
    },
    xAxis: {
        type: 'category',
        width:600
    },
    yAxis: {
        title: {
            text: 'Jumlah Culaan'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.f}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.f}</b> <br/>'
    },

    "series": [
        {
            "name": "Kecenderungan",
            "colorByPoint": true,
            "data": [
                {
                    "name": "<b style='color:red; font-size:14px;font-weight:bold'>Putih</b>",
                    "y": <?php echo $putih; ?>,
                    "color":"#be0d12"
                },
                {
                    "name": "<b style='color:grey; font-size:14px;font-weight:bold'>Kelabu</b>",
                    "y": <?php echo $kelabu; ?>,
                    "color":"#808080"
                },
                {
                    "name": "<b style='color:black; font-size:14px;font-weight:bold'>Hitam</b>",
                    "y": <?php echo $hitam; ?>,
                    "color":"#0c0c0c"
                }
               
            ]
        }
    ]
    
});

        });
</script>
    </body>
</html>