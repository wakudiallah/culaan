<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>SEMARAK</title>
        <link href="{{asset('admin/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('admin/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
        <link href="{{asset('admin/css/plugins/iCheck/custom.css')}}" rel="stylesheet">
        <link href="{{asset('admin/css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
        <!-- Toastr style -->
        
        <link href="{{asset('admin/css/animate.css')}}" rel="stylesheet">
        <link href="{{asset('admin/css/style.css')}}" rel="stylesheet">

        <link rel="icon" href="{{asset('main/images/favicon.ico')}}" type="image/x-icon">
    </head>
    @include('admin.layout.admin_header')
    @yield('content_admin')
    @include('admin.layout.admin_footer')
        <!-- Mainly scripts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
         <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="{{asset('admin/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('admin/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
        <script src="{{asset('admin/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
        <!-- Custom and plugin javascript -->
        <script src="{{asset('admin/js/inspinia.js')}}"></script>
        <script src="{{asset('admin/js/plugins/pace/pace.min.js')}}"></script>
        <!-- Steps -->
        <script src="{{asset('admin/js/plugins/dataTables/datatables.min.js')}}"></script>
        <script>
            $(document).ready(function(){
                $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    //{ extend: 'copy'},
                    //{extend: 'csv'},
                    //{extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'PPBM - N24'},
                    {extend: 'print',
                    customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                        }
                    }]
                });
            });
        </script>
        <?php
            $number =  $target; 
            $new_numb = number_format($number);
        ?>
     <script type="text/javascript">
      $(function () {
    

  var chart =   $('#container').highcharts({

        title: {
            text: 'Senarai Target Program Bertandang {{$namadm}} - Sasaran Maksimum: {{$new_numb }} data' ,
            x: -20 //center
        },
        subtitle: {
            text: 'N24 - DUN SEMENYIH - FEBRUARY 2019',
            x: -20
        },

        xAxis: {
            categories: ['1', '2', '3', '4', '5', '6',
                '7', '8', '9', '10', '11', '12','13','14','15','16'],

                var arrData = [
           {"line":"Pengundi putih dikenal pasti","data":{"0": <?php echo $cif_00; ?>,"1": <?php echo $cif_01; ?>,"2": <?php echo $cif_02; ?>,"3": <?php echo $cif_03; ?>,"4": <?php echo $cif_04; ?>,
         "5": <?php echo $cif_05; ?>,"6": <?php echo $cif_06; ?>,"7": <?php echo $cif_07; ?>,"8": <?php echo $cif_08; ?>,"9": <?php echo $cif_09; ?>,"10": <?php echo $cif_10; ?>,"11": <?php echo $cif_11; ?>,"12": <?php echo $cif_12; ?>,"13": <?php echo $cif_13; ?>,"14": <?php echo $cif_14; ?>,
         "15": <?php echo $cif_15; ?>,"16": <?php echo $cif_16; ?>,"17": <?php echo $cif_17; ?>,"18": <?php echo $cif_18; ?>,"19": <?php echo $cif_19; ?>,"20": <?php echo $cif_20; ?>,"21": <?php echo $cif_21; ?>,"22": <?php echo $cif_22; ?>,"23": <?php echo $cif_23; ?>,"24": <?php echo $cif_24; ?>,
         "25": <?php echo $cif_25; ?>,"26": <?php echo $cif_26; ?>,"27": <?php echo $cif_27; ?>,"28": <?php echo $cif_28; ?>
          }},
          {"line":"Target Pengundi Putih","data":
         [
        { x: 0, y: 0 },
        { x: 1, y: 0 },
        { x: 2, y: 0 },
        { x: 3, y: 0 },
        { x: 4, y: 0 },
        { x: 5, y: 0 },
        { x: 6, y: 0 },
        { x: 7, y: 0 },
        { x: 8, y: 0 },
        { x: 9, y: 0},
        { x: 10, y: 0},
        { x: 11, y: 0 },
        { x: 12, y: 0 },
        { x: 13, y: <?php echo $daily13; ?>},
        { x: 14, y: <?php echo $daily13; ?>},
        { x: 15, y: <?php echo $daily14; ?>},
        { x: 16, y: <?php echo $daily15; ?>},
        { x: 17, y: <?php echo $daily16; ?>},
        { x: 18, y:  <?php echo $daily17; ?>},
        { x: 19, y:  <?php echo $daily18; ?>},
        { x: 20, y:  <?php echo $daily19; ?>},
        { x: 21, y:  <?php echo $daily20; ?>},
        { x: 22, y:  <?php echo $daily21; ?>},
        { x: 23, y:  <?php echo $daily22; ?>},
        { x: 24, y:  <?php echo $daily23; ?>},
        { x: 25, y:  <?php echo $daily24; ?>},
        { x: 26, y:  <?php echo $daily25; ?>},
        { x: 27, y:  <?php echo $daily26; ?>},
        { x: 28, y:  <?php echo $target; ?>},
        ]
      },
        ]

        },

        yAxis: {
            title: {
                text: 'Target'
            },
             min: null,
              max: <?php echo $target; ?>,
               tickInterval: 100,
            plotLines: [{
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ' data'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },

        series: [ ]
    });
   
  
       


       
   arrSeries = []; 
   arrMonth = ['0','1', '2', '3', '4', '5', '6',
                '7', '8', '9', '10', '11', '12', '13', '14', '15','16','17','18','19','20','21','22','23','24','25','26','27','28'];

    var index;
    arrData.forEach(function(oneSeries,key){
        
           
       
        var tempArr = Array.apply(0, new Array(29));
        tempArr = tempArr.map(function(val,key){ return null})
       
        for(keyVal in oneSeries.data){
            index  = arrMonth.indexOf(keyVal);
      
           tempArr[parseInt(index)]=oneSeries.data[keyVal];
             
        }
         
        arrSeries[oneSeries.line] = tempArr;
        
        //arrSeries     
    });
    console.log(arrSeries) ;
    
    
    
     var chart=$("#container").highcharts();
    
    for(keyVal in arrSeries){
        console.log(keyVal,arrSeries[keyVal])
        
        chart.addSeries({                        
             name: keyVal,
             data:  arrSeries[keyVal],
            axis : 1
        }) 
    }
 

    
});
    </script>
    
    </body>
</html>

