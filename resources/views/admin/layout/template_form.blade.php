<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>SEMARAK</title>

        <link href="{{asset('admin/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('admin/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
        <link href="{{asset('admin/css/plugins/iCheck/custom.css')}}" rel="stylesheet">
        <link href="{{asset('admin/css/plugins/steps/jquery.steps.css')}}" rel="stylesheet">
        <!-- Toastr style -->
        
        <link href="{{asset('admin/css/animate.css')}}" rel="stylesheet">
        <link href="{{asset('admin/css/style.css')}}" rel="stylesheet">

        <link rel="icon" href="{{asset('main/images/favicon.ico')}}" type="image/x-icon">
    </head>
    @include('admin.layout.admin_header')
    @yield('content_admin')
    @include('admin.layout.admin_footer')
        <!-- Mainly scripts -->
        <script src="{{asset('admin/js/jquery-3.1.1.min.js')}}"></script>
        <script src="{{asset('admin/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('admin/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
        <script src="{{asset('admin/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
        <!-- Custom and plugin javascript -->
        <script src="{{asset('admin/js/inspinia.js')}}"></script>
        <script src="{{asset('admin/js/plugins/pace/pace.min.js')}}"></script>
        <!-- Steps -->
        <script src="{{asset('admin/js/plugins/steps/jquery.steps.min.js')}}"></script>
        <!-- Jquery Validate -->
        <script src="{{asset('admin/js/plugins/validate/jquery.validate.min.js')}}"></script>
        <script>
            $(document).ready(function(){
                $("#wizard").steps();
                $("#form").steps({
                    bodyTag: "fieldset",
                    onStepChanging: function (event, currentIndex, newIndex)
                    {
                        // Always allow going backward even if the current step contains invalid fields!
                        if (currentIndex > newIndex)
                            {
                                return true;
                            }
                        // Forbid suppressing "Warning" step if the user is to young
                        if (newIndex === 3 && Number($("#age").val()) < 18)
                            {
                                return false;
                            }

                        var form = $(this);
                    // Clean up if user went backward before

                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }
                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";
                    // Start validation; Prevent going forward if false
                    return form.valid();
                },

                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }
                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },

                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);
                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";
                    // Start validation; Prevent form submission if false
                    return form.valid();
                },

                onFinished: function (event, currentIndex)
                {
                    var form = $(this);
                    // Submit form input
                    form.submit();
                }
            }).validate({
                errorPlacement: function (error, element)
                    {
                        element.before(error);
                    },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }

                    });
            });
        </script>
        <script type="text/javascript">
            $( "#postcode" ).change(function() {
                var postcode = $('#postcode').val();
                $.ajax({
                url: "<?php  print url('/'); ?>/administrator/postcode/"+postcode,
                dataType: 'json',
                data: {
                },
                success: function (data, status) {
                    jQuery.each(data, function (k) {
                        $("#city").val(data[k].post_office );
                        $("#state").val(data[k].state.state_name );
                        $("#country").val("Malaysia");
                        });
                    }
                });
            });
        </script>

          <script type="text/javascript">
        $( "#chairman_ic" ).change(function() {
            var chairman_ic = $('#chairman_ic').val();
            $.ajax({
            url: "<?php  print url('/'); ?>/administrator/chairman_ic/"+chairman_ic,
            dataType: 'json',
            data: {
            },
            success: function (data, status) {
                jQuery.each(data, function (k) {
                    $("#chairman_name").val(data[k].Nama );
                    $("#NamaDM").val(data[k].NamaDM );
                    $("#NamaDUN").val(data[k].NamaDun );
                    $("#NamaParlimen").val(data[k].NamaParlimen );

                    });
                }
            });
        });
    </script>

    <script type="text/javascript">
        $( "#ic" ).change(function() {
            var ic = $('#ic').val();
            $.ajax({
            url: "<?php  print url('/'); ?>/administrator/ic/"+ic,
            dataType: 'json',
            data: {
            },
            success: function (data, status) {
                jQuery.each(data, function (k) {
                    $("#name").val(data[k].Nama );
                    $("#NamaDM_ajk").val(data[k].NamaDM );
                    $("#NamaDUN_ajk").val(data[k].NamaDun );
                    $("#NamaParlimen_ajk").val(data[k].NamaParlimen );

                    });
                }
            });
        });
    </script>

    <script type="text/javascript">
        $( "#ic_survey" ).change(function() {
            var ic_survey = $('#ic_survey').val();
            $.ajax({
            url: "<?php  print url('/'); ?>/administrator/ic_survey/"+ic_survey,
            dataType: 'json',
            data: {
            },
            success: function (data, status) {
                jQuery.each(data, function (k) {
                    $("#name_survey").val(data[k].Nama );
                    $("#kodlokaliti_survey").val(data[k].Kodlokaliti );
                    $("#Negeri_survey").val(data[k].Negeri );
                    $("#NamaLokaliti_survey").val(data[k].NamaLokaliti );
                    $("#NamaDM_survey").val(data[k].NamaDM );
                    $("#NamaDUN_survey").val(data[k].NamaDun );
                    $("#NamaParlimen_survey").val(data[k].NamaParlimen );

                    });
                }
            });
        });
    </script>
    <script type="text/javascript">
        var Privileges = jQuery('#ones');
        var select = this.value;
        Privileges.change(function () {
            if ($(this).val() == 'kenderaan') { //1 others  
                $('.resources1').show().find(':input').attr('required', true);
            }
            else if($(this).val() == 'wheelchair'){ //2 Maybank
               $('.resources1').show().find(':input').attr('required', true);
            }
             else if($(this).val() == 'both'){ //2 Maybank
                $('.resources1').show().find(':input').attr('required', true);
            }
            else{
                $('.resources1').hide().find(':input').attr('required', false);
            } 

        });
    </script>

    @stack('custom')

    </body>
</html>

