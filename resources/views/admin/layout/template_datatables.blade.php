<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>SEMARAK</title>
        <link href="{{asset('admin/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('admin/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
        <link href="{{asset('admin/css/plugins/iCheck/custom.css')}}" rel="stylesheet">
        <link href="{{asset('admin/css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
        <!-- Toastr style -->
        
        <link href="{{asset('admin/css/animate.css')}}" rel="stylesheet">
        <link href="{{asset('admin/css/style.css')}}" rel="stylesheet">

        <link rel="icon" href="{{asset('main/images/favicon.ico')}}" type="image/x-icon">
    </head>
    @include('admin.layout.admin_header')
    @yield('content_admin')
    @include('admin.layout.admin_footer')
        <!-- Mainly scripts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
         <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="{{asset('admin/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('admin/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
        <script src="{{asset('admin/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
        <!-- Custom and plugin javascript -->
        <script src="{{asset('admin/js/inspinia.js')}}"></script>
        <script src="{{asset('admin/js/plugins/pace/pace.min.js')}}"></script>
        <!-- Steps -->
        <script src="{{asset('admin/js/plugins/dataTables/datatables.min.js')}}"></script>
      
<script type="text/javascript">
   $('#example').dataTable( {
    paging: false,
    searching: false
} )
 
</script>
        <script>
            $(document).ready(function(){
                $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

    </script>

          @stack('custom')
                   
        </script>


        
     
    </script>

    @stack('custom')
    

    </body>
</html>

