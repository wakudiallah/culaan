@extends('admin.layout.template_datatables')

@section('content_admin')
              <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />
    <div class="row wrapper border-bottom white-bg page-heading">
           
        <div class="col-lg-10">
            <h2>Web</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{url('/administrator')}}">Home</a>
                </li>
                <li>
                    <a>News</a>
                </li>

            </ol>
        </div>
        <div class="col-lg-2">
        </div>

    </div>


    <div class="wrapper wrapper-content animated fadeInRight">
        
        
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>News</h5>
                        <div class="ibox-tools">
                          
                        </div>
                    </div>

                    <div class="ibox-content">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8">
                            <h3>Calendar</h3>
                            <div id='calendar'></div>
                        </div>
                        <div class="col-lg-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('custom')
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js'></script>
<script>
    $(document).ready(function() {
        // page is now ready, initialize the calendar...
        $('#calendar').fullCalendar({
            // put your options and callbacks here
            events : [
                @foreach($event as $event)
                {
                    title : '{{ $event->title }}',
                    start : '{{ $event->event_date }}',
                    url : '{{url('/')}}/administrator/web/event/kemaskini/{{$event->id}}'
                },
                @endforeach
            ]
        })
    });
</script>
@endpush