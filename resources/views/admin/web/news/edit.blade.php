@extends('admin.layout.template_form')

@section('content_admin')


        <div class="row wrapper border-bottom white-bg page-heading">
               
            <div class="col-lg-10">
                <h2>Web</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('/administrator')}}">Home</a>
                    </li>
                    <li>
                        <a href="{{url('/administrator/web/news/index')}}">Berita</a>
                    </li>
                    <li class="active">
                        <strong>Kemaskini Berita</strong>
                    </li>

                </ol>
            </div>
            <div class="col-lg-2">
            </div>

        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
             

            <div class="row">
                <div class="col-lg-12">

                    <div class="ibox float-e-margins">

                        <div class="ibox-title">

                            <h5>Kemaskini Berita </h5>

                            <div class="ibox-tools">

                                <a class="collapse-link">

                                    <i class="fa fa-chevron-up"></i>

                                </a>

                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">

                                    <i class="fa fa-wrench"></i>

                                </a>

                               

                                <a class="close-link">

                                    <i class="fa fa-times"></i>

                                </a>

                            </div>

                        </div>

                        <div class="ibox-content">

                            <form class="form-horizontal" method="post" action="{{url('/administrator/web/news/update/'.$sd->id)}}" enctype="multipart/form-data">

                            {{csrf_field()}}

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tajuk</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="title" required value="{{$sd->title}}"> 
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Berita</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control ckeditor" rows="5" id="desc" name="news" required="">{{$sd->news}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Gambar</label>
                                    <div class="col-sm-10">
                                        <input type="file" class="form-control" name="picture" value="{{$sd->picture}}" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Title Gambar</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="title_pic" required value="{{$sd->pic_title}}">
                                    </div>
                                </div>


                                <div class="hr-line-dashed"></div>

                                <div class="form-group">

                                    <div class="col-sm-4 col-sm-offset-2">

                                        <button class="btn btn-primary" type="submit">Simpan</button>

                                    </div>

                                </div>

                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

@endsection


@push('custom')    
    <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>

    <script type="text/javascript">  
        CKEDITOR.replace( 'desc', { 
        shiftEnterMode : CKEDITOR.ENTER_P,
        enterMode: CKEDITOR.ENTER_BR, 
        on: {'instanceReady': function (evt) { evt.editor.execCommand('');     }},
        });      
    </script>

@endpush
