@extends('admin.layout.template_form')

@section('content_admin')


        <div class="row wrapper border-bottom white-bg page-heading">
           
            <div class="col-lg-10">
                <h2>Parameter Organization</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{url('/administrator')}}">Home</a>
                    </li>
                    <li>
                        <a href="{{url('/administrator/parameter/list-pr-organization')}}">Parameter</a>
                    </li>

                    <li class="active">
                        <strong>Kemaskini Parameter</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">
            </div>

        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
             

            <div class="row">

                <div class="col-lg-12">

                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Kemaskini Parameter Baharu</h5>
                            <div class="ibox-tools">

                                <a class="collapse-link">

                                    <i class="fa fa-chevron-up"></i>

                                </a>

                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">

                                    <i class="fa fa-wrench"></i>

                                </a>

                               

                                <a class="close-link">

                                    <i class="fa fa-times"></i>

                                </a>

                            </div>

                        </div>

                        <div class="ibox-content">

                            <form class="form-horizontal" method="post" action="{{url('/administrator/parameter/kemaskini-pr-organization/'.$pr->id)}}">

                            {{csrf_field()}}

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Organization</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="organization" required value="{{$pr->organization}}">
                                    </div>
                                </div>


                                <div class="hr-line-dashed"></div>

                                <div class="form-group">

                                    <div class="col-sm-4 col-sm-offset-2">

                                        <button class="btn btn-primary" type="submit">Simpan</button>

                                    </div>

                                </div>

                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

@endsection





