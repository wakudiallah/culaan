@extends('main.layout.template')

@section('content')

    

    <div class="blogs">
        <div class="container" >


            @guest
            <form method="POST" action="{{ route('login') }}">
                @csrf
            
                <div class="row" style="padding-top: 60px !important; padding-bottom:  60px !important">
                    
                    <div class="col text-center">
                        <div class="section_title">
                            <h2>Login</h2>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div>
                            <input id="email" class="form_input input_name input_ph{{ $errors->has('email') ? ' is-invalid' : '' }}"" type="email" name="name" placeholder="Email / IC" required="required" data-error="Name is required." required autofocus>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif

                                

                            <input id="password" type="password" class="form_input input_email input_ph {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required="required" data-error="Valid email is required.">

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div>
                            <button id="review_submit" type="submit" class="newsletter_submit_btn trans_300" value="Submit">Login</button>
                            <p><a href="{{url('account-register')}}">Create Account</a></p>

                        </div>
                            
                    </div>
                </div>
            </form>

            @else
                <form method="POST" action="{{ url('') }}">
                    @csrf
                
                    <div class="row" style="padding-top: 60px !important; padding-bottom:  60px !important">
                        
                        <div class="col text-center" style="margin-bottom: 60px">
                            <div class="section_title">
                                <h2>Complaint</h2>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div>
                                
                                
                                <h4>Type :</h4>
                                <select class="form_input" id="exampleFormControlSelect1" name="pr_complaint_id">
                                    @foreach($pr as $data)
                                    <option value="{{$data->id}}">{{$data->desc}}</option>
                                    @endforeach
                                </select>


                                <h4>IC :</h4>
                                <input id="" type="text" class="form_input" name="ic" placeholder="IC" required="required" data-error="Valid email is required.">

                                <h4>Email :</h4>
                                <input id="" type="email" class="form_input" name="email" placeholder="Email" required="required" data-error="Valid email is required.">

                                <h4>Password :</h4>
                                <input id="" class="form_input " type="password" name="name" placeholder="Password" required="required" data-error="Name is required.">

                            </div>

                            <div>
                                <button id="review_submit" type="submit" class="newsletter_submit_btn trans_300" value="Submit">Save</button>
                                

                            </div>
                                
                        </div>
                    </div>


                </form>
            @endguest

            
        </div>
    </div>

    
    

@endsection

