@extends('main.layout.template_form')

@section('content')
	
	<div class="container single_product_container">
	<!-- Tabs -->

	<div class="tabs_section_container">

		<div class="container">
			<div class="row">
				<div class="col">
					<div class="tabs_container">
						<ul class="tabs d-flex flex-sm-row flex-column align-items-left align-items-md-center justify-content-center">
							<!--<li class="tab " data-active-tab="tab_1"><span>Description</span></li>
							<li class="tab" data-active-tab="tab_2"><span>Additional Information</span></li>-->
							<li class="tab active" data-active-tab="tab_3"><span>Senarai Bantuan ({{$total}})</span></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">

					<!-- Tab Description -->

					


					<!-- Tab Reviews -->

					<div id="tab_3" class="tab_container active">
						<div class="row">

							<!-- User Reviews -->

							<div class="col-lg-12 reviews_col">
								<div class="tab_title reviews_title">
									<h4>Total Senarai Bantuan ({{$total}})</h4>
								</div>

								<!-- User Review -->
								@foreach($help as $help)
								<?php 
		                            $date=  date('d F Y ', strtotime($help->created_at));
                      			?>
								<div class="user_review_container d-flex flex-column flex-sm-row">
									<div class="user">
										<div class="user_rating">
											<a href="{{$help->link}}" target="_blank">Link</a>
										</div>
									</div>
									<div class="review">
										<div class="review_date">{{$date}}</div>
										<div class="user_name">{{$help->title}}</div>
										<p>{{$help->desc}}</p>
									</div>
								</div>
								@endforeach

							<!-- Add Review -->

						</div>
					</div>

				</div>
			</div>
		</div>

	</div>

	<!-- Benefit -->

	<div class="benefit">
		<div class="container">
			<div class="row benefit_row">
				<div class="col-lg-3 benefit_col">
					<div class="benefit_item d-flex flex-row align-items-center">
						<div class="benefit_icon"><i class="fa fa-truck" aria-hidden="true"></i></div>
						<div class="benefit_content">
							<h6>free shipping</h6>
							<p>Suffered Alteration in Some Form</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 benefit_col">
					<div class="benefit_item d-flex flex-row align-items-center">
						<div class="benefit_icon"><i class="fa fa-money" aria-hidden="true"></i></div>
						<div class="benefit_content">
							<h6>cach on delivery</h6>
							<p>The Internet Tend To Repeat</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 benefit_col">
					<div class="benefit_item d-flex flex-row align-items-center">
						<div class="benefit_icon"><i class="fa fa-undo" aria-hidden="true"></i></div>
						<div class="benefit_content">
							<h6>45 days return</h6>
							<p>Making it Look Like Readable</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 benefit_col">
					<div class="benefit_item d-flex flex-row align-items-center">
						<div class="benefit_icon"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
						<div class="benefit_content">
							<h6>opening all week</h6>
							<p>8AM - 09PM</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

