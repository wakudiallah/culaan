@extends('main.layout.template_gallery')

@section('content')


	 <link type="text/css" rel="stylesheet" href="{{asset('main/src/css/lightGallery.css')}}" /> 
	
	<div class="container product_section_container">
		<div class="row" style="padding-top: 90px !important; padding-bottom:  60px !important">
			<div class="col product_section clearfix">

				<!-- Breadcrumbs -->

				<div class="col text-center" style="margin-bottom: 60px">
					<div class="section_title">
						<h2>{{trans('home/gallery.gallery')}}</h2>
					</div>
				</div>

				<!-- Main Content -->

				<div class="main_content">

					<!-- Products -->

					<div class="products_iso">
						<div class="row">
							
									
									<div id="lightgallery">
										@foreach($gallery as $gallery)

										

										<a href="{{url('main/images/gallery/'.$gallery->picture)}}">

										    <img src="{{url('main/images/gallery/'.$gallery->picture)}}" class="img-responsive img-fluid img-thumbnail"  />
										</a>

										
									  
									  @endforeach
									  
									</div>


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Benefit 

	<div class="benefit">
		<div class="container">
			<div class="row benefit_row">
				<div class="col-lg-3 benefit_col">
					<div class="benefit_item d-flex flex-row align-items-center">
						<div class="benefit_icon"><i class="fa fa-truck" aria-hidden="true"></i></div>
						<div class="benefit_content">
							<h6>free shipping</h6>
							<p>Suffered Alteration in Some Form</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 benefit_col">
					<div class="benefit_item d-flex flex-row align-items-center">
						<div class="benefit_icon"><i class="fa fa-money" aria-hidden="true"></i></div>
						<div class="benefit_content">
							<h6>cach on delivery</h6>
							<p>The Internet Tend To Repeat</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 benefit_col">
					<div class="benefit_item d-flex flex-row align-items-center">
						<div class="benefit_icon"><i class="fa fa-undo" aria-hidden="true"></i></div>
						<div class="benefit_content">
							<h6>45 days return</h6>
							<p>Making it Look Like Readable</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 benefit_col">
					<div class="benefit_item d-flex flex-row align-items-center">
						<div class="benefit_icon"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
						<div class="benefit_content">
							<h6>opening all week</h6>
							<p>8AM - 09PM</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>-->
@endsection


@push('custom')

	<script src="{{asset('main/src/jquery.min.js')}}"></script>

    <!-- A jQuery plugin that adds cross-browser mouse wheel support. (Optional) -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>

    <script src="{{asset('main/src/js/lightgallery.min.js')}}"></script>

    <!-- lightgallery plugins -->
    <script src="{{asset('main/src/js/lg-thumbnail.min.js')}}"></script>
    <script src="{{asset('main/src/js/lg-fullscreen.min.js')}}"></script>

    <script type="text/javascript">
    	require(['./lightgallery.js'], function() {
		    require(["./lg-zoom.js", "./lg-thumbnail.js"], function(){
		        $("#lightgallery").lightGallery(); 
		    });
		});
    </script>

    <script type="text/javascript">
	    $(document).ready(function() {
	        $("#lightgallery").lightGallery(); 
	    });
	</script>




@endpush