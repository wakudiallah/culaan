<header class="header trans_300">

		<!-- Top Navigation -->

		<div class="top_nav">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<!-- <div class="top_nav_left">free shipping on all u.s orders over $50</div> -->
					</div>
					<div class="col-md-9 text-right">
						<div class="top_nav_right">
							<ul class="top_nav_menu">

								<li class="currency">
									<a href="#">
										{{trans('menu/menu.information')}}
										<i class="fa fa-angle-down"></i>
									</a>
									<ul class="currency_selection">
										<li><a href="{{url('information/help')}}">{{trans('menu/menu.beneficiary')}}</a></li>
										<li><a href="#">xd</a></li>
									</ul>
								</li> 
							

								<li class="language">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								      <img src="{!! asset('img/' .  Config::get('languages')[App::getLocale()]  . '.png') !!}"><span class="fa fa-angle-down"></span>&nbsp; {{Config::get('languages')[App::getLocale()]}}
								    </a>
									<ul class="language_selection">
										  @foreach(Config::get('languages') as $lang => $language)
								            @if($lang != App::getLocale())
								                <li>
								                    <a href="{{ route('lang.switch', $lang) }}"><img src="{!! asset('img/' . $language . '.png') !!}">&nbsp; {{$language}}</a>

								                </li>

								            @endif
								        @endforeach
									</ul>
								</li>


								@guest

								<li class="language">
										<form method="POST" action="{{ route('login') }}">
                						@csrf
								      	<input type="text" tabindex="1" id="email" placeholder="Email / IC" name="email" class="inputtext radius1" value="" required="">
								      	@if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif

								      	<input type="password" tabindex="2" id="pass" placeholder="Password" name="password" class="inputtext radius1" required>
								      	@if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif

								      	<input type="submit" class="fbbutton" name="login" value="Login" />

								      	</form>

								</li>
												
								@endguest


								<!--<li class="account">
									<a href="#">
										{{trans('menu/menu.home')}}
										<i class="fa fa-angle-down"></i>
									</a>
									<ul class="account_selection">
										<li><a href="#"><i class="fa fa-sign-in" aria-hidden="true"></i>Sign In</a></li>
										<li><a href="#"><i class="fa fa-user-plus" aria-hidden="true"></i>Register</a></li>
									</ul>
								</li>-->
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Main Navigation -->

		<div class="main_nav_container">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-right">
						<div class="logo_container">
							<a href="#">SMA<span>RAK</span></a>
						</div>
						<nav class="navbar">
							<ul class="navbar_menu">
								<li><a href="{{url('/')}}">{{trans('menu/menu.home')}}</a></li>
								<li><a href="#">{{trans('menu/menu.information')}}</a></li>
								<li><a href="#">{{trans('menu/menu.know')}}</a></li>

								<li class="main">
									<a href="#">{{trans('menu/menu.service')}}
										<i class="fa fa-angle-down"></i>
									</a>
									
									<ul class="main_selection">

										<li style="font-size: 11px !important"><a href="{{url('/')}}//services/add/1">{{trans('menu/menu.complaint')}}</a></li>
										<li style="font-size: 11px !important"><a href="{{url('/')}}//services/add/2">{{trans('menu/menu.help')}}</a></li>
										<li style="font-size: 11px !important"><a href="{{url('/')}}//services/add/3">{{trans('menu/menu.proposal')}}</a></li>
									</ul>
								</li>
								<li><a href="{{url('event')}}">{{trans('menu/menu.event')}}</a></li>
								<li><a href="{{url('gallery')}}">{{trans('menu/menu.gallery')}}</a></li>

								<!--<li><a href="#">PRK</a></li>
								 <?php 
                                    $prk = \App\Model\PRK::where('status','1')->orderby('created_at','DESC')->get();
                                ?>
								<li class="main">
									<a href="#">
										PRK
										<i class="fa fa-angle-down"></i>
									</a>
									<ul class="main_selection">
										@foreach($prk as $prk)
										<li style="font-size: 11px !important"><a href="{{url('/')}}/pilihan-raya/prk/{{$prk->dun_code}}">{{$prk->name_dun->dun_name}}</a></li>
										@endforeach
									</ul>
								</li> -->
								@guest
								
								<li><a href="{{url('contact-us')}}">{{trans('menu/menu.contact')}}</a></li>
								<li><a href="{{route('login')}}">{{trans('menu/menu.login')}}</a></li>
								@else


								@if(Auth::user()->role == 2)
								<li><a href="{{url('/')}}//services/list">{{trans('menu/menu.list')}}</a></li>

								<li><a href="{{route('login')}}">
									<li><a href="{{route('logout')}}"
											onclick="event.preventDefault();
			                                   document.getElementById('logout-form').submit();"> {{trans('menu/menu.logout')}}</a>

			                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                                        @csrf
	                                    </form>
			                        </li>
			                        @else
			                        	<li><a href="{{url('administrator')}}">Admin</a></li>
			                        	<li><a href="{{route('logout')}}"
											onclick="event.preventDefault();
			                                   document.getElementById('logout-form').submit();"> {{trans('menu/menu.logout')}}</a>
				                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
		                                        @csrf
		                                    </form>
			                        	</li>
			                        @endif
								@endguest

								


							</ul>
							<!-- <ul class="navbar_user">
								<li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-user" aria-hidden="true"></i></a></li>
								
							</ul>-->
							<div class="hamburger_container">
								<i class="fa fa-bars" aria-hidden="true"></i>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</div>

	</header>