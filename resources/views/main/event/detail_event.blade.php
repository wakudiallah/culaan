    @extends('main.layout.template_fix')

    @section('content')


    <link rel="stylesheet" type="text/css" href="{{asset('main/styles/single_responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('main/styles/single_styles.css')}}">


    <div class="container single_product_container">
        <div class="row">
            <div class="col">

                <!-- Breadcrumbs -->

                <div class="breadcrumbs d-flex flex-row align-items-center">
                    <ul>
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li><a href="{{url('/event')}}">Event</a></li>
                        <li class="active"><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Detail Event</a></li>
                    </ul>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-lg-5">
                <div class="single_product_pics">
                    <div class="row">
                            <img src="{{url('main/images/event/'.$news->picture)}}" class="img-fluid" alt="..." >
                        
                    </div>
                </div>
            </div>

            <?php 
                    $tarikh =  date('d-m-Y ', strtotime($news->created_at)); 
                    $waktu = date('h:i A', strtotime($news->created_at));
                    $stringCut = substr($news->news, 0, 250);
            ?>

            <div class="col-lg-7">
                <div class="product_details">
                    <div class="product_details_title">
                        <h2>{{$news->title}}</h2>
                        <p><i class="fa fa-calendar"></i> {{$tarikh}}<i class="fa fa-clock-o"></i> {{$waktu}}</p>
                        <p class="align-items-justify justify-content-center">{{$news->desc}}</p>
                    </div>
                    <div class="free_delivery d-flex flex-row align-items-center justify-content-center">
                        <span>Penulis : </span>
                    </div>
                    
                   
                    
                </div>
            </div>
        </div>

    </div>


  


   
    <script src="{{asset('main/jssingle_custom.js')}}"></script>

    @endsection