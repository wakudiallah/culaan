@extends('main.layout.template_no_slider')

@section('content')
	
	<div class="blogs">
		<div class="container" >

			<form method="POST" action="{{ url('save-report-complaints') }}" enctype="multipart/form-data">
	            @csrf
				<input type='hidden' name='latitude' id='latitude' >
                <input type='hidden' name='longitude' id='longitude'>
                <input type='hidden' name='location' id='location' >
                <input id="type_service" class="form_input " type="hidden" name="type_service" value="{{$report_complaint_id}}">
				<div class="row" style="padding-top: 60px !important; padding-bottom:  60px !important">
					
					<div class="col text-center" style="margin-bottom: 60px">
						<div class="section_title">
							<h2>{{trans('complaint/complaint.complaint')}}</h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<div>
							<h4>{{trans('complaint/complaint.type')}} :</h4>
							<select class="form_input" id="pr_complaint_id" name="pr_complaint_id">
								@foreach($pr as $data)                                        
									 <?php 
                                        if($report_complaint_id==$data->id) {
                                          $selected = "selected";
                                        }
                                        else {
                                           $selected = "";
                                        } 
                                    ?>
                                    <option {{$selected}} value="{{$data->id}}">{{$data->desc}}</option>
                                @endforeach
						    </select>
						    
							<h4>{{trans('complaint/complaint.case')}} :</h4>
                            <select name="jenis" class="form-control" required="" id="jenis">
                                @foreach($pr_service as $pr_service)
                                    <option value="{{$pr_service->code}}|{{$pr_service->report_complaint_id}}">{{$pr_service->code}} - {{$pr_service->desc}}</option>
                                @endforeach
                            </select><br>
							<h4>{{trans('complaint/complaint.name')}} :</h4>
							<input id="name" class="form_input " type="text" name="name" placeholder="{{trans('complaint/complaint.name')}}" required="required" data-error="Name is required." readonly  value="{{$name}}">

							<input id="name" class="form_input " type="hidden" name="ic" placeholder="{{trans('complaint/complaint.name')}}" required="required" data-error="Name is required." readonly  value="{{$ic->ic}}">
							
							<h4>{{trans('complaint/complaint.phone')}} :</h4>
							<input id="phone" class="form_input " type="text" name="phone" placeholder="{{trans('complaint/complaint.phone')}}" required="required" data-error="Name is required." onkeypress="return isNumberKey(event)" maxlength="12">

							<!--<h4>{{trans('complaint/complaint.ic')}} :</h4>
							<input class="form_input " type="text" name="ic" placeholder="{{trans('complaint/complaint.ic')}}" required="required" data-error="Name is required." readonly  value="{{$ic->ic}}">-->

							<h4>{{trans('complaint/complaint.title')}} :</h4>
							<input id="" class="form_input " type="text" name="title" placeholder="{{trans('complaint/complaint.title')}}" required="required" data-error="Name is required.">

							<h4>{{trans('complaint/complaint.desc')}} :</h4>
							<textarea id="input_message" class="input_ph input_message" name="desc"  placeholder="{{trans('complaint/complaint.desc')}}" rows="6" required data-error="Please, write us a message."></textarea>
						</div>
					</div>
					<div class="col-lg-6">
						<div>
							<h4>{{trans('complaint/complaint.location')}} :</h4>
							<input id="address_1" class="form_input " type="text" name="address_1" placeholder="{{trans('complaint/complaint.address')}} 1" required="required" data-error="Name is required.">
							<input id="address_2" class="form_input " type="text" name="address_2" placeholder="{{trans('complaint/complaint.address')}} 2" required="required" data-error="Name is required.">
							<input id="address_3" class="form_input " type="text" name="address_2" placeholder="{{trans('complaint/complaint.address')}} 3" required="required" data-error="Name is required.">

							<h4>{{trans('complaint/complaint.postcode')}} :</h4>
							<input id="postcode2" class="form_input " type="text" name="postcode" placeholder="{{trans('complaint/complaint.postcode')}}" required="required" data-error="Name is required.">

							<h4>{{trans('complaint/complaint.city')}} :</h4>
							<input id="city" class="form_input" type="text" name="city" placeholder="{{trans('complaint/complaint.city')}}" required="required" data-error="Name is required.">

							<h4>{{trans('complaint/complaint.state')}} :</h4>
							<input id="state" class="form_input " type="text" name="state" placeholder="{{trans('complaint/complaint.state')}}" required="required" data-error="Name is required.">
							
							<h4>{{trans('complaint/complaint.image')}} :</h4>
							<div class="input-group control-group increment" >
					          <input type="file" name="filename[]" class="form-control" required="">
					          <div class="input-group-btn"> 
					            <button class="btn btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>{{trans('complaint/complaint.add')}}</button>
					          </div>
					        </div>
					        <div class="clone hide">
					          <div class="control-group input-group" style="margin-top:10px">
					            <input type="file" name="filename[]" class="form-control">
					            <div class="input-group-btn"> 
					              <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i> {{trans('complaint/complaint.remove')}}</button>
					            </div>
					          </div>
					        </div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-5"></div>
					<div class="col-lg-6">
						<div style="margin-top: 30px !important">
							<button id="review_submit" type="submit" class="newsletter_submit_btn trans_300" value="Submit">{{trans('complaint/complaint.save')}}</button>
						</div>
					</div>
				</div>

			</form>

		</div>
	</div>

@endsection			


@push('custom')

	<script type="text/javascript">
        $( "#postcode2" ).change(function() {
            var postcode = $('#postcode2').val();
            $.ajax({
            url: "<?php  print url('/'); ?>/administrator/postcode2/"+postcode,
            dataType: 'json',
            data: {
            },
            success: function (data, status) {
                jQuery.each(data, function (k) {
                    $("#city").val(data[k].post_office );
                    $("#state").val(data[k].state.state_name );
                    $("#country").val("Malaysia");
                    });
                }
            });
        });
    </script>


    <script type="text/javascript">

	    $(document).ready(function() {

	      $(".btn-success").click(function(){ 
	          var html = $(".clone").html();
	          $(".increment").after(html);
	      });

	      $("body").on("click",".btn-danger",function(){ 
	          $(this).parents(".control-group").remove();
	      });

	    });

	</script>

	<script>
	function isNumberKey(evt){
	    var charCode = (evt.which) ? evt.which : event.keyCode
	    if (charCode > 31 && (charCode < 48 || charCode > 57))
	        return false;
	    return true;
	}
	</script>

	<script type="text/javascript">
       $( document ).ready(function() {
       var options = $('#jenis').children().clone();
      
        $('#pr_complaint_id').change(function() {
          $('#jenis').children().remove();
        var rawValue =this.value;
         options.each(function () {
                var newValue = $(this).val().split('|');
                if (rawValue == newValue[1] ) {
                    $('#jenis').append(this);
                 }
            });
          $('#jenis').val('');
        });
    });

@endpush