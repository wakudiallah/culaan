@extends('main.layout.template_no_slider')

@section('content')
<style>
.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
}
.button2 {background-color: #008CBA;} /* Blue */
.button3 {background-color: #f44336;} /* Red */ 
.button4 {background-color: #e7e7e7; color: black;} /* Gray */ 
.button5 {background-color: #555555;} /* Black */
</style>   

	<div class="blogs">
		<div class="container">
			<form method="POST" action="{{ url('save-report-complaints') }}" enctype="multipart/form-data">
	            @csrf
				<div class="row" style="padding-top: 90px !important; padding-bottom:  60px !important">
					
					<div class="col text-center" style="margin-bottom: 60px">
						<div class="section_title">
							<h2>{{trans('complaint/list.complaint')}}</h2>
						</div>
					</div>

					<div class="col-lg-12">
						<div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>{{trans('complaint/list.no')}}</th>
                                        <th>{{trans('complaint/list.name')}}</th>
                                        <th>{{trans('complaint/list.ic')}}</th>
                                        <th>{{trans('complaint/list.title')}}</th>
                                        <th>{{trans('complaint/list.type')}}</th>
                                        <th>{{trans('complaint/list.detail')}}</th>
                                        <th>{{trans('complaint/list.petugas')}}</th>
                                        <th>Tarikh {{trans('complaint/list.open_date')}}</th>
                                        <th>{{trans('complaint/list.close_date')}}</th>
                                        <th>{{trans('complaint/list.status')}}</th>
                                        <th>{{trans('complaint/list.action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $i=1; ?>
                                @foreach($report as $report)
                                 	<tr class="gradeX">
                                        <td>{{$i}}</td>
                                        <td>{{$report->user_rakyat->fullname}}</td>
                                        <td>{{$report->ic}}</td>
                                        <td>{{$report->title}}</td>
                                        <td><span class="badge badge-{{$report->pr_report->color}}">{{$report->pr_report->desc}}</span></td>
                                        <td>
                                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#example{{$report->id}}"><i class="fa fa-chevron-down"></i> {{trans('complaint/list.detail')}}</button>
                                        </td><!-- modal image -->
                                        @if($report->petugas_id!='')
                                        <td class="center">{{$report->user_petugas->name}}</td>
                                        @else
                                        <td class="center"></td>
                                        @endif
                                        <td>
                                            <?php $tarikh =  date('d-m-Y ', strtotime($report->created_at)); ?>
                                            {{$tarikh}}
                                        </td>
                                        @if($report->reply_id!='')
                                            <td>
                                                <?php $tarikh_tutup =  date('d-m-Y ', strtotime($report->reply->created_at)); ?>
                                                    {{$tarikh_tutup}}
                                            </td>
                                        @else
                                            <td></td>
                                        @endif
                                        @if($report->reply_id!='')
                                            @if($report->reply->status=='TRM')
                                                <td>   <button class="button button3">{{trans('complaint/list.accepted')}}</button></td>
                                            @elseif($report->reply->status=='TRK')
                                                <td> <button class="button button5">{{trans('complaint/list.rejected')}}</button></td>
                                            @endif
                                        @else
                                        <td></td>
                                        @endif
                                        <td class="center">
                                            @if($report->petugas_id=='')
                                            <a href="{{url('/')}}/administrator/report-complaint/action/{{$report->id}}" type="submit" class="btn btn-danger" style="cursor:pointer;">
                                            <i class="fa fa-paste"></i> {{trans('complaint/list.action')}}
                                            </a>

                                            @elseif($report->reply->status=='TRM')
                                                <a href="{{url('/')}}/administrator/report-complaint/print/{{$report->id}}" type="submit" class="btn btn-warning" target="_blank">
                                                <i class="fa fa-print"></i>{{trans('complaint/list.print')}}
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                <?php  $i++; ?>
                                @endforeach
                                </tbody>
                                <!--<tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>IC</th>
                                        <th>Nama DUN</th>
                                        <th>Petugas</th>
                                        <th>Tindakan</th>
                                    </tr>
                                </tfoot>-->
                            </table>
                        </div>
                    </div>
				</div>
			</form>
		</div>
	</div>

	@foreach($report2 as $data)
    <div class="modal fade bd-example-modal-lg" id="example{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-red">
                    <h2 class="modal-title" id="exampleModalLabel"><b>{{trans('complaint/list.detail')}} - {{$data->user_rakyat->fullname}} {{$data->ic}}</b></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <?php 
                        $image   = \DB::table('detail_image_report_complaints')->where('detail_report_complaints_id', $data->id)->get();
                    ?>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                        	<tbody>
                                <b style="color: blue !important">{{trans('complaint/list.detail')}} </b>
                                <tr class="gradeX">
                                    <td>{{trans('complaint/list.type')}} </td>
                                    <td>:</td>
                                    <td><b style="color: red !important">{{$data->pr_report->desc}}</b></td>
                                </tr>
                                <tr class="gradeX">
                                    <td>{{trans('complaint/list.title')}}  </td>
                                    <td>:</td>
                                    <td><b>{{$data->title}}</b></td>
                                    
                                </tr>
                                <tr class="gradeX">
                                    <td>{{trans('complaint/list.desc')}}  </td>
                                    <td>:</td>
                                    <td><b>{{$data->desc}}</b></td>
                                </tr>
                                <tr class="gradeX">
                                    <td>{{trans('complaint/list.address')}}  </td>
                                    <td>:</td>
                                    <td><b>{{$data->address}}</b></td>
                                </tr>
                                <tr class="gradeX">
                                    <td>{{trans('complaint/list.postcode')}}</td>
                                    <td>:</td>
                                    <td><b>{{$data->postcode}}</b></td>
                                </tr>

                                <tr class="gradeX">
                                    <td>{{trans('complaint/list.city')}} </td>
                                    <td>:</td>
                                    <td><b>{{$data->city}}</b></td>
                                </tr>

                                <tr class="gradeX">
                                    <td>{{trans('complaint/list.state')}} </td>
                                    <td>:</td>
                                    <td><b>{{$data->state}}</b></td>
                                </tr>

                            </tbody>
                            
                        </table>
                    </div>
                     
                    <b style="color: blue !important; margin-top: 40px !important">{{trans('complaint/list.image')}}</b>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>{{trans('complaint/list.no')}}</th>
                                        <th>{{trans('complaint/list.filename')}}</th>
                                        <th>{{trans('complaint/list.submit_date')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $i=1; ?>
                                @foreach($image as $report)
                                    <tr class="gradeX">
                                        <td>{{$i}}</td>
                                        <td>
                                            <a href="{{asset('/admin/complaint/'.$data->user_rakyat->fullname.'/'.$report->filename)}}" class="btn bg-light-blue" target='_blank'>{{$report->filename}}</a>
                                        </td>
                                        <td>{{$report->created_at}}</td>
                                        
                                    </tr>
                                <?php  $i++; ?>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                  </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection			


@push('custom')

	<script type="text/javascript">
        $( "#postcode2" ).change(function() {
            var postcode = $('#postcode2').val();
            $.ajax({
            url: "<?php  print url('/'); ?>/administrator/postcode2/"+postcode,
            dataType: 'json',
            data: {
            },
            success: function (data, status) {
                jQuery.each(data, function (k) {
                    $("#city").val(data[k].post_office );
                    $("#state").val(data[k].state.state_name );
                    $("#country").val("Malaysia");
                    });
                }
            });
        });
    </script>


    <script type="text/javascript">

	    $(document).ready(function() {

	      $(".btn-success").click(function(){ 
	          var html = $(".clone").html();
	          $(".increment").after(html);
	      });

	      $("body").on("click",".btn-danger",function(){ 
	          $(this).parents(".control-group").remove();
	      });

	    });

	</script>

@endpush